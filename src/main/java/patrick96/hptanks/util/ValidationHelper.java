/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.util;

public class ValidationHelper {
    
    /**
     * Checks if the dimensions are big enough and not too big
     * 
     * @param dimensions
     *            the dimensions to check
     * @param min
     *            the minimum size
     * @param max
     *            the maximum size
     */
    public static boolean hasProperDimensions(Coords dimensions, int min, int max) {
        return !(dimensions.x < min || dimensions.y < min || dimensions.z < min || dimensions.x > max || dimensions.y > max || dimensions.z > max);

    }
    
    @Deprecated
    /**
     * Checks if the given block is placed in the frame of the multiblock
     * @param relativePosition the position of the block to check relative to the corner block
     * @param dimensions the dimensions of the multiblock
     * @return if the block is in the multiblock frame
     */
    public static boolean isInFrame(Coords relativePosition, Coords dimensions) {
        int x = relativePosition.x;
        int y = relativePosition.y;
        int z = relativePosition.z;

        return x == 0 && 0 <= y && y <= dimensions.y - 1 && 0 <= z && z <= dimensions.z - 1 || y == 0 && 0 <= x && x <= dimensions.x - 1 && 0 <= z
                && z <= dimensions.z - 1 || z == 0 && 0 <= y && y <= dimensions.y - 1 && 0 <= x && x <= dimensions.x - 1;

    }
    
    @Deprecated
    /**
     * Checks if the given block is placed in the walls of the multiblock
     * @param relativePosition the position of the block to check relative to the corner block
     * @param dimensions the dimensions of the multiblock
     * @return if the block is in the multiblock walls
     */
    public static boolean isInWalls(Coords relativePosition, Coords dimensions) {
        int x = relativePosition.x;
        int y = relativePosition.y;
        int z = relativePosition.z;

        return x == 0 && 1 <= y && y <= dimensions.y - 2 && 1 <= z && z <= dimensions.z - 2 || y == 0 && 1 <= x && x <= dimensions.x - 2 && 1 <= z
                && z <= dimensions.z - 2 || z == 0 && 1 <= y && y <= dimensions.y - 2 && 1 <= x && x <= dimensions.x - 2;

    }
}
