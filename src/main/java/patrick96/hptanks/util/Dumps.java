/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.util;

import cpw.mods.fml.common.registry.FMLControlledNamespacedRegistry;
import cpw.mods.fml.common.registry.GameData;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.ReflectionHelper;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.*;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.oredict.RecipeSorter;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import patrick96.hptanks.blocks.BlockMachine;
import patrick96.hptanks.blocks.BlockOre;
import patrick96.hptanks.blocks.fluids.BlockFluid;
import patrick96.hptanks.blocks.hpt.BlockTankComponent;
import patrick96.hptanks.config.ConfigHandler;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.handler.FuelHandler;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.items.*;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.lib.WorldGenInfo;
import patrick96.hptanks.recipe.*;
import patrick96.hptanks.recipe.condition.BaseRecipeCondition;
import patrick96.hptanks.recipe.condition.UpgradeRecipeCondition;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Dumps information about blocks and items to file
 * Different blocks or items are seperated by \n\n\n
 */
public class Dumps {

    public static Map<String, String> displayNameMap = new HashMap<String, String>() {{
        this.put("ic2.itemCableO", "Copper Cable");
        this.put("ic2.itemCable", "Insulated Copper Cable");
        this.put("ic2.itemGoldCable", "Gold Cable");
        this.put("ic2.itemGoldCableI", "Insulated Gold Cable");
        this.put("ic2.itemIronCable", "HV Cable");
        this.put("ic2.itemIronCableI", "Insulated HV Cable");
        this.put("ic2.itemTinCable", "Tin Cable");
        this.put("ic2.itemTinCableI", "Insulated Tin Cable");
        this.put("ic2.itemPartAlloy", "Advanced Alloy");
        this.put("ic2.itemPartCircuitAdv", "Advanced Circuit");
        this.put("ic2.itemIngotTin", "Tin Ingot");
        this.put("ic2.blockRubWood", "Rubber Wood");
        this.put("ic2.itemHarz", "Sticky Resin");
        this.put("ic2.itemRubber", "Rubber");
        this.put("ic2.blockRubber", "Rubber Sheet");
        this.put("ic2.boatRubber", "Rubber Dinghy");
        this.put("ic2.itemStaticBoots", "Rubber Boots");
    }};


    public static FMLControlledNamespacedRegistry<Block> blockRegistry = GameData.getBlockRegistry();
    public static FMLControlledNamespacedRegistry<Item> itemRegistry = GameData.getItemRegistry();

    public static void dump() {

        long time = System.currentTimeMillis();
        LogHelper.info("Started dumping data");

        File dir = new File(new File(Minecraft.getMinecraft().mcDataDir, "dumps"), Reference.MOD_ID);
        if(ConfigHandler.dumpBlocks.getBoolean()) {
            try {
                long blockTime = System.currentTimeMillis();
                int dumped = dumpBlocks(dir);
                LogHelper.info("Successfully dumped " + dumped + " block info entries in " + (System.currentTimeMillis() - blockTime)  + "ms");
            } catch(Exception e) {
                LogHelper.error(e.getMessage());
                e.printStackTrace();
            }
        }

        if(ConfigHandler.dumpItems.getBoolean()) {
            try {
                long itemTime = System.currentTimeMillis();
                int dumped = dumpItems(dir);
                LogHelper.info("Successfully dumped " + dumped + " item info entries in " + (System.currentTimeMillis() - itemTime)  + "ms");
            } catch(Exception e) {
                LogHelper.error(e.getMessage());
                e.printStackTrace();
            }
        }

        if(ConfigHandler.dumpRecipes.getBoolean()) {
            try {
                long recipeTime = System.currentTimeMillis();
                int dumped = dumpRecipes(dir);
                LogHelper.info("Successfully dumped " + dumped + " recipes in " + (System.currentTimeMillis() - recipeTime)  + "ms");
            } catch(Exception e) {
                LogHelper.error(e.getMessage());
                e.printStackTrace();
            }
        }

        if(ConfigHandler.dumpWorldGen.getBoolean()) {
            try {
                long worldGenTime = System.currentTimeMillis();
                int dumped = dumpWorldGen(dir);
                LogHelper.info("Successfully dumped " + dumped + " worldgen info entries in " + (System.currentTimeMillis() - worldGenTime)  + "ms");
            } catch(Exception e) {
                LogHelper.error(e.getMessage());
                e.printStackTrace();
            }
        }

        if(ConfigHandler.dumpCatalysts.getBoolean()) {
            try {
                long catalystsTime = System.currentTimeMillis();
                int dumped = dumpCatalysts(dir);
                LogHelper.info("Successfully dumped " + dumped + " catalyst info entries in " + (System.currentTimeMillis() - catalystsTime)  + "ms");
            } catch(Exception e) {
                LogHelper.error(e.getMessage());
                e.printStackTrace();
            }
        }

        if(ConfigHandler.dumpCutters.getBoolean()) {
            try {
                long cuttersTime = System.currentTimeMillis();
                int dumped = dumpCutters(dir);
                LogHelper.info("Successfully dumped " + dumped + " cutters info entries in " + (System.currentTimeMillis() - cuttersTime)  + "ms");
            } catch(Exception e) {
                LogHelper.error(e.getMessage());
                e.printStackTrace();
            }
        }

        LogHelper.info("Finished data dump in " + (System.currentTimeMillis() - time)  + "ms");
    }

    private static int dumpCutters(File dir) throws IOException {
        File file = new File(dir, "cutters.txt");
        dir.mkdirs();

        if(!file.exists()) {
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(file);
        writer.write("");

        int dumped = 0;

        for(ModItems.ItemCutter cutter : ModItems.ItemCutter.values()) {
            if(appendCutter(writer, cutter)) {
                dumped++;
            }
        }

        writer.close();

        return dumped;
    }

    private static boolean appendCutter(PrintWriter writer, ModItems.ItemCutter cutter) {
        if(cutter == null) {
            return false;
        }

        LogHelper.trace("Dumping Cutter info for " + cutter.getStack().getUnlocalizedName());

        writer.append("cutter\t" + getDisplayName(cutter.getStack()) + "\n");
        writer.append("tier\t" + cutter.config.getTier() + "\n");
        writer.append("maxDamage\t" + cutter.maxDamage + "\n");
        writer.append("damageType\t" + cutter.config.getDamage().name() + "\n");
        writer.append("baseEnergyCoefficient\t" + StringUtils.prettyPrintNumber(cutter.config.baseEnergyCoefficient, 1, false)  + "\n");
        writer.append("baseTimeCoefficient\t" + StringUtils.prettyPrintNumber(cutter.config.baseTimeCoefficient, 1, false) + "\n");

        writer.append("\n\n");
        return true;
    }

    private static int dumpCatalysts(File dir) throws IOException {
        File file = new File(dir, "catalysts.txt");
        dir.mkdirs();

        if(!file.exists()) {
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(file);
        writer.write("");

        int dumped = 0;

        for(CVDFurnaceRecipeInfo.Catalyst catalyst : CVDFurnaceRecipeInfo.Catalyst.values()) {
            if(appendCatalyst(writer, catalyst)) {
                dumped++;
            }
        }

        writer.close();

        return dumped;
    }

    private static boolean appendCatalyst(PrintWriter writer, CVDFurnaceRecipeInfo.Catalyst catalyst) {
        if(catalyst == null) {
            return false;
        }

        LogHelper.trace("Dumping " + catalyst.localize() + " catalyst.");

        ItemStack stack = getItemStackFromOreDictList(catalyst.stack.getStacks());

        if(stack == null) {
            LogHelper.info("Tried to dump catalyst " + catalyst.localize() + " with an empty ItemStackIdentifier due to empty oredict, will be ignored. Set LogLevel to debug to see more information");
            LogHelper.debug("OreDict: " + catalyst.stack.getOreDict());
            LogHelper.debug("Original stack size: " + catalyst.stack.getStacks().size());
            return false;
        }

        writer.append("stack\t" + getDisplayName(stack) + "\n");
        writer.append("catalyst\t" + catalyst.localize() + "\n");
        writer.append("shortName\t" + catalyst.getShortName() + "\n");
        writer.append("limit\t" + catalyst.limit + "\n");

        writer.write("\n\n");

        return true;
    }

    public static int dumpWorldGen(File dir) throws IOException, IllegalAccessException {
        File file = new File(dir, "worldgen.txt");
        WorldGenInfo.Specs[] specs = WorldGenInfo.defaultSpecs;
        dir.mkdirs();

        if(!file.exists()) {
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(file);
        writer.write("");

        int dumped = 0;

        for(WorldGenInfo.Specs spec : specs) {
            if(appendWorldGen(writer, spec)) {
                dumped++;
            }
        }

        writer.close();
        return dumped;
    }

    public static boolean appendWorldGen(Writer writer, WorldGenInfo.Specs spec) throws IOException {
        if(spec == null) {
            return false;
        }

        ItemStack ore = ModBlocks.ore.getStack(1, spec.meta);

        LogHelper.trace("Dumping Worldgen Spec for " + spec.name);

        writer.append("ore\t" + getDisplayName(ore) + "\n");
        writer.append("minY\t" + spec.minY + "\n");
        writer.append("maxY\t" + spec.maxY + "\n");
        writer.append("maxVeinSize\t" + spec.maxVeinSize + "\n");
        writer.append("veinsPerChunk\t" + spec.veinsPerChunk + "\n");

        writer.append("\n\n");

        return true;
    }

    public static int dumpRecipes(File dir) throws IOException, IllegalAccessException {
        File file = new File(dir, "recipes.txt");
        List craftingRecipes = CraftingManager.getInstance().getRecipeList();
        Map furnaceRecipes = FurnaceRecipes.smelting().getSmeltingList();
        dir.mkdirs();

        if(!file.exists()) {
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(file);
        writer.write("");

        int dumped = 0;
        for(Object obj : craftingRecipes) {
            if(obj instanceof IRecipe) {
                if(appendCraftingRecipe(writer, (IRecipe) obj)) {
                    dumped++;
                }
            }
        }

        if(furnaceRecipes != null) {
            for(Object obj : furnaceRecipes.keySet()) {
                if(appendFurnaceRecipe(writer, (ItemStack) obj)) {
                    dumped++;
                }
            }
        }

        List<MachineRecipeInfo> machineRecipes = new ArrayList<MachineRecipeInfo>();
        machineRecipes.addAll(HPTRegistry.wireMillRecipes);
        machineRecipes.addAll(HPTRegistry.cvdFurnaceRecipes);
        machineRecipes.addAll(HPTRegistry.hpCutterRecipes);
        machineRecipes.addAll(HPTRegistry.resinExtractoRecipes);
        machineRecipes.addAll(HPTRegistry.rubberFormerRecipes);

        for(MachineRecipeInfo machineRecipe : machineRecipes) {
            if(appendMachineRecipe(writer, machineRecipe)) {
                dumped++;
            }
        }

        writer.close();
        return dumped;
    }

    public static boolean appendFurnaceRecipe(Writer writer, ItemStack input) throws IOException {
        if(input == null) {
            return false;
        }

        LogHelper.trace("Dumping furnace recipe with input " + input.getUnlocalizedName());

        ItemStack output = FurnaceRecipes.smelting().getSmeltingResult(input);

        if(output == null) {
            LogHelper.info("Tried to dump furnace recipe with empty output. Input: " + input.getUnlocalizedName());
            return false;
        }

        if(!Utils.belongsToMod(input) && !Utils.belongsToMod(output)) {
            return false;
        }

        float experience = FurnaceRecipes.smelting().func_151398_b(input);

        writer.append("type\tFURNACE\n");

        writer.append("input\t" + getDisplayName(input) + "\n");
        writer.append("output\t" + output.stackSize + "\t" + getDisplayName(output) + "\n");
        writer.append("experience\t" + StringUtils.prettyPrintNumber(FurnaceRecipes.smelting().func_151398_b(output), 2, false) + "\n");

        writer.append("\n\n");

        return true;
    }

    public static boolean appendMachineRecipe(Writer writer, MachineRecipeInfo recipe) throws IOException {
        if(recipe == null) {
            return false;
        }

        LogHelper.trace("Dumping " + recipe.getClass().getName());

        if(recipe.hasStackInput()) {
            ItemStack usedInput = getItemStackFromOreDictList(recipe.getInput().getStacks());
            if(usedInput == null) {
                LogHelper.info("Tried to dump " + recipe.getClass().getSimpleName() + " with an empty ItemStackIdentifier input due to empty oredict, will be ignored. Set LogLevel to debug to see more information");
                LogHelper.debug("OreDict: " + recipe.getInput().getOreDict());
                LogHelper.debug("Original stack size: " + recipe.getInput().getStacks().size());
                return false;
            }

            writer.append("input\t" + getDisplayName(usedInput) + "\n");
        }

        writer.append("type\t" + recipe.getClass().getSimpleName() + "\n");
        writer.append("ept\t" + recipe.getEnergyPerTick() + "\n");
        writer.append("time\t" + recipe.getTime() + "\n");

        if(recipe.hasStackResult()) {
            writer.append("output\t" + recipe.getResult().stackSize + "\t" + getDisplayName(recipe.getResult()) + "\n");
        }

        int upgradeConditions = 0;
        for(BaseRecipeCondition cond : recipe.getConditions()) {
            // Support for new conditions has to be added here manually
            if(cond instanceof UpgradeRecipeCondition) {
                writer.append("upgrade" + upgradeConditions + "\t" + getDisplayName(((UpgradeRecipeCondition) cond).upgrade.getItemStack())  + "\n");
                // Allows for multiple upgrade conditions
                upgradeConditions++;
            }
        }

        if(recipe instanceof WireMillRecipeInfo) {
            writer.append("resinUsed\t" + ((WireMillRecipeInfo) recipe).getResinUsed() + "\n");
        }

        if(recipe instanceof RubberFormerRecipeInfo) {
            writer.append("resinUsed\t" + ((RubberFormerRecipeInfo) recipe).getResinUsed() + "\n");
        }

        if(recipe instanceof CVDFurnaceRecipeInfo) {
            writer.append("waterUsed\t" + ((CVDFurnaceRecipeInfo) recipe).getWaterUsed() + "\n");
            int catalysts = 0;
            for(CVDFurnaceRecipeInfo.CatalystConfiguration conf : ((CVDFurnaceRecipeInfo) recipe).getCatalysts()) {
                writer.append("catalyst" + catalysts + "\t" + StringUtils.prettyPrintNumber(conf.used, 2, false) + ";" + conf.optional + ";" + conf.catalyst.localize() + "\n");
                catalysts++;
            }
        }

        if(recipe instanceof HPCutterRecipeInfo) {
            writer.append("minCutter\t" + ((HPCutterRecipeInfo) recipe).minimalCutterTier + "\n");
        }

        if(recipe instanceof ResinExtractorRecipeInfo) {
            writer.append("resinProduced\t" + ((ResinExtractorRecipeInfo) recipe).getResinProduced() + "\n");
        }

        writer.append("\n\n");
        return true;
    }

    public static boolean appendCraftingRecipe(Writer writer, IRecipe recipe) throws IOException, IllegalAccessException {
        RecipeSorter.Category type = RecipeSorter.getCategory(recipe);

        if(type == RecipeSorter.Category.UNKNOWN) {
            return false;
        }

        ItemStack output = recipe.getRecipeOutput();

        if(output == null) {
            LogHelper.debug("Skipping Recipe with null output " + recipe.getClass().getName());
            return false;
        }

        int size = recipe.getRecipeSize();
        int width = 0;
        List<ItemStack> input = new ArrayList<ItemStack>();

        if(recipe instanceof ShapedRecipes) {
            Collections.addAll(input, ((ShapedRecipes) recipe).recipeItems);
            width = ((ShapedRecipes) recipe).recipeWidth;
        }
        else if(recipe instanceof ShapelessRecipes) {
            input.addAll(((ShapelessRecipes) recipe).recipeItems);
        }
        else if(recipe instanceof ShapedOreRecipe || recipe instanceof ShapelessOreRecipe) {
            List<Object> oreInput = new ArrayList<Object>();

            if(recipe instanceof ShapedOreRecipe) {
                Collections.addAll(oreInput, ((ShapedOreRecipe) recipe).getInput());
                width = ReflectionHelper.findField(ShapedOreRecipe.class, "width").getInt(recipe);
            }
            else {
                oreInput.addAll(((ShapelessOreRecipe) recipe).getInput());
            }
            for(Object obj : oreInput) {
                if(obj instanceof List) {
                    ItemStack usedInput = getItemStackFromOreDictList(((List<ItemStack>) obj));
                    if(usedInput == null) {
                        LogHelper.info("Tried to dump ShapedOreRecipe with an empty oreDict entry, will be ignored. Set LogLevel to debug to see more information");
                        LogHelper.debug("Recipe output: " + output.getUnlocalizedName());
                        return false;
                    }

                    input.add(usedInput);
                }
                else {
                    if(obj instanceof ItemStack) {
                        input.add(((ItemStack) obj).copy());
                    }
                    else {
                        // Shaped recipes require null values where there is not item in the crafting grid
                        input.add(null);
                    }
                }
            }
        }
        else {
            LogHelper.debug("Skipping unsupported " + recipe.getClass().getName());
            return false;
        }

        boolean hasModItem = false;
        if(Utils.belongsToMod(output)) {
            hasModItem = true;
        }

        for(ItemStack stack : input) {
            if(Utils.belongsToMod(stack)) {
                hasModItem = true;
            }
        }

        if(!hasModItem) {
            LogHelper.trace(type + " recipe w/o mod item:");
            LogHelper.trace("\toutput: " + output.getUnlocalizedName());
            for(ItemStack stack : input) {
                LogHelper.trace("\t" + (stack == null? "null" : stack.getUnlocalizedName()));
            }
            return false;
        }

        LogHelper.trace("Dumping " + type + " " + recipe.getClass().getName() + " with output " + output.getUnlocalizedName());

        writer.append("type\t" + type + "\n");
        writer.append("size\t" + recipe.getRecipeSize() + "\n");
        if(width > 0) {
            writer.append("width\t" + width + "\n");
        }
        writer.append("output\t" + output.stackSize + "\t" + getDisplayName(output) + "\n");

        int i = 0;
        for(ItemStack stack : input) {
            if(stack != null) {
                /*
                * Math.floor(i / width) is the row
                * i % width is the column in the crafting grid
                */
                writer.append(i + "\t" + getDisplayName(stack) + "\n");
            }
            i++;
        }

        writer.append("\n\n");

        return true;
    }

    protected static ItemStack getItemStackFromOreDictList(List<ItemStack> list) {
        /**
         * Only one oredict item appears in dump
         * First priority are this mod's items then minecraft then everything else ordered by when they where added to the list (FIFO)
         */
        boolean foundMinecraftItem = false;
        ItemStack usedInput = null;
        for(ItemStack stack : list) {
            if(stack != null) {
                if(Utils.belongsToMod(stack)) {
                    usedInput = stack;
                    break;
                }

                if(!foundMinecraftItem && Utils.belongsToMod(stack, "minecraft")) {
                    foundMinecraftItem = true;
                    usedInput = stack;
                }

                // Do not override if an input was found, items inserted first will have a higher priority
                if(usedInput == null) {
                    usedInput = stack;
                }
            }
        }

        return usedInput;
    }

    public static int dumpBlocks(File dir) throws IOException, NoSuchFieldException, IllegalAccessException {
        File file = new File(dir, "blocks.txt");
        dir.mkdirs();

        if(!file.exists()) {
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(file);
        writer.write("");

        int dumped = 0;
        for(Map.Entry<String, List<ItemStack>> entry : Utils.getBlocksFromMod().entrySet()) {
            for(ItemStack stack : entry.getValue()) {
                if(appendBlock(writer, entry.getKey(), stack)) {
                    dumped++;
                }
            }
        }

        writer.close();

        return dumped;
    }


    public static int dumpItems(File dir) throws IOException {
        File file = new File(dir, "items.txt");
        dir.mkdirs();

        if(!file.exists()) {
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(file);
        writer.write("");

        int dumped = 0;
        for(Map.Entry<String, List<ItemStack>> entry : Utils.getItemsFromMod().entrySet()) {
            for(ItemStack stack : entry.getValue()) {
                if(appendItem(writer, entry.getKey(), stack)) {
                    dumped++;
                }
            }
        }

        writer.close();
        return dumped;
    }

    public static boolean appendBlock(Writer writer, String key, ItemStack stack) throws IOException, NoSuchFieldException, IllegalAccessException {
        Item item = stack.getItem();
        Block block = Block.getBlockFromItem(stack.getItem());
        Class clazz = Block.class;

        writer.append(getDisplayName(stack) + "\n");

        String type = (block.isBlockNormalCube()? "Solid " : "") + "Block";

        int fuelValue = GameRegistry.getFuelValue(stack);
        if(fuelValue > 0) {
            type = "Fuel";
        }

        if(block instanceof BlockFluid) {
            type = "Fluid";
        }

        if(block instanceof BlockMachine && block.hasTileEntity(stack.getItemDamage())) {
            type = "Machine";
        }

        if(block instanceof BlockTankComponent) {
            type = "Tank Component";
        }

        if(block instanceof BlockOre) {
            type = "Ore";
        }

        if(ModBlocks.isBuildingBlock(block)) {
            type = "Building Block";
        }

        writer.append("type\t" + type + "\n");
        writer.append("ID\t" + key + ":" + stack.getItemDamage() + "\n");
        writer.append("unlocalizedName\t" + stack.getUnlocalizedName() + "\n");
        writer.append("maxStackSize\t" + stack.getMaxStackSize() + "\n");
        writer.append("stackable\t" + stack.isStackable() + "\n");
        // For colors see http://minecraft.gamepedia.com/Formatting_codes#Color_codes
        writer.append("rarity\t" + stack.getRarity() + "\n");

        if(fuelValue > 0) {
            writer.append("fuelvalue\t" + StringUtils.prettyPrintNumber(fuelValue / FuelHandler.ONE_ITEM_BURN_TIME, 1, false) + "\n");
        }

        if(block instanceof BlockFluid) {
            writer.append("tool\tbucket\n");
        }
        else {
            writer.append("tool\t" + block.getHarvestTool(stack.getItemDamage()) + "\n");
        }
        /* Wood:    0
         * Stone:   1
         * Iron:    2
         * Diamond: 3
         * Gold:    0 (4?) */
        writer.append("toolLevel\t" + block.getHarvestLevel(stack.getItemDamage()) + "\n");
        writer.append("opaque\t" + block.isOpaqueCube() + "\n");
        // 255 for fully opaque blocks and zero for transparent ones
        writer.append("lighOpacity\t" + block.getLightOpacity() + "\n");
        writer.append("lightValue\t" + block.getLightValue() + "\n");
        // 0 = free, 1 = can't push but pistons can move over, 2 = total immobility and stop pistons
        writer.append("mobility\t" + block.getMobilityFlag() + "\n");

        Field hardness = ReflectionHelper.findField(clazz, "blockHardness", "field_149782_v");
        writer.append("hardness\t" + new DecimalFormat("0.##").format(hardness.getFloat(block)) + "\n");

        Field resistance = ReflectionHelper.findField(clazz, "blockResistance", "field_149781_w");
        writer.append("resistance\t" + new DecimalFormat("0.##").format(resistance.getFloat(block)) + "\n");
        // Default 0.6
        writer.append("slipperiness\t" + block.slipperiness + "\n");
        // List of semicolon seperated oreDict values
        writer.append("oreDict\t" + getOreDictEntries(stack) + "\n");

        TileEntity tile = block.createTileEntity(null, stack.getItemDamage());
        if(tile instanceof IUpgradeable) {
            IUpgradeable upgradeable = (IUpgradeable) tile;

            int i = 0;
            for(UpgradeType upgradeType : upgradeable.getSupportedUpgrades()) {
                Upgrade upgrade = Upgrade.getUpgrade(upgradeType);

                writer.append("acceptedUpgrade" + i + "\t" + upgrade.getItemStack().getDisplayName() + "\n");
                i++;
            }
        }

        writer.append("\n\n");

        return true;
    }

    public static boolean appendItem(Writer writer, String key, ItemStack stack) throws IOException {
        Item item = stack.getItem();
        if(Block.getBlockFromItem(item) != Blocks.air) {
            return false;
        }

        writer.append(getDisplayName(stack) + "\n");

        String type = "Item";

        int fuelValue = GameRegistry.getFuelValue(stack);
        if(fuelValue > 0) {
            type = "Fuel";
            writer.append("fuelvalue\t" + StringUtils.prettyPrintNumber(fuelValue / FuelHandler.ONE_ITEM_BURN_TIME, 1, false) + "\n");
        }

        if(item.isDamageable() || item instanceof ItemWrench) {
            type = "Tool";
        }

        if(HPTRegistry.isCutter(stack)) {
            type = "Cutter";
        }

        if(item instanceof ItemIngot) {
            type = "Ingot";
        }

        if(item instanceof ItemBucket) {
            type = "Bucket";
        }

        if(item instanceof ItemUpgrade) {
            type = "Upgrade";

            writer.append("upgradeLimit\t" + ItemUpgrade.getUpgrade(stack).getLimit() + "\n");

            int i = 0;
            for(Map.Entry<String, List<ItemStack>> entry : Utils.getBlocksFromMod().entrySet()) {
                for(ItemStack entryStack : entry.getValue()) {
                    TileEntity tile = Block.getBlockFromItem(entryStack.getItem()).createTileEntity(null, entryStack.getItemDamage());
                    if(tile instanceof IUpgradeable) {
                        for(UpgradeType upgradeType : ((IUpgradeable) tile).getSupportedUpgrades()) {
                            if(ItemUpgrade.getUpgradeType(stack.getItemDamage()) == upgradeType) {
                                writer.append("upgradeAcceptedBy" + i + "\t" + entryStack.getDisplayName() + "\n");
                                i++;
                                break;
                            }
                        }
                    }
                }
            }
        }

        if(item instanceof ItemWire) {
            type = "Wire";
        }

        if(item instanceof ItemCircuitBoard) {
            type = "Circuit Board";
        }

        if(item instanceof ItemDust) {
            type = "Dust";
        }

        writer.append("ID\t" + key + ":" + stack.getItemDamage() + "\n");
        writer.append("type\t" + type + "\n");

        FluidStack fluid = FluidContainerRegistry.getFluidForFilledItem(new ItemStack(item));
        if(fluid != null) {
            writer.append("fluid\t" + FluidUtils.getFluidName(fluid.getFluid()) + "\n");
        }

        writer.append("unlocalizedName\t" + stack.getUnlocalizedName() + "\n");
        writer.append("maxStackSize\t" + stack.getMaxStackSize() + "\n");
        writer.append("stackable\t" + stack.isStackable() + "\n");
        writer.append("damageable\t" + stack.isItemStackDamageable() + "\n");
        writer.append("maxDamage\t" + stack.getMaxDamage() + "\n");
        writer.append("enchantable\t" + stack.isItemEnchantable() + "\n");
        writer.append("enchantability\t" + item.getItemEnchantability(stack) + "\n");
        writer.append("rarity\t" + stack.getRarity() + "\n");
        writer.append("oreDict\t" + getOreDictEntries(stack) + "\n");
        writer.append("\n\n");

        return true;
    }

    private static String getOreDictEntries(ItemStack stack) {
        List<String> list = Utils.getOreDictEntries(stack);
        String result = "";
        for(int i = 0; i < list.size(); i++) {
            String entry = list.get(i);
            if(i != 0) {
                result += ";";
            }

            result += entry;
        }

        return result;
    }

    public static String getDisplayName(ItemStack stack) {
        String name = stack.getDisplayName();
        if(displayNameMap.containsKey(name)) {
            name = displayNameMap.get(name);
        }
        return name;
    }

}
