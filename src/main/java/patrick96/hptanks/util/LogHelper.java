/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.util;

import cpw.mods.fml.common.FMLLog;
import org.apache.logging.log4j.Level;
import patrick96.hptanks.lib.Reference;

public class LogHelper {
    
    /** The most specific log level the logger should still log */
    public static Level logLevel = Level.DEBUG;

    public static void log(Level level, Object msg) {
        if(level.isAtLeastAsSpecificAs(logLevel)) {
            FMLLog.log(Reference.MOD_NAME, level, String.valueOf(msg));
        }
    }
    
    public static void fatal(Object msg) {
        log(Level.FATAL, msg);
    }
    
    public static void error(Object msg) {
        log(Level.ERROR, msg);
    }
    
    public static void warn(Object msg) {
        log(Level.WARN, msg);
    }
    
    public static void info(Object msg) {
        log(Level.INFO, msg);
    }
    
    public static void debug(Object msg) {
        log(Level.DEBUG, msg);
    }
    
    public static void trace(Object msg) {
        log(Level.TRACE, msg);
    }

    public static void setLogLevel(Level level) {
        LogHelper.logLevel = level;

        LogHelper.info("Log Level set to " + LogHelper.logLevel.name());
    }

    public static Level getLevelByInt(int intLevel) {
        switch(intLevel) {
            case 0:
                return Level.OFF;

            case 1:
                return Level.FATAL;

            case 2:
                return Level.ERROR;

            case 3:
                return Level.WARN;

            case 4:
                return Level.INFO;

            case 5:
                return Level.DEBUG;

            case 6:
                return Level.TRACE;

            default:
                return Level.ALL;
        }
    }
    
}
