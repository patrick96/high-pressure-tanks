/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.util;

import buildcraft.api.tools.IToolWrench;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Optional;
import net.minecraft.item.Item;
import net.minecraft.world.IBlockAccess;
import patrick96.hptanks.items.ItemWrench;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.tile.hpt.IHPTFrameComponent;
import patrick96.hptanks.tile.hpt.IHPTWallComponent;
import patrick96.hptanks.tile.hpt.TileEntityTankComponent;
import patrick96.hptanks.tile.hpt.TileEntityTankControl;

public class Helper {

    public static boolean isIC2Loaded() {
        return Loader.isModLoaded("IC2");
    }
    
    public static boolean isBCEnergyLoaded() {
        return Loader.isModLoaded("BuildCraft|Energy");
    }
    
    public static boolean isWailaLoaded() {
        return Loader.isModLoaded("Waila");
    }
    
    public static boolean isFMPLoaded() {
        return Loader.isModLoaded("McMultipart");
    }
    
    public static boolean isCCLoaded() {
        return Loader.isModLoaded("ComputerCraft");
    }
    
    public static boolean isComponent(IBlockAccess world, Coords blockCoords) {
        return blockCoords.getTileEntity(world) instanceof TileEntityTankComponent;
    }
    
    public static boolean isControl(IBlockAccess world, Coords blockCoords) {
        return blockCoords.getTileEntity(world) instanceof TileEntityTankControl;
    }
    
    public static boolean canWrench(Item item) {
        return item instanceof ItemWrench || (Loader.isModLoaded("Buildcraft|Core") && isBcWrench(item));
    }

    /**
     * Method to check whether the given item is a wrench from Buildcraft
     * Warning: only call this method if you're sure BC is loaded
     */
    @Optional.Method(modid= Reference.PLUGIN_BC_DEP)
    public static boolean isBcWrench(Item item) {
        return item instanceof IToolWrench;
    }

    public static boolean isAllowedInFrame(IBlockAccess world, Coords blockCoords) {
        return blockCoords.getTileEntity(world) instanceof IHPTFrameComponent;
    }
    
    public static boolean isAllowedInWalls(IBlockAccess world, Coords blockCoords) {
        return blockCoords.getTileEntity(world) instanceof IHPTWallComponent;
    }
}
