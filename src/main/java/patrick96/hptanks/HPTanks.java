/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.FMLEventChannel;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import patrick96.hptanks.config.ConfigHandler;
import patrick96.hptanks.core.network.PacketHandler;
import patrick96.hptanks.creativetab.CreativeTabHPT;
import patrick96.hptanks.handler.FuelHandler;
import patrick96.hptanks.handler.GuiHandler;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.init.Recipes;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.plugins.ModuleLoader;
import patrick96.hptanks.plugins.buildcraft.ModuleBuildCraft;
import patrick96.hptanks.plugins.cc.ModuleComputerCraft;
import patrick96.hptanks.plugins.exnihilo.ModuleExNihilo;
import patrick96.hptanks.plugins.forgemultipart.ModuleForgeMultiPart;
import patrick96.hptanks.plugins.ic2.ModuleIndustrialCraft2;
import patrick96.hptanks.plugins.waila.ModuleWAILA;
import patrick96.hptanks.proxy.CommonProxy;
import patrick96.hptanks.util.Dumps;
import patrick96.hptanks.util.LogHelper;
import patrick96.hptanks.worldgen.HPTWorldGen;

import java.io.File;
import java.util.Random;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION, dependencies = Reference.MOD_DEPENDENCIES)
public class HPTanks {
    
    @Instance(Reference.MOD_ID)
    public static HPTanks instance;
    
    @SidedProxy(clientSide = Reference.CLIENT_PROXY, serverSide = Reference.SERVER_PROXY)
    public static CommonProxy proxy;
    
    public static CreativeTabs tabsHPT = new CreativeTabHPT(CreativeTabs.getNextID(), Reference.MOD_ID);
    
    public static FMLEventChannel channel;

    public static final Random RANDOM = new Random();

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        LogHelper.info("Starting High Pressure Tanks " + Reference.VERSION);

        channel = NetworkRegistry.INSTANCE.newEventDrivenChannel(Reference.CHANNEL_NAME);
        channel.register(new PacketHandler());
        
        ConfigHandler.init(new File(event.getModConfigurationDirectory(), Reference.MOD_ID + File.separator + Reference.MOD_ID + ".cfg"));
        
        ModBlocks.init();
        ModItems.init();
        
        MinecraftForge.EVENT_BUS.register(proxy);
        
        proxy.registerRenderer();
        
        ModuleLoader.instance.addModule(new ModuleBuildCraft());
        ModuleLoader.instance.addModule(new ModuleForgeMultiPart());
        ModuleLoader.instance.addModule(new ModuleIndustrialCraft2());
        ModuleLoader.instance.addModule(new ModuleWAILA());
        ModuleLoader.instance.addModule(new ModuleExNihilo());
        ModuleLoader.instance.addModule(new ModuleComputerCraft());

        ModuleLoader.instance.preInit();

        Recipes.preInit();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
        
        proxy.registerTileEntities();

        Recipes.init();

        proxy.init();
        
        ModuleLoader.instance.init();
        
        GameRegistry.registerWorldGenerator(new HPTWorldGen(), 999);
        GameRegistry.registerFuelHandler(new FuelHandler());
        
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        ModuleLoader.instance.postInit();
        proxy.postInit();
    }
    
}