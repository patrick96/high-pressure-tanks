/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.worldgen;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.lib.WorldGenInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HPTWorldGen implements IWorldGenerator {
    
    public List<WorldGenerator> oreGens = new ArrayList<WorldGenerator>();
    
    public HPTWorldGen() {
        for(WorldGenInfo.Specs specs : WorldGenInfo.specs) {
            WorldGenerator generator;

            if(specs.overwrite == Blocks.stone) {
                generator = new WorldGenMinable(ModBlocks.ore, specs.meta, specs.maxVeinSize, specs.overwrite);
            }
            else {
                generator = new WorldGenVeinOverride(ModBlocks.ore, specs.meta, specs.maxVeinSize, specs.overwrite);
            }

            oreGens.add(generator);
        }
    }
    
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
        int i = 0;
        for(WorldGenerator gen : oreGens) {
            WorldGenInfo.Specs specs = WorldGenInfo.specs[i];
            if(specs.doGenerate()) {
                genOre(random, chunkX, chunkZ, world, specs, gen);
                i++;
            }
        }
    }
    
    protected void genOre(Random random, int chunkX, int chunkZ, World world, WorldGenInfo.Specs specs, WorldGenerator worldGenerator) {
        int veinsPerChunk = specs.veinsPerChunk;
        int minY = specs.minY;
        int maxY = specs.maxY;

        if(worldGenerator instanceof WorldGenVeinOverride) {
            veinsPerChunk *= 2;
        }

        int count = 0;
        for(int i = 0; i < veinsPerChunk * 10000; i++) {
            if(count >= veinsPerChunk) {
                break;
            }

            int x = chunkX * 16 + random.nextInt(16);
            int y = random.nextInt(maxY - minY) + minY;
            int z = chunkZ * 16 + random.nextInt(16);

            Block block = world.getBlock(x, y, z);

            if(block == specs.overwrite) {
                worldGenerator.generate(world, random, x, y, z);
                count++;
            }
        }
    }
}
