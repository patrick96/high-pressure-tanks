/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.worldgen;

import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenMinable;

import java.util.Random;

public class WorldGenVeinOverride extends WorldGenMinable {

    private Block block, target;
    private int numBlocks;
    private int blockMeta;

    public WorldGenVeinOverride(Block block, int meta, int number, Block target) {
        super(block, meta, number, target);
        this.block = block;
        this.blockMeta = meta;
        this.numBlocks = number;
        this.target = target;
    }

    @Override
    public boolean generate(World world, Random rand, int x, int y, int z) {
        Block block = world.getBlock(x, y, z);
        if(block == target && block.isReplaceableOreGen(world, x, y, z, target)) {
            setBlockAndNotifyAdequately(world, x, y, z, this.block, blockMeta);
            for(int offsetX = -1; offsetX <= 1; offsetX++) {
                for(int offsetY = -1; offsetY <= 1; offsetY++) {
                    for(int offsetZ = -1; offsetZ <= 1; offsetZ++) {
                        if(offsetX == 0 || offsetY == 0 || offsetZ == 0) {
                            continue;
                        }

                        if(world.getBlock(x + offsetX, y + offsetY, z + offsetZ) != target) {
                            numBlocks--;
                            return super.generate(world, rand, x + offsetX, y + offsetY, z + offsetZ);
                        }
                    }
                }
            }
        }
        else {
            for(int offsetX = -1; offsetX <= 1; offsetX++) {
                for(int offsetY = -1; offsetY <= 1; offsetY++) {
                    for(int offsetZ = -1; offsetZ <= 1; offsetZ++) {
                        if(offsetX == 0 || offsetY == 0 || offsetZ == 0) {
                            continue;
                        }

                        if(world.getBlock(x + offsetX, y + offsetY, z + offsetZ) == target) {
                            return generate(world, rand, x + offsetX, y + offsetY, z + offsetZ);
                        }
                    }
                }
            }
        }

        return super.generate(world, rand, x, y, z);
    }
}
