/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.handler;

import cpw.mods.fml.common.IFuelHandler;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.init.ModItems.ItemPlain;
import patrick96.hptanks.util.Utils;

public class FuelHandler implements IFuelHandler {

    public static final int ONE_ITEM_BURN_TIME = TileEntityFurnace.getItemBurnTime(new ItemStack(Items.coal)) / 8;
    
    public static final int GRAPHITE_ITEMS_SMELTED = 12, BLOCK_SMELTED_MULTIPLIER = 9;
    
    
    @Override
    public int getBurnTime(ItemStack fuel) {
        if(Utils.isItemStackEmpty(fuel)) {
            return 0;
        }
        
        if(Utils.areItemStacksEqualIgnoreStackSize(fuel, ItemPlain.Graphite.getStack())) {
            return ONE_ITEM_BURN_TIME * GRAPHITE_ITEMS_SMELTED;
        }

        if(Utils.areItemStacksEqualIgnoreStackSize(fuel, ModBlocks.compressed.getStack())) {
            return ONE_ITEM_BURN_TIME * GRAPHITE_ITEMS_SMELTED * BLOCK_SMELTED_MULTIPLIER;
        }
        
        return 0;
    }
}
