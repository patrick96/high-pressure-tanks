/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.handler;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import patrick96.hptanks.client.gui.*;
import patrick96.hptanks.inventory.ContainerMachine;
import patrick96.hptanks.inventory.ContainerMiniTank;
import patrick96.hptanks.inventory.ContainerRubberFormer;
import patrick96.hptanks.inventory.ContainerTank;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.tile.TileEntityTank;
import patrick96.hptanks.tile.hpt.TileEntityTankControl;
import patrick96.hptanks.tile.machine.*;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Helper;

public class GuiHandler implements IGuiHandler {
    
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        switch (ID) {
            case GuiInfo.MULTI_BLOCK_TANK_ID:
                //if (Helper.isControl(world, coords) && ((TileEntityTankControl) tile).isPartOfMultiBlock() && ((TileEntityTankControl) tile).wasSynced) {
                if (Helper.isControl(world, coords)) {
                    TileEntityTankControl control = (TileEntityTankControl) tile;
                    return new ContainerTank(player.inventory, control);
                }
                break;
            
            case GuiInfo.MINI_TANK_ID:
                if (tile instanceof TileEntityTank) {
                    TileEntityTank tank = (TileEntityTank) tile;
                    return new ContainerMiniTank(player.inventory, tank);
                }
                break;
            
            case GuiInfo.WIRE_MILL_ID:
            case GuiInfo.RESIN_EXTRACTOR_ID:
            case GuiInfo.CVD_FURNACE_ID:
            case GuiInfo.HP_CUTTER_ID:
                if (tile instanceof TileEntityMachine) {
                    ((TileEntityMachine) tile).markTileDirty();
                    return new ContainerMachine(player.inventory, (TileEntityMachine) tile);
                }
                break;

            case GuiInfo.RUBBER_FORMER_ID:
                if(tile instanceof TileEntityRubberFormer) {
                    ((TileEntityRubberFormer) tile).markTileDirty();
                    return new ContainerRubberFormer(player.inventory, (TileEntityRubberFormer) tile);
                }
                break;

        }
        return null;
    }
    
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        switch (ID) {
            case GuiInfo.MULTI_BLOCK_TANK_ID:
                //if (Helper.isControl(world, coords) && ((TileEntityTankControl) tile).isPartOfMultiBlock() && ((TileEntityTankControl) tile).wasSynced) {
                if (Helper.isControl(world, coords)) {
                    TileEntityTankControl control = (TileEntityTankControl) tile;
                    return new GuiTank(player.inventory, control);
                }
                break;
            
            case GuiInfo.MINI_TANK_ID:
                if (tile instanceof TileEntityTank) {
                    TileEntityTank tank = (TileEntityTank) tile;
                    return new GuiMiniTank(player.inventory, tank);
                }
                break;
            
            case GuiInfo.WIRE_MILL_ID:
                if (tile instanceof TileEntityWireMill) {
                    return new GuiWireMill(player.inventory, (TileEntityWireMill) tile);
                }
                break;
            
            case GuiInfo.RESIN_EXTRACTOR_ID:
                if (tile instanceof TileEntityResinExtractor) {
                    return new GuiResinExtractor(player.inventory, (TileEntityResinExtractor) tile);
                }
                break;

            case GuiInfo.CVD_FURNACE_ID:
                if(tile instanceof TileEntityCVDFurnace) {
                    return new GuiCVDFurnace(player.inventory, (TileEntityCVDFurnace) tile);
                }
                break;

            case GuiInfo.HP_CUTTER_ID:
                if(tile instanceof TileEntityHPCutter) {
                    return new GuiHPCutter(player.inventory, (TileEntityHPCutter) tile);
                }
                break;

            case GuiInfo.RUBBER_FORMER_ID:
                if(tile instanceof TileEntityRubberFormer) {
                    return new GuiRubberFormer(player.inventory, (TileEntityRubberFormer) tile);
                }
                break;

        }
        return null;
    }
}
