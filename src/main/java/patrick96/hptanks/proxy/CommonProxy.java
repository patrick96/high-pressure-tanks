/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.proxy;

import com.mojang.realmsclient.gui.ChatFormatting;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.config.ConfigHandler;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.recipe.HPCutterRecipeInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.tile.TileEntityFluidPump;
import patrick96.hptanks.tile.TileEntityTank;
import patrick96.hptanks.tile.hpt.*;
import patrick96.hptanks.tile.machine.*;
import patrick96.hptanks.util.LogHelper;
import patrick96.hptanks.util.StringUtils;

public abstract class CommonProxy {

    public void init() {}

    public void postInit() {}

    public abstract void registerRenderer();

    public void registerTileEntities() {
        GameRegistry.registerTileEntity(TileEntityTankCasing.class, BlockInfo.TANK_CASING_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityTankGlass.class, BlockInfo.TANK_GLASS_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityTankValve.class, BlockInfo.TANK_VALVE_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityTank.class, BlockInfo.TANK_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityTankControl.class, BlockInfo.TANK_CONTROL_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityTankCCPeripheral.class, BlockInfo.TANK_CC_PERIPHERAL_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityWireMill.class, BlockInfo.WIRE_MILL_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityResinExtractor.class, BlockInfo.RESIN_EXTRACTOR_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityCVDFurnace.class, BlockInfo.CVD_FURNACE_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityHPCutter.class, BlockInfo.HP_CUTTER_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityFluidPump.class, BlockInfo.FLUID_PUMP_UNLOCALIZED_NAME);
        GameRegistry.registerTileEntity(TileEntityRubberFormer.class, BlockInfo.RUBBER_FORMER_UNLOCALIZED_NAME);
    }

    @SubscribeEvent
    public void onLivingDrops(LivingDropsEvent event) {
        if(!(event.entity instanceof EntityWither) || !ConfigHandler.witherDropShard.getBoolean()) {
            return;
        }

        // Looting can only give you one extra shard
        int num = HPTanks.RANDOM.nextInt(2) + 1 + (Math.min(3, event.lootingLevel) / 4F > HPTanks.RANDOM.nextFloat()? 1 : 0);
        EntityItem entityitem = new EntityItem(event.entity.worldObj, event.entity.posX, event.entity.posY, event.entity.posZ, ModItems.ItemCutter.NetherStarShard.getStack(num));
        entityitem.delayBeforeCanPickup = 10;
        event.drops.add(entityitem);
    }
}
