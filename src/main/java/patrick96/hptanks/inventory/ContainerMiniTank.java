/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.core.inventory.HPTContainer;
import patrick96.hptanks.tile.TileEntityTank;

public class ContainerMiniTank extends HPTContainer {
    
    private TileEntityTank tank;
    
    public ContainerMiniTank(InventoryPlayer invPlayer, TileEntityTank tankBlock) {
        super(invPlayer);
        tank = tankBlock;
    }
    
    public TileEntityTank getTank() {
        return tank;
    }
    
    @Override
    public boolean canInteractWith(EntityPlayer entityplayer) {
        return tank.isUseableByPlayer(entityplayer);
    }
    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
        ItemStack stack = null;
        Slot slotObject = (Slot) inventorySlots.get(slot);
        // null checks and checks if the item can be stacked (maxStackSize > 1)
        if (slotObject != null && slotObject.getHasStack()) {
            ItemStack stackInSlot = slotObject.getStack();
            stack = stackInSlot.copy();
            if (slot < 9) {
                if (!mergeItemStack(stackInSlot, 9, 36, false)) {
                    return null;
                }
            } else if (!mergeItemStack(stackInSlot, 0, 9, false)) {
                return null;
            }
            
            if (stackInSlot.stackSize == 0) {
                slotObject.putStack(null);
            } else {
                slotObject.onSlotChanged();
            }
            
            if (stackInSlot.stackSize == stack.stackSize) {
                return null;
            }
            slotObject.onPickupFromSlot(player, stackInSlot);
        }
        return stack;
    }
    
    /*
     * -- Old for Non Control GUI public ContainerTank(InventoryPlayer
     * invPlayer, TileEntityTank tankBlock) { this.tank = tankBlock;
     * 
     * for(int x = 0; x < 9; x++) { addSlotToContainer(new Slot(invPlayer, x, 8
     * + 18 * x, 156)); }
     * 
     * for(int y = 0; y < 3; y++) { for(int x = 0; x < 9; x++) {
     * addSlotToContainer(new Slot(invPlayer, x + y * 9 + 9, 8 + 18 * x , 98 + y
     * * 18)); } }
     * 
     * // Input Slot slotNumberInput = addSlotToContainer(new
     * SlotTankContainer(tank, 0, 103, 17)).slotNumber;
     * 
     * // Output Slot slotNumberOutput = addSlotToContainer(new SlotOutput(tank,
     * 1, 151, 17)).slotNumber; }
     * 
     * public TileEntityTank getTank() { return tank; }
     * 
     * @Override public boolean canInteractWith(EntityPlayer entityplayer) {
     * return tank.isUseableByPlayer(entityplayer); }
     * 
     * 
     * 
     * @Override public ItemStack transferStackInSlot(EntityPlayer player, int
     * slot) { ItemStack stack = null; Slot slotObject = (Slot)
     * inventorySlots.get(slot); //null checks and checks if the item can be
     * stacked (maxStackSize > 1) if (slotObject != null &&
     * slotObject.getHasStack()) { ItemStack stackInSlot =
     * slotObject.getStack(); stack = stackInSlot.copy(); if(slot < 36) {
     * if(!((Slot) inventorySlots.get(slotNumberInput)).isItemValid(stack) ||
     * !this.mergeItemStack(stackInSlot, 36, 37, true)) { if(slot < 9) {
     * if(!this.mergeItemStack(stackInSlot, 9, 35, false)) { return null; } }
     * else if(!this.mergeItemStack(stackInSlot, 0, 8, false)) { return null; }
     * } } else if(slot < 9) { if(!this.mergeItemStack(stackInSlot, 9, 36,
     * false)) { return null; } } else if(slot < 36) {
     * if(!this.mergeItemStack(stackInSlot, 0, 9, false)) { return null; } }
     * else { if(!this.mergeItemStack(stackInSlot, 0, 36, false)) { return null;
     * } }
     * 
     * if(stackInSlot.stackSize == 0) { slotObject.putStack(null); } else {
     * slotObject.onSlotChanged(); }
     * 
     * if(stackInSlot.stackSize == stack.stackSize) { return null; }
     * slotObject.onPickupFromSlot(player, stackInSlot); } return stack; }
     */
    
}
