/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.recipe;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.oredict.RecipeSorter;
import patrick96.hptanks.blocks.BlockTank;
import patrick96.hptanks.blocks.hpt.BlockTankControl;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClearControllerDataRecipe implements IRecipe {

    protected ItemStack output = null;

    static {
        RecipeSorter.register(Reference.MOD_ID + ":clearcontrollerdata", ClearControllerDataRecipe.class, RecipeSorter.Category.SHAPELESS, "after:minecraft:shapeless");
    }

    public ClearControllerDataRecipe() {}

    @Override
    public boolean matches(InventoryCrafting inv, World world) {
        int numStacks = 0;
        ItemStack controller = null;
        for(int i = 0; i < inv.getSizeInventory(); i++) {
            ItemStack current = inv.getStackInSlot(i);
            if(!Utils.isItemStackEmpty(current)) {
                numStacks++;
                if(Block.getBlockFromItem(current.getItem()) instanceof BlockTankControl && current.stackTagCompound != null) {
                    controller = current;
                }
            }
        }

        if(numStacks == 1 && !Utils.isItemStackEmpty(controller)) {
            output = controller.copy();
            output.stackSize = 1;
            output.setTagCompound(null);
        }
        else {
            output = null;
        }

        return output != null;
    }

    @Override
    public ItemStack getCraftingResult(InventoryCrafting inv) {
        return output.copy();
    }

    @Override
    public int getRecipeSize() {
        return 1;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return output;
    }

    @SubscribeEvent
    public void onTooltip(ItemTooltipEvent event) {
        if (output != null && ItemStack.areItemStacksEqual(output, event.itemStack)) {
            event.toolTip.add("" + EnumChatFormatting.RED + EnumChatFormatting.ITALIC + StringUtils.localize("misc.hpt.cleardata"));
        }
    }
}