/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.recipe.condition;

import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.tile.machine.TileEntityMachine;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecipeCondition {

    public abstract boolean meetsCondition(TileEntityMachine tile, MachineRecipeInfo recipe);

    /**
     * Two conditions are already equal when they come from the same class
     * Conditions that depend on construction parameters should change this.
     */
    @Override
    public boolean equals(Object obj) {
        return obj != null && getClass() == obj.getClass();
    }

    /* NEI Methods */

    /**
     *
     * @return
     */
    public List<String> getTooltip() {
        return new ArrayList<String>();
    }

    /**
     * Draw an image for this condition
     * Size: 16x16
     * Start at 0,0, will be translated
     */
    public void drawExtras() {}

    /**
     * When return true, implement transfer to actually load some recipe
     * @return Whether or not this condition can be used as a transfer rectangle
     */
    public boolean hasTransferRect() {
        return false;
    }

    public void transfer(boolean usage) {}

    /**
     * Only return true if it is truly necessary, for example when you know there is another recipe using the same
     * input/output and when it is very unlikely that the conditions for both the recipes are met at the same time
     * (i.e. IC2 Upgrade vs normal recipe)
     * @return Whether or not this condition should be taken into account when comparing two {@link MachineRecipeInfo}
     */
    public boolean affectsEquality() {
        return false;
    }
}
