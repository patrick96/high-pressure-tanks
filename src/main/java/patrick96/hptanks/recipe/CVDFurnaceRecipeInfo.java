/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.recipe;

import net.minecraft.item.ItemStack;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.tile.machine.TileEntityCVDFurnace;
import patrick96.hptanks.util.ItemStackIdentifier;
import patrick96.hptanks.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CVDFurnaceRecipeInfo extends MachineRecipeInfo {

    private final List<CatalystConfiguration> catalysts = new ArrayList<CatalystConfiguration>();

    /**
     * Catalyst Missing Time Multiplier
     * The Time is multiplied with this value to the power of optional catalysts missing
     * this is unused if both optional flags are false
     */
    private final float catMissingTimeMult;

    /**
     * The amount of water is used during the process [mB]
     */
    private final int waterUsed;

    /**
     * Constructor for recipes w/o catalysts
     */
    public CVDFurnaceRecipeInfo(ItemStackIdentifier input, ItemStack result, int ept, int time, int waterUsed) {
        this(input, result, ept, time, waterUsed, new ArrayList<CatalystConfiguration>(), 1F);
    }

    /**
     * Constructor for recipes with exactly one catalyst
     */
    public CVDFurnaceRecipeInfo(ItemStackIdentifier input, ItemStack result, int ept, int time, int waterUsed, CatalystConfiguration catalystConfiguration, float catMissingTimeMult) {
        this(input, result, ept, time, waterUsed, Collections.singletonList(catalystConfiguration), catMissingTimeMult);
    }

    /**
     * @param result
     * @param ept [/t]
     * @param time [t]
     * @param waterUsed [mB]
     * @param catalysts an Array of CatalystConfiguration, defining the catalysts used in the recipe
     * @param catMissingTimeMult see above
     */
    public CVDFurnaceRecipeInfo(ItemStackIdentifier input, ItemStack result, int ept, int time, int waterUsed, List<CatalystConfiguration> catalysts, float catMissingTimeMult) {
        super(input, result, ept, time);

        if(catalysts != null && !catalysts.isEmpty()) {
            for(CatalystConfiguration catalystConfiguration : catalysts) {
                if(catalystConfiguration != null) {
                    if(!hasCatalyst(catalystConfiguration.catalyst)) {
                        this.catalysts.add(catalystConfiguration);
                    }
                }
            }
        }

        this.catMissingTimeMult = catMissingTimeMult;

        this.waterUsed = waterUsed;
    }

    public float getCatMissingTimeMult() {
        return catMissingTimeMult;
    }

    public int getWaterUsed() {
        return waterUsed;
    }

    public List<CatalystConfiguration> getCatalysts() {
        return catalysts;
    }

    public boolean hasCatalyst(Catalyst catalyst) {
        for(CatalystConfiguration configuration : this.catalysts) {
            if(configuration.catalyst == catalyst) {
                return true;
            }
        }
        return false;
    }

    public boolean isOptional(Catalyst catalyst) {
        for(CatalystConfiguration config : catalysts) {
            if(config.catalyst == catalyst && config.optional) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isValid() {
        return super.isValid() && waterUsed >= 0 && catMissingTimeMult >= 1 && catalysts.size() <= TileEntityCVDFurnace.getCatalystTypeLimit();
    }

    @Override
    public boolean hasStackInput() {
        return true;
    }

    @Override
    public boolean hasStackResult() {
        return true;
    }

    public static class CatalystConfiguration {

        public final Catalyst catalyst;
        public final boolean optional;
        public final float used;

        public CatalystConfiguration(Catalyst catalyst, float used, boolean optional) {
            this.catalyst = catalyst;
            this.optional = optional;
            this.used = used;
        }

    }

    public enum Catalyst {

        AL2O3(new ItemStackIdentifier(ModItems.dust.getStack()), "Al\u2082O\u2083"),
        IRON(new ItemStackIdentifier("iron_ingot"), "Fe");

        public final ItemStackIdentifier stack;
        public final String shortName;
        public final int limit;

        Catalyst(ItemStackIdentifier stack, String shortName) {
            this(stack, 64, shortName);
        }

        Catalyst(ItemStackIdentifier stack, int limit, String shortName) {
            this.stack = stack;
            this.limit = Math.abs(limit);
            this.shortName = shortName;
        }

        public boolean matches(ItemStack match) {
            return stack.matches(match);
        }

        public int getLimit() {
            return limit;
        }

        public String localize() {
            return StringUtils.localize("catalyst.hpt." + this.name().toLowerCase());
        }

        public String getShortName() {
            return shortName;
        }
    }
}