/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.recipe;

import net.minecraft.item.ItemStack;
import patrick96.hptanks.recipe.condition.BaseRecipeCondition;
import patrick96.hptanks.tile.machine.TileEntityMachine;
import patrick96.hptanks.util.ItemStackIdentifier;
import patrick96.hptanks.util.LogHelper;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HPTRegistry {

    // Generalized methods to fetch RecipeInfos or add recipes
    public static <T extends MachineRecipeInfo> T getRecipeInfo(ItemStack input, List<T> list, TileEntityMachine tile) {
        if(Utils.isItemStackEmpty(input)) {
            return null;
        }

        T result = null;
        for(T info : list) {
            if(info.hasStackInput() && info.getInput().matches(input)) {
                boolean valid = true;
                for(BaseRecipeCondition condition : info.getConditions()) {
                    valid = condition.meetsCondition(tile, info);
                }

                if(valid) {
                    // Overwrites previous valid recipe.
                    result = info;
                }
            }
        }

        return result;
    }

    public static <T extends MachineRecipeInfo> void addRecipe(List<T> list, T info) {
        if (info == null) {
            LogHelper.warn("Invalid Recipe (null)");
            return;
        }
        if(!info.isValid()) {
            String identifier = "<Empty>";
            if(info.hasStackInput()) {
                identifier = info.getInput().toString();
            }
            else if(info.hasStackResult()) {
                if(!Utils.isItemStackEmpty(info.getResult())) {
                    identifier = info.getResult().getUnlocalizedName();
                }
            }
            LogHelper.warn("Invalid Recipe " + info.toString() + ", " + StringUtils.localize(identifier));
            return;
        }

        for(T e : list) {
            if(!e.canCoexist(info)) {
                LogHelper.warn("This Recipe can't be added, it conflicts with an already existing one " + info.toString() + ", " + e.toString());
                return;
            }
        }

        list.add(info);
    }

    /* ######## WireMill ######## */

    /**
     * A Map with all the wire mill recipes
     * key: input
     * value: info (output, energy used etc.)
     */
    public static final List<WireMillRecipeInfo> wireMillRecipes = new ArrayList<WireMillRecipeInfo>();

    public static void addWireMillRecipe(WireMillRecipeInfo info) {
        addRecipe(wireMillRecipes, info);
    }

    public static WireMillRecipeInfo getwireMillResult(ItemStack input, TileEntityMachine tile) {
        return getRecipeInfo(input, wireMillRecipes, tile);
    }

    /* ######## ResinExtractor ######## */

    /**
     * A Map with all the resin extractor recipes
     * key: input
     * value: info (output, energy used etc.)
     */
    public static final List<ResinExtractorRecipeInfo> resinExtractoRecipes = new ArrayList<ResinExtractorRecipeInfo>();

    public static void addResinExtractorRecipe(ResinExtractorRecipeInfo info) {
        addRecipe(resinExtractoRecipes, info);
    }

    public static ResinExtractorRecipeInfo getResinExtractorRecipeInfo(ItemStack input, TileEntityMachine tile) {
        return getRecipeInfo(input, resinExtractoRecipes, tile);
    }

    /**
     * Gets the amount of fluid the input stack produces in the resin extractor
     *
     * @param input the resin extractor input stack
     * @return the amount of fluid it produces [mB]
     */
    public static int getResinExtractorFluidAmount(ItemStack input, TileEntityMachine tile) {
        ResinExtractorRecipeInfo info = getResinExtractorRecipeInfo(input, tile);
        return info == null? 0 : info.getResinProduced();
    }

    /* ######## CVDFurnace ######## */

    public static final List<CVDFurnaceRecipeInfo> cvdFurnaceRecipes = new ArrayList<CVDFurnaceRecipeInfo>();

    public static void addCvdFurnaceRecipe(CVDFurnaceRecipeInfo info) {
        addRecipe(cvdFurnaceRecipes, info);
    }

    public static CVDFurnaceRecipeInfo getCvdFurnaceRecipeInfo(ItemStack input, TileEntityMachine tile) {
        return getRecipeInfo(input, cvdFurnaceRecipes, tile);
    }

    /* ######## HighPrecisionCutter ######## */

    public static final List<HPCutterRecipeInfo> hpCutterRecipes = new ArrayList<HPCutterRecipeInfo>();
    public static final Map<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration> cutters = new HashMap<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration>();

    public static void addHpCutterRecipe(HPCutterRecipeInfo info) {
        addRecipe(hpCutterRecipes, info);
    }

    public static HPCutterRecipeInfo getHpCutterRecipeInfo(ItemStack input, TileEntityMachine tile) {
        return getRecipeInfo(input, hpCutterRecipes, tile);
    }

    /**
     * Registers a new cutter item, with the given configuration
     * Note: NBT Data on the item stack will be ignored
     * @param stack
     * @param cutter
     */
    public static void registerCutter(ItemStackIdentifier stack, HPCutterRecipeInfo.CutterConfiguration cutter) {
        if(cutter != null && cutter.tier > 0 && !cutters.containsKey(stack)) {
            cutters.put(stack, cutter);
        }
    }

    public static boolean isCutter(ItemStack cutter) {
        return getCutter(cutter) != null;
    }

    public static HPCutterRecipeInfo.CutterConfiguration getCutter(ItemStack cutter) {
        if(Utils.isItemStackEmpty(cutter)) {
            return null;
        }

        for(Map.Entry<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration> entry : cutters.entrySet()) {
            if(cutter.isItemStackDamageable()) {
                List<ItemStack> stacks = entry.getKey().getStacks();

                for(ItemStack stack : stacks) {
                    if(stack != null && stack.getItem() == cutter.getItem()) {
                        return entry.getValue();
                    }
                }
            }
            else if(entry.getKey().matches(cutter, false)) {
                return entry.getValue();
            }
        }

        return null;
    }

    public static Map<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration> getCutters(int minTier) {
        Map<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration> cutters = new HashMap<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration>();
        for(Map.Entry<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration> entry : HPTRegistry.cutters.entrySet()) {
            if(entry.getValue().getTier() >= minTier) {
                cutters.put(entry.getKey(), entry.getValue());
            }
        }

        return cutters;
    }

    public static int getMaxCutterTier() {
        int maxTier = 1;
        for(Map.Entry<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration> entry : cutters.entrySet()) {
            if(entry.getValue().tier > maxTier) {
                maxTier = entry.getValue().tier;
            }
        }

        return maxTier;
    }

    /* ######## RubberFormer ######## */

    public static final List<RubberFormerRecipeInfo> rubberFormerRecipes = new ArrayList<RubberFormerRecipeInfo>();

    public static void addRubberFormerRecipe(RubberFormerRecipeInfo info) {
        addRecipe(rubberFormerRecipes, info);
    }

    /**
     * Gets the rubber Former Recipe by result stack
     * doesn't use the generic getRecipeInfo because there is not input
     * @param result the result to search for in the recipes
     * @return the recipe with the given result
     */
    public static RubberFormerRecipeInfo getRubberFormerRecipe(ItemStack result, TileEntityMachine tile) {
        RubberFormerRecipeInfo resultInfo = null;
        for(RubberFormerRecipeInfo info : rubberFormerRecipes) {
            if(Utils.areItemStacksEqualIgnoreStackSize(result, info.getResult())) {
                boolean valid = true;
                for(BaseRecipeCondition condition : info.getConditions()) {
                    valid = condition.meetsCondition(tile, info);
                }

                if(valid) {
                    // Overwrites previous valid recipe.
                    resultInfo = info;
                }
            }
        }

        return resultInfo;
    }

}