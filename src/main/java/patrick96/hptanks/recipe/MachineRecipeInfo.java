/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.recipe;

import net.minecraft.item.ItemStack;
import patrick96.hptanks.recipe.condition.BaseRecipeCondition;
import patrick96.hptanks.util.ItemStackIdentifier;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains info about basic machine recipes:
 * the output and the energy used
 */

/**
 * TODO add toString mehtod
 */
public abstract class MachineRecipeInfo {

    protected ItemStackIdentifier input;
    protected ItemStack result;
    // Energy Per Tick, Time [ticks]
    protected int ept, time;

    /**
     * A list of conditions the machine must meet in order for it to be able to craft this recipe
     * Two recipes with the same input but different conditions can coexist if all the conditions have affectEquality set to false,
     * but if the two recipes contain conditions that can be met simultaneously the recipe that was added LATER will be chosen.
     */
    protected List<BaseRecipeCondition> conditions = new ArrayList<BaseRecipeCondition>();

    /**
     * @param ept Energy per tick
     */
    public MachineRecipeInfo(ItemStackIdentifier input, ItemStack result, int ept, int time) {
        this.input = Utils.isItemStackEmpty(input) ? null : input.copy();
        this.result = Utils.isItemStackEmpty(result) ? null : result.copy();

        this.ept = ept;
        this.time = time;
    }

    public void addCondition(BaseRecipeCondition condition) {
        if(!conditions.contains(condition)) {
            conditions.add(condition);
        }
    }

    public ItemStackIdentifier getInput() {
        return input;
    }

    public ItemStack getResult() {
        return Utils.isItemStackEmpty(result) ? null : result.copy();
    }

    public int getEnergyPerTick() {
        return ept;
    }

    public int getTime() {
        return time;
    }

    public List<BaseRecipeCondition> getConditions() {
        return conditions;
    }

    /**
     * Method to check whether the info has valid properties (energy usage etc.)
     * It gets called by the Registry to check whether or not the info should be added to the recipe list
     *
     * @return whether or not the recipe has valid values
     */
    public boolean isValid() {
        /**
         * XNOR Operators, so that the recipe is also valid if the recipe has no stack input and the input stack is empty
         */
        return ept > 0 && time > 0 && (hasStackInput() == !Utils.isItemStackEmpty(input)) && (hasStackResult() == !Utils.isItemStackEmpty(result));
    }

    /**
     * @return true if the recipe has an input that is an ItemStack(Identifier)
     * false if the recipe doesn't have an item as input
     */
    public abstract boolean hasStackInput();

    /**
     * @return true if the recipe has an output that is a ItemStack
     * false if the recipe doesn't produce an item
     */
    public abstract boolean hasStackResult();

    /**
     *
     * @param info the recipe info to check against
     * @return true if this recipe and the given one do not conflict in the machine (e.g. same input)
     */
    public boolean canCoexist(MachineRecipeInfo info) {
        // Two Recipes of a different class cannot be in the same machine
        if(getClass() != info.getClass()) {
            return false;
        }

        boolean matches = false;

        if(hasStackInput()) {
            if(getInput().matches(info.getInput())) {
                matches = true;
            }
        }
        else if(hasStackResult()) {
            // If there is no input, the result is the unique identifier
            if(Utils.areItemsEqualIgnoreStackSize(getResult(), info.getResult())) {
                matches = true;
            }
        }

        if(matches) {
            List<BaseRecipeCondition> otherConditions = new ArrayList<BaseRecipeCondition>();
            for(BaseRecipeCondition otherCond : info.getConditions()) {
                if(otherCond != null && otherCond.affectsEquality()) {
                    otherConditions.add(otherCond);
                }
            }

            // Variables will contain only the amount of conditions that do affect equality
            int otherConditionsSize = otherConditions.size();
            int conditionsSize = 0;

            for(BaseRecipeCondition cond : getConditions()) {
                if(cond != null && cond.affectsEquality()) {
                    otherConditions.remove(cond);
                    conditionsSize++;
                }
            }

            // Two recipes can only not coexist based on their Conditions when they have exactly the same conditions that do affect equality
            matches = conditionsSize == otherConditionsSize && otherConditions.size() == 0;
        }

        return !matches;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof MachineRecipeInfo)) {
            return false;
        }

        MachineRecipeInfo info = (MachineRecipeInfo) obj;

        return hasStackInput() == info.hasStackInput() && (!hasStackInput() || getInput().equals(info.getInput()))
                && hasStackResult() == info.hasStackResult() && (!hasStackResult() || ItemStack.areItemStacksEqual(getResult(), info.getResult()));
    }
}
