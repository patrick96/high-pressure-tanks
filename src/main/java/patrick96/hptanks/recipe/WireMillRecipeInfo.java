/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.recipe;

import net.minecraft.item.ItemStack;
import patrick96.hptanks.util.ItemStackIdentifier;

/**
 * Contains info about a wiremill recipe:
 * the result, the amount of resin used and the amount of energy used
 */
public class WireMillRecipeInfo extends MachineRecipeInfo {
    
    protected int resinUsed;

    public WireMillRecipeInfo(ItemStackIdentifier input, ItemStack result, int resinUsed, int time) {
        this(input, result, resinUsed, 8, time);
    }

    public WireMillRecipeInfo(ItemStackIdentifier input, ItemStack result, int resinUsed, int ept, int time) {
        super(input, result, ept, time);
        
        this.resinUsed = resinUsed;
    }
    
    /**
     * @return the resinUsed
     */
    public int getResinUsed() {
        return resinUsed;
    }

    @Override
    public boolean isValid() {
        return super.isValid() && resinUsed >= 0;
    }

    @Override
    public boolean hasStackInput() {
        return true;
    }

    @Override
    public boolean hasStackResult() {
        return true;
    }
}
