/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.InventoryPlayer;
import patrick96.hptanks.client.gui.rect.GuiRectangleEnergy;
import patrick96.hptanks.client.gui.tabs.GuiTab.TabSide;
import patrick96.hptanks.client.gui.tabs.GuiTabEnergyMachine;
import patrick96.hptanks.client.gui.tabs.GuiTabUpgrades;
import patrick96.hptanks.client.gui.tooltip.GuiToolTipEnergyUse;
import patrick96.hptanks.inventory.ContainerMachine;
import patrick96.hptanks.tile.machine.TileEntityMachine;
import patrick96.hptanks.util.StringUtils;

@SideOnly(Side.CLIENT)
public abstract class GuiMachine extends HPTGui {
    
    public final TileEntityMachine machine;

    public GuiMachine(InventoryPlayer invPlayer, TileEntityMachine machine) {
        this(invPlayer, machine, new ContainerMachine(invPlayer, machine));
    }

    public GuiMachine(InventoryPlayer invPlayer, TileEntityMachine machine, ContainerMachine container) {
        super(container, machine);
        this.machine = machine;

        GuiRectangleEnergy energyRect = new GuiRectangleEnergy(this, machine, 8, 8, 12, 59);
        GuiTabEnergyMachine energyTab = new GuiTabEnergyMachine(this, 76, TabSide.RIGHT);
        energyTab.setDynamicToolTip(new GuiToolTipEnergyUse(machine));
        GuiTabUpgrades upgradeTab = new GuiTabUpgrades(this, TabSide.RIGHT, machine);
        
        tabManager.addTab(energyTab).addTab(upgradeTab);
        rectManager.addRect(energyRect);
        
        /*GuiTab settingsTab = new GuiTab(this, 128, 106, TabSide.RIGHT, new Color(0xFF374868));
        settingsTab.addToolTip("Settings");
        GuiTab rsTab = new GuiTab(this, 128, 106, TabSide.RIGHT, new Color(0xFFB3312C));
        rsTab.addToolTip("Redstone Control");
        rsTab.setTitle("Redstone Control");
        
        settingsTab.setIcon(new IconTexture(Items.wrench.getIconFromDamage(0), true));
        rsTab.setIcon(new IconTexture(Item.redstone.getIconFromDamage(0), true));
        
        tabManager.addTab(settingsTab);
        tabManager.addTab(rsTab);*/
    }
    
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        drawString(StringUtils.localize(this.machine.getInventoryName()), 32, 7, 0x404040);
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
    }
}
