/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.InventoryPlayer;
import patrick96.hptanks.client.gui.rect.GuiRectangle;
import patrick96.hptanks.client.gui.rect.GuiRectangleEnergy;
import patrick96.hptanks.client.gui.rect.GuiRectangleFluid;
import patrick96.hptanks.client.gui.tabs.GuiTab.TabSide;
import patrick96.hptanks.client.gui.tabs.GuiTabUpgrades;
import patrick96.hptanks.inventory.ContainerTank;
import patrick96.hptanks.tile.hpt.TileEntityTankControl;

@SideOnly(Side.CLIENT)
public class GuiTank extends HPTGui {
    
    private GuiRectangle controlEnergyRect, controlFluidRect;
    
    protected TileEntityTankControl control;
    
    public GuiTank(InventoryPlayer invPlayer, TileEntityTankControl control) {
        super(new ContainerTank(invPlayer, control), control);
        
        this.control = control;
        
        controlEnergyRect = new GuiRectangleEnergy(this, control, 77, 8, 8, 63, 0.2F);
        controlFluidRect = new GuiRectangleFluid(this, control.getTankManager(), 8, 8, GuiRectangleFluid.FluidScaleType.BIG);
        
        GuiTabUpgrades upgradeTab = new GuiTabUpgrades(this, TabSide.RIGHT, control);
	    /*GuiTab testTab = new GuiTab(this, getXSize() - 16, 24, 0, 0, TabSide.BOTTOM);
		testTab.setResizeable(false);

		List<GuiTab> topTabs = new ArrayList<GuiTab>();
		topTabs.add(new GuiTab(this, 0, 0, TabSide.TOP));
		topTabs.add(new GuiTab(this, 0, 0, TabSide.TOP));
		topTabs.add(new GuiTab(this, 0, 0, TabSide.TOP));
		topTabs.add(new GuiTab(this, 0, 0, TabSide.TOP));
		topTabs.add(new GuiTab(this, 0, 0, TabSide.TOP));
		topTabs.add(new GuiTab(this, 0, 0, TabSide.TOP));*/


        tabManager.addTab(upgradeTab);
        rectManager.addRect(controlEnergyRect);
        rectManager.addRect(controlFluidRect);
	    /*tabManager.addTab(testTab);
	    for(GuiTab tab : topTabs) {
		    tab.setResizeable(false);
		    tabManager.addTab(tab);
	    }*/
    }
    
    @Override
    protected void drawGuiContainerBackgroundLayer(float opacity, int x, int y) {
        if (!control.isPartOfMultiBlock()) {
            bindDefaultTexture();
            return;
        }
        super.drawGuiContainerBackgroundLayer(opacity, x, y);
    }
    
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        if (!control.isPartOfMultiBlock()) {
            return;
        }

        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
    }
}
