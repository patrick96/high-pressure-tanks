/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect;

import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.gui.tooltip.GuiToolTipFluid;
import patrick96.hptanks.core.fluids.TankManager;

public class GuiRectangleFluid extends GuiRectangle {

    protected TankManager tankManager;
    protected FluidScaleType type;

    public GuiRectangleFluid(HPTGui gui, TankManager tankManager, int x, int y, FluidScaleType type) {
        super(gui, x, y, type.w, type.h);

        this.type = type;
        this.tankManager = tankManager;
        
        setDynamicToolTip(new GuiToolTipFluid(tankManager));
    }
    
    @Override
    public void draw() {
        gui.drawFluidDisplay(this, tankManager.getTank());
        gui.drawFluidScale(x, y, type);
    }

    public static enum FluidScaleType {
        SMALL(65, 0, 16, 59),
        BIG(0, 0, 64, 63);

        public final int u, v, w, h;

        private FluidScaleType(int u, int v, int w, int h) {
            this.u = u;
            this.v = v;
            this.w = w;
            this.h = h;
        }
    }
}
