/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect.draw;

import net.minecraft.client.gui.Gui;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.gui.HPTGui;

public class GuiRectColor extends GuiRectDrawType {
    
    public int color;
    
    public GuiRectColor(int color) {
        super();
        
        this.color = color;
    }
    
    @Override
    public void draw(HPTGui gui, int x, int y, int width, int height) {
        GL11.glDisable(GL11.GL_LIGHTING);
        Gui.drawRect(x, y, x + width, y + height, color);
        GL11.glColor4f(1F, 1F, 1F, 1F);
    }
    
}
