/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect.draw;

import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.gui.HPTGui;

public class GuiRectItemStack extends GuiRectDrawType {
    
    ItemStack stack = null;
    
    public GuiRectItemStack(ItemStack stack) {
        super();
        this.stack = stack;
    }
    
    /**
     * @param stack the stack to set
     */
    public void setStack(ItemStack stack) {
        this.stack = stack;
    }
    
    @Override
    public void draw(HPTGui gui, int x, int y, int width, int height) {
        x -= gui.getLeft();
        y -= gui.getTop();
        GL11.glDisable(GL11.GL_LIGHTING);
        gui.renderItemAndEffectIntoGUI(stack, x, y);
        gui.renderItemOverlayIntoGUI(stack, x, y);
        GL11.glEnable(GL11.GL_LIGHTING);
    }
    
}
