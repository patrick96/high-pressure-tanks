/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect;

import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.gui.rect.draw.GuiRectDrawType;
import patrick96.hptanks.client.gui.tooltip.GuiDynamicToolTip;
import patrick96.hptanks.client.util.MouseClickType;

import java.util.ArrayList;
import java.util.List;

// TODO use java.awt.Rectangle for Coords, this way a instance of HPTGui isn't necessary
/**
 * TODO put the isInRect inside a Util class. It should take only a Gui class as param, since it only uses getLeft and getRight
 * It should check for instance of HPTGui to get right and left, if not use 0
 * This way NEI Plugin GUIs don't need an instance of HPTGui
 */
public class GuiRectangle {
    
    protected int x;
    protected int y;
    protected int w;
    protected int h;
    
    protected HPTGui gui;
    
    protected ArrayList<String> toolTip = new ArrayList<String>();
    protected GuiDynamicToolTip dynamicToolTip = null;
    
    protected GuiRectDrawType defaultDrawType = null;
    
    protected ArrayList<GuiRectangle> children = new ArrayList<GuiRectangle>();
    
    protected GuiRectangle parent = null;
    
    public GuiRectangle(HPTGui gui, int x, int y, int w, int h) {
        this(gui, null, x, y, w, h);
    }
    
    public GuiRectangle(HPTGui gui, GuiRectDrawType defaultDrawType, int x, int y, int w, int h) {
        this.gui = gui;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        
        this.defaultDrawType = defaultDrawType;
    }
    
    public void init() {
        for(GuiRectangle rect : children) {
            rect.init();
        }
    };
    
    /**
     * Loops through the child recangles to get a rect that is at those coords, 
     * because the child rectangles overlay the parent one.
     * @param x coords
     * @param y coords
     * @return the rectangle at these coords
     */
    public GuiRectangle getRectAt(int x, int y) {
        for(GuiRectangle rect : children) {
            if(rect.isInRect(x, y)) {
                return rect.getRectAt(x, y);
            }
        }
        
        if(isInRect(x, y)) {
            return this;
        }
        
        return null;
    }
    
    public GuiRectangle addChild(GuiRectangle child) {
        children.add(child);
        child.setParent(this);
        return this;
    }
    
    /**
     * @param parent the parent rectangle to set
     */
    public void setParent(GuiRectangle parent) {
        this.parent = parent;
    }
    
    /**
     * @param toolTip the toolTip to set
     */
    public GuiRectangle setToolTip(ArrayList<String> toolTip) {
        this.toolTip = toolTip;
        return this;
    }
    
    public GuiRectangle addToolTip(String toolTip) {
        if(this.toolTip == null) {
            this.toolTip = new ArrayList<String>();
        }
        
        this.toolTip.add(toolTip);
        return this;
    }
    
    /**
     * @param dynamicToolTip the dynamicToolTip to set
     */
    public void setDynamicToolTip(GuiDynamicToolTip dynamicToolTip) {
        this.dynamicToolTip = dynamicToolTip;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public int getW() {
        return w;
    }
    
    public int getH() {
        return h;
    }
    
    /**
     * @return the gui
     */
    public HPTGui getGui() {
        return gui;
    }
    
    public void draw() {
        draw(defaultDrawType);
    }
    
    public void draw(GuiRectDrawType drawType) {
        draw(drawType, getX(), getY(), getW(), getH());
    }
    
    public void draw(GuiRectDrawType drawType, int posX, int posY, int width, int height) {
        if (drawType != null) {
            drawType(drawType, posX, posY, width, height);
        }

        drawChildren();
    }
    
    public void drawChildren() {
        for(GuiRectangle rect : children) {
            if(rect.drawInBackground()) {
                rect.draw();
            }
        }
    }
    
    public void drawType(GuiRectDrawType type) {
        drawType(type, getX(), getY(), getW(), getH());
    }
    
    public void drawType(GuiRectDrawType type, int posX, int posY, int width, int height) {
        posX += gui.getLeft();
        posY += gui.getTop();
        
        type.draw(gui, posX, posY, width, height);
    }
    
    
    // This shouldn't call mouseOver because that is now handles by the RectManager because this way all the tooltips are overlaying everything else
    public void drawForeground(int mouseX, int mouseY) {
        drawChildrenForeGround(mouseX, mouseY);
    }
    
    public void drawChildrenForeGround(int mouseX, int mouseY) {
        for(GuiRectangle rect : children) {
            if(!rect.drawInBackground()) {
                rect.draw();
            }
            
            rect.drawForeground(mouseX, mouseY);
        }
    }
    
    public void mouseOver(int mouseX, int mouseY) {
        if(isInRect(mouseX, mouseY)) {
            drawToolTip(mouseX, mouseY);
        }
        
        mouseOverChildren(mouseX, mouseY);
    }
    
    public void mouseOverChildren(int mouseX, int mouseY) {
        for(GuiRectangle child : children) {
            child.mouseOver(mouseX, mouseY);
        }
    }
    
    public boolean isInRect(int x, int y) {
        x -= gui.getLeft();
        y -= gui.getTop();
        return x >= getX() && x < getX() + getW() && y >= getY() && y < getY() + getH();
    }
    
    public void drawToolTip(int mouseX, int mouseY) {
        if (isInRect(mouseX, mouseY)) {
            ArrayList<String> list = new ArrayList<String>();
            if(toolTip != null) {
                list.addAll(toolTip);
            }
            
            if(dynamicToolTip != null) {
                List<String> dynToolTip = dynamicToolTip.getToolTip();
                if(dynToolTip != null) {
                    list.addAll(dynToolTip);
                }
            }
            if(list.size() > 0) {
                gui.drawToolTip(list, mouseX + 2 - gui.getLeft(), mouseY + 2 - gui.getTop(), gui.getFontRenderer());
            }
        }
    }
    
    public boolean shouldFireMouseClick(int x, int y, MouseClickType type) {
        return type == MouseClickType.RIGHT || type == MouseClickType.LEFT;
    }
    
    public boolean passClickEventToParent() {
        return true;
    }
    
    public boolean drawInBackground() {
        return true;
    }
    
    public void mouseClicked(int x, int y, MouseClickType type) {
        if(parent != null && passClickEventToParent()) {
            parent.mouseClicked(x, y, type);
        }
    }
    
}
