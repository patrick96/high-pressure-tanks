/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect.draw;

import net.minecraft.util.ResourceLocation;
import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.util.GuiUtils;

public class GuiRectTextured extends GuiRectDrawType {
    
    public int u, v;
    
    public ResourceLocation texture;
    
    public GuiRectTextured(int u, int v) {
        this(u, v, null);
    }
    
    public GuiRectTextured(int u, int v, ResourceLocation texture) {
        super();
        
        this.u = u;
        this.v = v;
        
        this.texture = texture;
    }
    
    @Override
    public void draw(HPTGui gui, int x, int y, int width, int height) {
        if(texture != null) {
            GuiUtils.bindTexture(texture);
        }

        gui.drawTexturedModalRect(x, y, u, v, width, height);
    }
    
}
