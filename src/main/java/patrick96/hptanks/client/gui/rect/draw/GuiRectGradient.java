/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect.draw;

import patrick96.hptanks.client.gui.HPTGui;

public class GuiRectGradient extends GuiRectDrawType {
    
    public int color1, color2;
    
    public boolean sideways = false;
    
    public GuiRectGradient(int color1, int color2) {
        this(color1, color2, false);
    }
    
    public GuiRectGradient(int color1, int color2, boolean sideways) {
        super();
        
        this.color1 = color1;
        this.color2 = color2;
        
        this.sideways = sideways;
    }
    
    @Override
    public void draw(HPTGui gui, int x, int y, int width, int height) {
        if(sideways) {
            gui.drawSideGradientRect(x, y, x + width, y + height, color1, color2);
        }
        else {
            gui.drawGradientRect(x, y, x + width, y + height, color1, color2);
    
        }
    }
}
