/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect;

import net.minecraft.item.ItemStack;
import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.gui.rect.draw.GuiRectDrawType;
import patrick96.hptanks.client.gui.rect.draw.GuiRectItemStack;
import patrick96.hptanks.upgrades.types.Upgrade;

import java.util.ArrayList;

public class GuiRectangleUpgrade extends GuiRectangle {

    public Upgrade upgrade;
    public ItemStack stack;
    
    public GuiRectangleUpgrade(HPTGui gui, int x, int y, Upgrade upgrade) {
        super(gui, null, x, y, 16, 16);
        
        this.upgrade = upgrade;
        
    }
    
    @Override
    public void draw(GuiRectDrawType drawType, int posX, int posY, int width, int height) {
        stack = upgrade.getItemStack();
        drawType = new GuiRectItemStack(stack);
        super.draw(drawType, posX, posY, width, height);
    }
    
    @Override
    public void drawForeground(int mouseX, int mouseY) {
        if(stack != null) {
            ArrayList<String> tooltip = new ArrayList<String>();
            tooltip.add(stack.getDisplayName());
            ArrayList<String> upgradeTooltip = upgrade.getTooltip();
            if(upgradeTooltip != null) {
                tooltip.addAll(upgradeTooltip);
            }
            setToolTip(tooltip);
        }
        super.drawForeground(mouseX, mouseY);
    }
    
    @Override
    public boolean drawInBackground() {
        return false;
    }
}