/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.gui.rect;

import patrick96.hptanks.client.gui.GuiCVDFurnace;
import patrick96.hptanks.client.gui.rect.draw.GuiRectDrawType;
import patrick96.hptanks.client.gui.rect.draw.GuiRectTextured;
import patrick96.hptanks.client.gui.tooltip.GuiToolTipCatalyst;
import patrick96.hptanks.client.util.GuiUtils;
import patrick96.hptanks.recipe.CVDFurnaceRecipeInfo;

public class GuiRectangleCatalyst extends GuiRectangle {

    private CVDFurnaceRecipeInfo.Catalyst cat;

    public GuiRectangleCatalyst(CVDFurnaceRecipeInfo.Catalyst cat, GuiCVDFurnace gui, int x, int y, int w, int h) {
        super(gui, null, x, y, w, h);

        this.cat = cat;
        addToolTip(cat.localize());
        setDynamicToolTip(new GuiToolTipCatalyst(gui.cvdFurnace, cat));
    }

    @Override
    public void draw() {
        gui.drawSplitString(cat.getShortName(), gui.getLeft() + x + 2, gui.getTop() + y + 2, 28, 0xFF202020);
        gui.drawFrame(x + 2, y + 18, 26, 10, 0xFF202020);
        GuiRectDrawType barType = new GuiRectTextured(82, 0, GuiUtils.getGuiTexture("Icons"));
        new GuiRectangle(gui, barType, x + 3, y + 19, ((GuiCVDFurnace) gui).cvdFurnace.getCatalystFilledScaled(cat, 24), 8).draw();
    }
}
