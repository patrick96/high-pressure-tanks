/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.InventoryPlayer;
import patrick96.hptanks.client.gui.rect.GuiRectangleFluid;
import patrick96.hptanks.client.gui.rect.GuiRectangleProgressArrow;
import patrick96.hptanks.tile.machine.TileEntityWireMill;

@SideOnly(Side.CLIENT)
public class GuiWireMill extends GuiMachine {
    
    public final TileEntityWireMill wireMill;
    
    public GuiWireMill(InventoryPlayer invPlayer, TileEntityWireMill wireMill) {
        super(invPlayer, wireMill);
        
        this.wireMill = wireMill;

        GuiRectangleProgressArrow progressArrow = new GuiRectangleProgressArrow(this, machine, 57, 47);
        GuiRectangleFluid fluidRect = new GuiRectangleFluid(this, wireMill.getTankManager(), 152, 8, GuiRectangleFluid.FluidScaleType.SMALL);
        
        rectManager.addRect(progressArrow);
        rectManager.addRect(fluidRect);
    }
}
