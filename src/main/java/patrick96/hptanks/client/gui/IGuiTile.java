/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import patrick96.hptanks.client.gui.tabs.GuiTab.TabSide;
import patrick96.hptanks.lib.GuiInfo;

import java.util.Map;

public interface IGuiTile {

    public GuiInfo getGuiInfo();

    public void handleGuiButtonClick(short buttonId);
    
    /**
     * The int is the index of the tab in the tabManager
     * @return the tabs that are currently open in the GUI for this TE and client
     */
    @SideOnly(Side.CLIENT)
    public Map<TabSide, Integer> getOpenTabs();
    
    /**
     * @param tabs the tabs that are currently opened
     */
    @SideOnly(Side.CLIENT)
    public void setOpenTabs(Map<TabSide, Integer> tabs);
}
