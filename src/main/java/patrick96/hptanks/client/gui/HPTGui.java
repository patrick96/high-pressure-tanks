/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui;

import codechicken.nei.VisiblityData;
import codechicken.nei.api.INEIGuiHandler;
import codechicken.nei.api.TaggedInventoryArea;
import cpw.mods.fml.common.Optional;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidTank;
import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.gui.rect.GuiRectangle;
import patrick96.hptanks.client.gui.rect.GuiRectangleFluid;
import patrick96.hptanks.client.gui.rect.RectManager;
import patrick96.hptanks.client.gui.rect.draw.GuiRectColor;
import patrick96.hptanks.client.gui.rect.draw.GuiRectDrawType;
import patrick96.hptanks.client.gui.rect.draw.GuiRectGradient;
import patrick96.hptanks.client.gui.tabs.GuiTab;
import patrick96.hptanks.client.gui.tabs.GuiTab.TabSide;
import patrick96.hptanks.client.gui.tabs.GuiTabInfo;
import patrick96.hptanks.client.gui.tabs.TabManager;
import patrick96.hptanks.client.util.GuiUtils;
import patrick96.hptanks.client.util.MouseClickType;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.inventory.HPTContainer;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.StringUtils;

import java.awt.*;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Optional.Interface(iface = "codechicken.nei.api.INEIGuiHandler", modid = "NotEnoughItems")
public class HPTGui extends GuiContainer implements INEIGuiHandler {
    
    protected TileEntityHPTBase tile;
    protected IGuiTile guiTile;
    protected HPTContainer hptContainer;
    
    protected Coords guiCoords = new Coords();
    
    protected TabManager tabManager;
    
    protected RectManager rectManager;
    
    public final ResourceLocation texture, icons;
    

    public HPTGui(Container container, TileEntityHPTBase tile) {
        super(container);
        
        this.tile = tile;
        this.guiTile = (IGuiTile) tile;
        guiCoords = tile.getCoords();

        this.hptContainer = (HPTContainer) container;
        
        texture = guiTile.getGuiInfo().getGuiTexture();
        icons = GuiUtils.getGuiTexture("Icons");

        tabManager = new TabManager(this);
        
        rectManager = new RectManager();
        
        if(guiTile.getGuiInfo().hasGuiInformation() && tile instanceof IInventory && StringUtils.hasLocalization(((IInventory) tile).getInventoryName() + ".info")) {
            // You don't need to set the max dimension because they're determined by the amount of text
            GuiTab infoTab = new GuiTabInfo(this, 0, 0, TabSide.LEFT);
            tabManager.addTab(infoTab);
        }
    }
    
    @Override
    public void initGui() {
        super.initGui();
        // All the tabs should be added before that
        tabManager.initTabs();
        rectManager.init();
        
        Map<TabSide, Integer> openTabs = guiTile.getOpenTabs();
        
        if(openTabs != null) {
            for(Entry<TabSide, Integer> e : openTabs.entrySet()) {
                List<GuiTab> tabsOnSide = tabManager.getTabsBySide(e.getKey());
                if(tabsOnSide.size() > e.getValue()) {
                    tabsOnSide.get(e.getValue()).open();
                }
            }
        }
    }

    private long lastFrame;

    /**
     * @return the timer in milliseconds
     */
    protected long getTime() {
        return Sys.getTime() * 1000 / Sys.getTimerResolution();
    }

    /**
     * @return the delta in milliseconds
     */
    public int getDelta() {
        return (int) (getTime() - lastFrame);
    }

    /**
     * @return the hptContainer
     */
    public HPTContainer getHptContainer() {
        return hptContainer;
    }
    
    /**
     * @return the tile
     */
    public TileEntityHPTBase getTile() {
        return tile;
    }
    
    /**
     * @return the guiTile
     */
    public IGuiTile getGuiTile() {
        return guiTile;
    }
    
    /**
     * @return the rectManager
     */
    public RectManager getRectManager() {
        return rectManager;
    }
    
    @Override
    protected void mouseClicked(int mouseX, int mouseY, int button) {
        super.mouseClicked(mouseX, mouseY, button);
        
        MouseClickType type = MouseClickType.getType(button);
        
        rectManager.mouseClicked(mouseX, mouseY, type);
    }

    @Override
    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
        super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
        lastFrame = getTime();
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float opacity, int x, int y) {
        GL11.glColor4f(1F, 1F, 1F, 1.0F);
        bindDefaultTexture();
        
        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
        
        tabManager.draw();
        rectManager.draw();
    }
    
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        rectManager.drawForeGround(mouseX, mouseY);
    }
    
    public List getButtonList() {
        return buttonList;
    }
    
    public int getLeft() {
        return guiLeft;
    }
    
    public int getTop() {
        return guiTop;
    }
    
    public int getXSize() {
        return xSize;
    }
    
    public int getYSize() {
        return ySize;
    }
    
    public float getZLevel() {
        return zLevel;
    }
    
    public void setZLevel(float level) {
        zLevel = level;
    }
    
    public void renderItemOverlayIntoGUI(ItemStack stack, int x, int y) {
        itemRender.renderItemOverlayIntoGUI(getFontRenderer(), mc.getTextureManager(), stack, x, y);
    }
    
    public void renderItemAndEffectIntoGUI(ItemStack stack, int x, int y) {
        itemRender.renderItemAndEffectIntoGUI(getFontRenderer(), mc.getTextureManager(), stack, x, y);
    }
    
    public void bindDefaultTexture() {
        GuiUtils.bindTexture(texture);
    }

    public FontRenderer getFontRenderer() {
        return fontRendererObj;
    }

    // TODO draw tooltip to the left if the string is too long
    public void drawToolTip(List par1List, int x, int y, FontRenderer font) {
        drawHoveringText(par1List, x, y, font);
    }
    
    public void drawSplitString(String str, int x, int y, int maxW, int color) {
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        getFontRenderer().drawSplitString(str, x, y, maxW, color);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
    }
    
    public void drawString(String str, int x, int y, int color) {
        drawString(str, x, y, color, false);
    }
    
    public void drawString(String str, int x, int y, int color, boolean shadow) {
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        getFontRenderer().drawString(str, x, y, color, shadow);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }

    /**
     * Draws a frame at the given coords with a width of 1
     * @param x the x-coordinate of the top left corner
     * @param y the y-coordinate of the top left corner
     * @param w the width of the frame
     * @param h the height of the frame
     * @param color the color of the frame
     */
    public void drawFrame(int x, int y, int w, int h, int color) {
        GuiRectDrawType type = new GuiRectColor(color);

        GuiRectangle top = new GuiRectangle(this, type, x, y, w, 1);
        GuiRectangle bottom = new GuiRectangle(this, type, x, y + h - 1, w, 1);
        GuiRectangle left = new GuiRectangle(this, type, x, y, 1, h);
        GuiRectangle right = new GuiRectangle(this, type, x + w - 1, y, 1, h);

        top.draw();
        bottom.draw();
        left.draw();
        right.draw();
    }
    
    @Override
    public void drawGradientRect(int startX, int startY, int endX, int endY, int startColor, int endColor) {
        super.drawGradientRect(startX, startY, endX, endY, startColor, endColor);
    }

    @Override
    public void drawVerticalLine(int par1, int par2, int par3, int par4) {
        super.drawVerticalLine(par1, par2, par3, par4);
    }

    @Override
    public void drawHorizontalLine(int par1, int par2, int par3, int par4) {
        super.drawHorizontalLine(par1, par2, par3, par4);
    }

    public void drawSideGradientRect(int startX, int startY, int endX, int endY, int startColor, int endColor)
    {
        float f = (float)(startColor >> 24 & 255) / 255.0F;
        float f1 = (float)(startColor >> 16 & 255) / 255.0F;
        float f2 = (float)(startColor >> 8 & 255) / 255.0F;
        float f3 = (float)(startColor & 255) / 255.0F;
        float f4 = (float)(endColor >> 24 & 255) / 255.0F;
        float f5 = (float)(endColor >> 16 & 255) / 255.0F;
        float f6 = (float)(endColor >> 8 & 255) / 255.0F;
        float f7 = (float)(endColor & 255) / 255.0F;
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(f1, f2, f3, f);
        tessellator.addVertex((double)startX, (double)startY, (double)this.zLevel);
        tessellator.addVertex((double)startX, (double)endY, (double)this.zLevel);
        tessellator.setColorRGBA_F(f5, f6, f7, f4);
        tessellator.addVertex((double)endX, (double)endY, (double)this.zLevel);
        tessellator.addVertex((double)endX, (double)startY, (double)this.zLevel);
        tessellator.draw();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }
    
    public void drawDoubleGradientRect(GuiRectangle rect, int startColor, int endColor) {
        drawDoubleGradientRect(rect, startColor, endColor, 0);
    }
    
    /**
     * Draws a to gradients into a rect so that the gradient goes from the middle to both edges.
     *
     * @param rect The rectangle to draw in
     * @param startColor the start color
     * @param endColor the end color
     * @param percentageToIgnore from the middle this percentage of the way and starts drawing after that. Should be between 1 and 0
     */
    public void drawDoubleGradientRect(GuiRectangle rect, int startColor, int endColor, float percentageToIgnore) {
        percentageToIgnore = MathHelper.clamp_float(percentageToIgnore, 0, 1);
        int middle = (int) Math.floor(rect.getW()/ 2D);
        int wLeft = Math.round(middle * (1 - percentageToIgnore));
        int wRight = Math.round((rect.getW() - middle) * (1 - percentageToIgnore));
        
        if(percentageToIgnore > 0) {
            wLeft += 1;
            wRight += 1;
        }
        
        GuiRectangle gradLeftRect = new GuiRectangle(this, rect.getX(), rect.getY(), wLeft, rect.getH());
        GuiRectGradient gradLeft = new GuiRectGradient(startColor, endColor, true);
        
        GuiRectangle gradRightRect = new GuiRectangle(this, rect.getX() + rect.getW() - wRight, rect.getY(), wRight, rect.getH());
        GuiRectGradient gradRight = new GuiRectGradient(endColor, startColor, true);
        
        gradLeftRect.draw(gradLeft);
        gradRightRect.draw(gradRight);
        
        GuiRectangle middleRect = new GuiRectangle(this, rect.getX() + wLeft - 1, rect.getY(), rect.getW() - wLeft - wRight + 2, rect.getH());
        GuiRectColor middleColor = new GuiRectColor(endColor);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        middleRect.draw(middleColor);
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
    }
    
    public void drawEnergyBar(GuiRectangle rect, float val, float maxVal) {
        drawEnergyBar(rect, val, maxVal, 0);
    }
    public void drawEnergyBar(GuiRectangle rect, double val, double maxVal, float percentageToIgnore) {
        int startColor = 0x96000000;
        int endColor = 0x00000000;
        drawProgressBar(rect, val, maxVal, GuiInfo.ENERGY_BACKGROUND_COLOR, GuiInfo.ENERGY_FOREGROUND_COLOR);
    
        drawDoubleGradientRect(rect, startColor, endColor, percentageToIgnore);
    }
    
    public void drawProgressBar(GuiRectangle rect, double val, double maxVal, int backgroundColor, int foregroundColor) {
        int x = rect.getX();
        int y = rect.getY();
        int w = rect.getW();
        int h = rect.getH();

        GuiRectColor foregroundDrawType = new GuiRectColor(foregroundColor);
        GuiRectColor backgroundDrawType = new GuiRectColor(backgroundColor);
        
        rect.draw(backgroundDrawType);
        
        double ratio = maxVal != 0 ? val / maxVal : 0;
        int pixels = (int) Math.round(ratio * h);
        rect.draw(foregroundDrawType, x, y + h - pixels, w, pixels);
    }
    
    public void drawFluidDisplay(GuiRectangle rect, FluidTank tank) {
        int x = rect.getX();
        int y = rect.getY();
        int w = rect.getW();
        int h = rect.getH();

        drawDoubleGradientRect(rect, 0x727272 | 255 << 24, 0xA5A5A5 | 255 << 24);

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        
        if (!FluidUtils.isTankEmpty(tank)) {
            GuiUtils.bindTexture(TextureMap.locationBlocksTexture);
            
            Fluid fluid = tank.getFluid().getFluid();
            
            IIcon icon = FluidUtils.getFluidIcon(fluid);
            
            if (icon == null) {
                return;
            }
            
            /*int iconHeight = icon.getIconHeight();
            int iconWidth = icon.getIconWidth();*/
            int iconWidth, iconHeight;
            iconWidth = iconHeight = 16;
            double amount = tank.getFluidAmount();
            double capacity = tank.getCapacity();
            double filledLevel = capacity != 0 ? amount / capacity : 0;
            
            int fluidPixelsVertical = (int) Math.round(filledLevel * h);
            
            // vertical count
            int vCount = 0;
            while (fluidPixelsVertical > 0) {
                // vertical size
                int vSize = fluidPixelsVertical >= iconHeight ? iconHeight : fluidPixelsVertical;
                fluidPixelsVertical -= vSize;
                
                int pixelsHorizontal = w;
                
                // horizontal count
                int hCount = 0;
                while (pixelsHorizontal > 0) {
                    // horizontal size
                    int hSize = pixelsHorizontal >= iconWidth ? iconWidth : pixelsHorizontal;
                    pixelsHorizontal -= hSize;
                    int minX = guiLeft + x + iconWidth * hCount;
                    int minY = guiTop + y + h - vSize - iconHeight * vCount;
                    Tessellator tessellator = Tessellator.instance;
                    tessellator.startDrawingQuads();
                    tessellator.addVertexWithUV(minX, minY + vSize, zLevel, icon.getMinU(), icon.getMinV());
                    tessellator.addVertexWithUV(minX + hSize, minY + vSize, zLevel, icon.getInterpolatedU(hSize), icon.getMinV());
                    tessellator.addVertexWithUV(minX + hSize, minY, zLevel, icon.getInterpolatedU(hSize), icon.getInterpolatedV(vSize));
                    tessellator.addVertexWithUV(minX, minY, zLevel, icon.getMinU(), icon.getInterpolatedV(vSize));
                    tessellator.draw();
                    //drawTexturedModelRectFromIcon(minX, minY, icon, hSize, vSize);
                    hCount++;
                }
                vCount++;
            }
        }
    }

    public void drawFluidScale(int x, int y, GuiRectangleFluid.FluidScaleType type) {
        GuiUtils.bindTexture(icons);
        drawTexturedModalRect(getLeft() + x, getTop() + y, type.u, type.v, type.w, type.h);
    }

    @Override
    public VisiblityData modifyVisiblity(GuiContainer gui, VisiblityData currentVisibility) {
        // TODO change to true when you figured out how to draw tooltips over the NEI Overlay
        currentVisibility.showNEI = false;
        return currentVisibility;
    }

    @Override
    public Iterable<Integer> getItemSpawnSlots(GuiContainer gui, ItemStack item) {
        return Collections.emptyList();
    }

    @Override
    public List<TaggedInventoryArea> getInventoryAreas(GuiContainer gui) {
        return Collections.emptyList();
    }

    @Override
    public boolean handleDragNDrop(GuiContainer gui, int mousex, int mousey, ItemStack draggedStack, int button) {
        return false;
    }

    @Override
    public boolean hideItemPanelSlot(GuiContainer gui, int x, int y, int w, int h) {
        return rectManager.doesIntersect(new Rectangle(x - guiLeft, y - guiTop, w, h));
    }
}
