/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.tabs;

import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.gui.rect.GuiRectangleUpgrade;
import patrick96.hptanks.client.util.Color;
import patrick96.hptanks.client.util.GuiResizeHelper.ResizeType;
import patrick96.hptanks.client.util.IconTexture;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.util.StringUtils;

import java.util.ArrayList;

public class GuiTabUpgrades extends GuiTab {
    
    protected IUpgradeable upgradeable;
    protected ArrayList<GuiRectangleUpgrade> upgrades = new ArrayList<GuiRectangleUpgrade>();
    
    
    public GuiTabUpgrades(HPTGui gui, TabSide side, IUpgradeable upgradeable) {
        this(gui, 0, 0, side, upgradeable);
    }
    
    public GuiTabUpgrades(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side, IUpgradeable upgradeable) {
        this(gui, w, h,maxW, maxH, side, ResizeType.normal, upgradeable);
    }
    
    public GuiTabUpgrades(HPTGui gui, int maxW, int maxH, TabSide side, IUpgradeable upgradeable) {
        this(gui, 24, 24, maxW, maxH, side, ResizeType.normal, upgradeable);
    }
    
    public GuiTabUpgrades(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side, ResizeType resizeType, IUpgradeable upgradeable) {
        super(gui, w, h, maxW, maxH, side, resizeType, new Color(0xFF219B1F));
        
        this.upgradeable = upgradeable;
        
        setTitle("hpt.gui.tab.upgrades", new Color(0xFF1D75E7));
        addToolTip(getTitle());
        setIcon(new IconTexture(ModItems.wrench.getIconFromDamage(0), true));
        
        this.maxW = 4 * 24 + 16;
    }
    
    @Override
    public void updateSize(int posX, int posY) {
        maxH = Math.max((int) (24 + Math.ceil(upgradeable.getUpgradeManager().getAmount() / 4F) * 24), 48);
        children.clear();
        if(resizeState == ResizeState.OPEN) {
            int i = 0;
            for(Upgrade upgrade : upgradeable.getUpgradeManager().getUpgrades()) {
                GuiRectangleUpgrade rect = new GuiRectangleUpgrade(gui, getX() + i % 4 * 24 + 8, (int) (getY() + Math.floor(i / 4F) * 24 + 24), upgrade);
                children.add(rect);
                i++;
            }
        }
        super.updateSize(posX, posY);
    }
    
    @Override
    public void drawForeground(int mouseX, int mouseY) {
        if(resizeState == ResizeState.OPEN) {
            int amount = upgradeable.getUpgradeManager().getAmount();
            if(amount == 0) {
                gui.drawSplitString(StringUtils.localize("hpt.gui.tab.upgrades.none"), getX() + 8, getY() + 24, maxW - 16, textColor.toInt());
            }
        }
        super.drawForeground(mouseX, mouseY);
    }
    
}
