/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.tabs;

import net.minecraft.client.Minecraft;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.util.Color;
import patrick96.hptanks.client.util.GuiResizeHelper.ResizeType;
import patrick96.hptanks.client.util.IconTexture;
import patrick96.hptanks.util.StringUtils;

import static patrick96.hptanks.util.StringUtils.localize;

public class GuiTabInfo extends GuiTab {
    
    public GuiTabInfo(HPTGui gui, TabSide side) {
        this(gui, 0, 0, side);
    }
    
    public GuiTabInfo(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side) {
        this(gui, w, h,maxW, maxH, side, ResizeType.normal);
    }
    
    public GuiTabInfo(HPTGui gui, int maxW, int maxH, TabSide side) {
        this(gui, 24, 24, maxW, maxH, side, ResizeType.normal);
    }
    
    public GuiTabInfo(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side, ResizeType resizeType) {
        super(gui, w, h, maxW, maxH, side, resizeType, new Color(0xFF727272));
        
        setTitle("hpt.gui.tab.info");
        addToolTip(getTitle());
        setIcon(new IconTexture(Items.book.getIconFromDamage(0), true));
    }
    
    @Override
    public void init() {
        super.init();
        if(gui.getGuiTile() instanceof IInventory) {
            String info = getInfo(); 
            if(!StringUtils.isStringEmpty(info)) {
                this.maxH = Minecraft.getMinecraft().fontRenderer.splitStringWidth(info, maxW - 16) + 24 + 8;
            }
        }
    }
    
    @Override
    public void drawForeground(int mouseX, int mouseY) {
        
        String info = getInfo(); 
        if(resizeState == ResizeState.OPEN && info != null && !info.equals("")) {
            gui.drawSplitString(info, x + 8, y + 24, maxW - 16, textColor.toInt());
        }
        
        super.drawForeground(mouseX, mouseY);
    }
    
    public String getInfo() {
        if(gui.getGuiTile() instanceof IInventory) {
            return localize(((IInventory) gui.getGuiTile()).getInventoryName() + ".info");
        }
        
        return null;
    }
}
