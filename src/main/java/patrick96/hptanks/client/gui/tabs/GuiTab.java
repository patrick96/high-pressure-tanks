/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.tabs;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.lang3.StringUtils;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.gui.IGuiTile;
import patrick96.hptanks.client.gui.rect.GuiRectangle;
import patrick96.hptanks.client.gui.rect.draw.GuiRectDrawType;
import patrick96.hptanks.client.gui.rect.draw.GuiRectTextured;
import patrick96.hptanks.client.util.Color;
import patrick96.hptanks.client.util.GuiResizeHelper.ResizeType;
import patrick96.hptanks.client.util.GuiUtils;
import patrick96.hptanks.client.util.IconTexture;
import patrick96.hptanks.client.util.MouseClickType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static patrick96.hptanks.util.StringUtils.*;

public class GuiTab extends GuiRectangle {
    
    protected int maxW = 124;
    protected int maxH;
    
    /* The values for the "normal" rect (not expanded) are stored here because the rect gets resized */
    protected int minW;
    protected int minH;
    
    
    /**
     * Used for Tabs that are not on the right side
     */
    protected int originX;
    
    protected int milliseconds = 500;

    protected ResizeState resizeState = ResizeState.CLOSED;
    
    protected final TabSide side;
    
    protected final ResizeType resizeType;
    
    protected Color bgColor;
    
    protected String title = null;
    protected boolean isTitleLocalized = false;
    protected Color titleColor = null;
    protected boolean titleShadow = false;
    
    protected Color textColor = new Color(0xFF202020);
    
    protected IconTexture tabIcon = null;
    
    protected TabManager tabManager;
    
    public static ResourceLocation textureRight = GuiUtils.getGuiTexture("TabRight");
    public static ResourceLocation textureLeft = GuiUtils.getGuiTexture("TabLeft");
    public static ResourceLocation textureTop = GuiUtils.getGuiTexture("TabTop");
    public static ResourceLocation textureBottom = GuiUtils.getGuiTexture("TabBottom");
	private boolean resizeable = true;

	public GuiTab(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side) {
        this(gui, w, h, maxW, maxH, side, null);
    }
    
    public GuiTab(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side, Color bgColor) {
        this(gui, w, h,maxW, maxH, side, ResizeType.normal, bgColor);
    }
    
    public GuiTab(HPTGui gui, int maxW, int maxH, TabSide side) {
        this(gui, 24, 24, maxW, maxH, side, null);
    }
    
    public GuiTab(HPTGui gui, int maxW, int maxH, TabSide side, Color bgColor) {
        this(gui, 24, 24, maxW, maxH, side, bgColor);
    }
    
    /**
     * if side == LEFT the x and y coords are meant supposed to be the topmost coords where the tab touches the GUI.
     * @param maxW the width of the rect when fully expanded
     * @param maxH the height of the rect when fully expanded
     */
    public GuiTab(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side, ResizeType resizeType, Color bgColor) {
        super(gui, 0, 0, w, h);
        
        this.maxW = maxW;
        this.maxH = maxH;
        
        this.minW = w;
        this.minH = h;
        
        this.side = side;
        
        this.resizeType = resizeType;
    
        this.bgColor = bgColor;
        
        if(bgColor != null) {
            textColor = bgColor.combine(new Color(0xFF101010));
        }

	    if(side == TabSide.BOTTOM) {
		    y = gui.getYSize();
	    }

	    if(side == TabSide.TOP) {
		    y = -h;
	    }
    }
    
    @Override
    public void init() {
        super.init();

        if(!isStringEmpty(getTitle())) {
            this.maxW = Math.max(Minecraft.getMinecraft().fontRenderer.getStringWidth(getTitle()) + 24 + 24, maxW);
        }
    }
    
    /**
     * @param tabManager the tabManager to set
     */
    public GuiTab setTabManager(TabManager tabManager) {
        this.tabManager = tabManager;
        return this;
    }
    
    public GuiTab setIcon(IconTexture icon) {
        this.tabIcon = icon;
        return this;
    }
    
    /**
     * @param bgColor the bgColor to set
     */
    public GuiTab setBgColor(Color bgColor) {
        this.bgColor = bgColor;
        return this;
    }
    
    public void setTitle(String title) {
        setTitle(title, titleColor);
    }
    
    public void setTitle(String title, int color) {
        setTitle(title, new Color(color));
    }
    
    public void setTitleShadow() {
        setTitleShadow(true);
    }
    
    /**
     * @param titleShadow the titleShadow to set
     */
    public void setTitleShadow(boolean titleShadow) {
        this.titleShadow = titleShadow;
    }

    public void setTitleLocalized(boolean isTitleLocalized) {
        this.isTitleLocalized = isTitleLocalized;
    }

    /**
     * Sets the title.
     *
     * @param title the title to set
     * @param color the title color
     */
    public void setTitle(String title, Color color) {
        this.title = title;
        setTitleColor(color);
    }
    
    /**
     * @param titleColor the titleColor to set
     */
    public void setTitleColor(Color titleColor) {
        if(titleColor != null) {
            this.titleColor = titleColor;
        }
    }

    public String getTitle() {
        return isTitleLocalized? title : localize(title);
    }
    
    /**
     * @return the resize currentState
     */
    public ResizeState getCurrentState() {
        return resizeState;
    }
    
    /**
     * @param resizeState the resizeState to set
     */
    public void setResizeState(ResizeState resizeState) {
        if(resizeState != this.resizeState) {
            this.resizeState = resizeState;
            
            Map<TabSide, ArrayList<GuiTab>> tabs = tabManager.getTabs();
            Map<TabSide, Integer> openTabs = new HashMap<TabSide, Integer>();
            for(Entry<TabSide, ArrayList<GuiTab>> e : tabs.entrySet()) {
                List<GuiTab> tabsSide = e.getValue();
                for(int i = 0; i < tabsSide.size(); i++) {
                    GuiTab tab = tabsSide.get(i);
                    if(tab.getCurrentState() == ResizeState.EXPANDING || tab.getCurrentState() == ResizeState.OPEN) {
                        openTabs.put(e.getKey(), i);
                        break;
                    }
                }
            }
            IGuiTile tile = tabManager.getGui().getGuiTile();
            tile.setOpenTabs(openTabs);
        }
    }
    
    public void draw() {
        draw(x, y, w, h);
    }
    
    public void draw(int posX, int posY, int width, int height) {
        draw(null, posX, posY, width, height);
    }
    
    public void updateSize(int posX, int posY) {
	    if(isHorizontal()) {
		    this.y = posY;
	    }
	    else {
		    this.x = posX;
	    }

		if(!isResizeable()) {
			return;
		}

        float scaledProgress;

        if(resizeState == ResizeState.EXPANDING) {
            if(w < maxW) {
	            scaledProgress = resizeType.getProgress((w - minW) * 1F / (maxW - minW) + gui.getDelta() * 1F / milliseconds);
                /**
                 * Ceil here because very small deltas lead to values which are less than 0.5 pixels higher than before,
                 * so we always ceil to get to the next pixel instead of waiting until a higher delta comes around
                 */
                w = (int) Math.ceil((maxW - minW) * scaledProgress) + minW;
                h = (int) Math.ceil((maxH - minH) * scaledProgress) + minH;
            }
            else {
                setResizeState(ResizeState.OPEN);
            }
        }
        else if(resizeState == ResizeState.SHRINKING) {
            if(w > minW) {
                scaledProgress = resizeType.getProgress((w - minW) * 1F / (maxW - minW) - gui.getDelta() * 1F / milliseconds);
                /**
                 * Floor here because very small deltas lead to values which are less than 0.5 pixels lower than before,
                 * so we always floor to get to the next pixel instead of waiting until a higher delta comes around
                 */
                w = (int) Math.floor((maxW - minW) * scaledProgress) + minW;
                h = (int) Math.floor((maxH - minH) * scaledProgress) + minH;
            }
            else {
                setResizeState(ResizeState.CLOSED);
            }
        }
        
        if(side == TabSide.LEFT) {
            x = originX - w;
        }
        else if(side == TabSide.RIGHT) {
            x = originX;
        }
        
    }

	@Override
    public void draw(GuiRectDrawType drawType, int posX, int posY, int width, int height) {
        GuiRectTextured top = null, bottom = null, shadowTop = null, shadowBottom = null;
        int shadowX = posX, shadowY = posY;
        
        switch(side) {
            case RIGHT:
                GuiUtils.bindTexture(textureRight);
                top = new GuiRectTextured(256 - w, 0);
                bottom = new GuiRectTextured(256 - w, 256 - 4);
                /* The shadow directly next to the "normal" GUI border */
                shadowTop = new GuiRectTextured(0, 0);
                shadowBottom = new GuiRectTextured(0, 256 - 4);
                
                break;
                
            case LEFT:
                GuiUtils.bindTexture(textureLeft);
                top = new GuiRectTextured(0, 0);
                bottom = new GuiRectTextured(0, 256 - 4);
                /* The shadow directly next to the "normal" GUI border */
                shadowTop = new GuiRectTextured(255, 0);
                shadowBottom = new GuiRectTextured(255, 256 - 4);
                
                shadowX = posX + w - 1;
                break;

	        /**
	         * For @TabSide TOP and BOTTOM the rects top and bottom are actually left and right
	         * and the same thing for the shadows
	         */

	        case TOP:
                GuiUtils.bindTexture(textureTop);
		        top = new GuiRectTextured(0, 0);
		        bottom = new GuiRectTextured(256 - 4, 0);

		        shadowTop = new GuiRectTextured(0, 255);
		        shadowBottom = new GuiRectTextured(256 - 4, 255);

		        shadowY = posY + h - 1;
		        break;

	        case BOTTOM:
                GuiUtils.bindTexture(textureBottom);
		        top = new GuiRectTextured(0, 256 - h);
		        bottom = new GuiRectTextured(256 - 4, 256 - h);

		        shadowTop = new GuiRectTextured(0, 0);
		        shadowBottom = new GuiRectTextured(256 - 4, 0);

		        break;

        }
        
        if(bgColor != null) {
            bgColor.setGL11Color();
        }

	    if(isHorizontal(side)) {
		    drawType(top, posX, posY, w, h - 4);
		    drawType(bottom, posX, posY + h - 4, w, 4);

		    drawType(shadowTop, shadowX, shadowY, 1, h - 4);
		    drawType(shadowBottom, shadowX, shadowY + h - 4, 1, 4);
	    }
	    else {
		    drawType(top, posX, posY, w - 4, h);
		    drawType(bottom, posX + w - 4, posY, 4, h);

		    drawType(shadowTop, shadowX, shadowY, w - 4, 1);
		    drawType(shadowBottom, shadowX + w - 4, shadowY, 4, 1);
	    }

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        
        if(tabIcon != null) {
            if(side == TabSide.LEFT) {
                tabIcon.draw(gui, originX - tabIcon.getIconWidth() - 3, y + 4);
            }
            else {
                tabIcon.draw(gui, x + 3, y + 4);
            }
        }
        
        super.draw(drawType, posX, posY, w - 4, h - 4);
    }
    
    @Override
    public void drawChildren() {
        if(resizeState == ResizeState.OPEN) {
            super.drawChildren();
        }
    }
    
    @Override
    public void drawForeground(int mouseX, int mouseY) {
        if(resizeState == ResizeState.OPEN && !isStringEmpty(title)) {
            Color color;
            
            if(titleColor == null) {
                if(bgColor == null) {
                    color = textColor;
                }
                else {
                    color = bgColor.combine(new Color(0xFF101010));
                }
            }
            else {
                color = titleColor;
            }
            
            int titleX;
            if(side == TabSide.RIGHT) {
                titleX = x + 24;
                //titleX = x + w - 8 - gui.getFontRenderer().getStringWidth(title);
            }
            else {
                titleX = x + 8;
            }
            
            gui.drawString(StringUtils.capitalize(getTitle()), titleX, y + 8, color.toInt(), titleShadow);
        }
        
        super.drawForeground(mouseX, mouseY);
    }
    
    @Override
    public void drawChildrenForeGround(int mouseX, int mouseY) {
        if(resizeState == ResizeState.OPEN) {
            super.drawChildrenForeGround(mouseX, mouseY);
        }
    }
    
    @Override
    public void mouseOver(int mouseX, int mouseY) {
        if(resizeState != ResizeState.OPEN) {
            if(isInRect(mouseX, mouseY)) {
                drawToolTip(mouseX, mouseY);
            }
        }
        else {
            mouseOverChildren(mouseX, mouseY);
        }
    }
    
    public void close() {
        if(isResizeable() && resizeState != ResizeState.CLOSED) {
            setResizeState(ResizeState.SHRINKING);
        }
    }
    
    public void open() {
        if(isResizeable() && resizeState != ResizeState.OPEN) {
            setResizeState(ResizeState.EXPANDING);
        }
    }

	public void setResizeable(boolean resizeable) {
		this.resizeable = resizeable;
	}

	public boolean isResizeable() {
		return resizeable;
	}

    public void mouseClicked(int x, int y, MouseClickType type) {
        super.mouseClicked(x, y, type);

	    if(isResizeable()) {
		    tabManager.closeSideExcept(side, this);

		    switch(resizeState) {
			    case EXPANDING:
			    case OPEN:
				    setResizeState(ResizeState.SHRINKING);
				    break;
			    case CLOSED:
			    case SHRINKING:
				    setResizeState(ResizeState.EXPANDING);
				    break;
		    }
	    }
    }
    
    /**
     * @return the max height
     */
    public int getMaxH() {
        return maxH;
    }
    
    /**
     * @return the max width
     */
    public int getMaxW() {
        return maxW;
    }
    
    
    public enum ResizeState {
        CLOSED(),
        EXPANDING(),
        OPEN(),
        SHRINKING()
    }
    
    public enum TabSide {
        RIGHT(),
        LEFT(),
	    // The Tabs at the top and the bottom aren't in the tabManager and don't expand
	    TOP(),
	    BOTTOM();
        
        public static TabSide[] sides = {LEFT, RIGHT, TOP, BOTTOM};
    }

	public static boolean isHorizontal(TabSide side) {
		return side == TabSide.LEFT || side == TabSide.RIGHT;
	}

	private boolean isHorizontal() {
		return isHorizontal(side);
	}
}
