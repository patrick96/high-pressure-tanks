/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.tabs;

import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.gui.tabs.GuiTab.TabSide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TabManager {

    private ArrayList<GuiTab> tabs = new ArrayList<GuiTab>();
    private HashMap<TabSide, ArrayList<GuiTab>> sidedTabs = new HashMap<GuiTab.TabSide, ArrayList<GuiTab>>();
    
    private HPTGui gui;
    
    public TabManager(HPTGui gui) {
        this.gui = gui;
        
        for(TabSide side : TabSide.sides) {
            sidedTabs.put(side, null);
        }
    }
    
    /**
     * After calling this method no more Tabs should be added.
     * Initializes variables for the tabs
     */
    public void initTabs() {
        for(TabSide side : TabSide.sides) {
            // Loads all the tabs into the HashMap
            getTabsBySide(side);
        }

        updateSizes();
        
        for(GuiTab tab : tabs) {
            tab.setTabManager(this);
        }
    }
    
    public TabManager addTab(GuiTab tab) {
        if(tab != null) {
            tabs.add(tab);
            gui.getRectManager().addRect(tab);
        }
        
        return this;
    }
    
    /**
     * Gets the tabs by side.
     * The Tabs are already because their order is determined by order they're added.
     *
     * @param side the side
     * @return the tabs at that side
     */
    public ArrayList<GuiTab> getTabsBySide(TabSide side) {
        if(sidedTabs.get(side) != null) {
            return sidedTabs.get(side);
        }
        ArrayList<GuiTab> tabsOnThisSide = new ArrayList<GuiTab>();
        for(GuiTab tab : tabs) {
            if(tab.side == side) {
                tabsOnThisSide.add(tab);
            }
        }
        
        sidedTabs.put(side, tabsOnThisSide);
        return tabsOnThisSide;
    }
    
    public Map<TabSide, ArrayList<GuiTab>> getTabs() {
        return sidedTabs;
    }
    
    public GuiTab getTabAt(int x, int y) {
        for(GuiTab tab : tabs) {

            if(tab.isInRect(x, y)) {
                return tab;
            }
        }
        
        return null;
    }

    /**
     * Closes all the tabs on one side except for the one specified
     * @param side
     * @param tabAt
     */
    public void closeSideExcept(TabSide side, GuiTab tabAt) {
        for(GuiTab tab : sidedTabs.get(side)) {
            if(!tab.equals(tabAt)) {
                tab.close();
            }
        }
    }

    
    public void closeSide(TabSide side) {
        closeAll(sidedTabs.get(side));
    }
    
    public void closeAll() {
        closeAll(tabs);
    }
    
    public void closeAll(ArrayList<GuiTab> tabs) {
        for(GuiTab tab : tabs) {
            tab.close();
        }
    }
    
    public void updateSizes() {
        for(Map.Entry<TabSide, ArrayList<GuiTab>> side : sidedTabs.entrySet()) {
            int originX = 0;
            switch(side.getKey()) {
                case RIGHT:
                    originX = gui.getXSize();
                    break;
            }

	        int currentY = 0, currentX = 0;
            if(GuiTab.isHorizontal(side.getKey())) {
	            currentY = 8;
            }
	        else {
	            List<GuiTab> tabsRight = getTabsBySide(side.getKey());
	            int width = 0;
	            for(GuiTab tab : tabsRight) {
		            width += tab.getW();
	            }

	            currentX = Math.round((gui.getXSize() - width) / 2F);
            }

            for(GuiTab tab : side.getValue()) {
                tab.originX = originX;
                tab.updateSize(currentX, currentY);
	            if(GuiTab.isHorizontal(side.getKey())) {
		            currentY += tab.getH();
	            }
	            else {
		            currentX += tab.getW();
	            }
            }
        }
    }
    
    public void draw() {
        updateSizes();
    }
    
    /**
     * @return the gui
     */
    public HPTGui getGui() {
        return gui;
    }
}
