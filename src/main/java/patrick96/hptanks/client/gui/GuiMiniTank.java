/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.entity.player.InventoryPlayer;
import patrick96.hptanks.client.gui.rect.GuiRectangleEnergy;
import patrick96.hptanks.client.gui.rect.GuiRectangleFluid;
import patrick96.hptanks.core.network.PacketHandler;
import patrick96.hptanks.core.network.packets.PacketButton;
import patrick96.hptanks.inventory.ContainerMiniTank;
import patrick96.hptanks.tile.TileEntityTank;
import patrick96.hptanks.upgrades.UpgradeType;

@SideOnly(Side.CLIENT)
public class GuiMiniTank extends HPTGui {
    
    protected TileEntityTank tank;
    protected GuiButton ejectButton;
    
    public GuiMiniTank(InventoryPlayer invPlayer, TileEntityTank tank) {
        super(new ContainerMiniTank(invPlayer, tank), tank);
        
        this.tank = tank;
        GuiRectangleEnergy energyRect = new GuiRectangleEnergy(this, tank, 77, 8, 8, 63, 0.2F);
        
        GuiRectangleFluid fluidRect = new GuiRectangleFluid(this, tank.getTankManager(), 8, 8, GuiRectangleFluid.FluidScaleType.BIG);
        
        rectManager.addRect(energyRect);
        rectManager.addRect(fluidRect);
    }
    
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        drawString("Auto Eject", 93, 33, 0x404040);
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
    }
    
    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        
        String ejectButtonText = tank.doEject ? "Enabled" : "Disabled";
        ejectButton = new GuiButton(ButtonIds.BUTTON_AUTO_EJECT, guiLeft + 93, guiTop + 49, 64, 20, ejectButtonText);
        
        if(tank.getUpgradeManager().hasUpgrade(UpgradeType.FLUID_AUTO_EJECT)) {
            buttonList.add(ejectButton);
        }
        
    }
    
    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == ButtonIds.BUTTON_AUTO_EJECT) {
            tank.doEject = !tank.doEject;
            if (!tank.doEject) {
                button.displayString = "Disabled";
            } else {
                button.displayString = "Enabled";
            }
            
            PacketButton packet = new PacketButton(ButtonIds.BUTTON_AUTO_EJECT, guiCoords);
            
            PacketHandler.sendToServer(packet.getPacket(), tile.getWorldObj());
        }
    }
}
