/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.renderer.block;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.blocks.BlockFluidPump;
import patrick96.hptanks.client.renderer.models.ModelFluidPump;
import patrick96.hptanks.client.util.RenderUtils;
import patrick96.hptanks.tile.TileEntityFluidPump;
import patrick96.hptanks.util.Coords;

import java.util.List;

public class FluidPumpRenderer extends SimpleBlockRenderer {
    
    public static int renderId = RenderingRegistry.getNextAvailableRenderId();
    public ModelFluidPump model;
    
    /*
     * These variable names are only accurate for when rotation.ordinal() == 0
     */
    public static float headLayerCount = 4.0F;
    public static float headMaxXZ = 0.5F;
    public static float headMaxY = 0.3F;
    
    public static float headMinXZ = 0.125F;
    public static float headMinY = 0.0F;
    
    public static float connLayerCount = 3.0F;
    public static float connMaxXZ = 0.34375F;
    public static float connMaxY = 0.3F;
    
    public static float connMinXZ = 0.4375F;
    public static float connMinY = 0.0F;
    
    public FluidPumpRenderer() {
        model = new ModelFluidPump();
    }
    
    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {}
    
    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        
        if(tile instanceof TileEntityFluidPump) {
            TileEntityFluidPump pump = (TileEntityFluidPump) tile;
            int meta = world.getBlockMetadata(x, y, z);
            
            // Block bounds for the middle cube
            block.setBlockBoundsForItemRender();
            renderer.setRenderBoundsFromBlock(block);
            renderer.renderStandardBlock(block, x, y, z);
            
            ForgeDirection rotation = pump.getRotation();
            List<ForgeDirection> connections = pump.getConnections();
            renderLayers(rotation, renderer, block, x, y, z, meta, headLayerCount, headMaxXZ, headMaxY, headMinXZ, headMinY, false);

            for(ForgeDirection connection : connections) {
                renderLayers(connection, renderer, block, x, y, z, meta, connLayerCount, connMaxXZ, connMaxY, connMinXZ, connMinY, false);
            }
            
            renderer.setRenderFromInside(false);
        }
        
        return true;
    }
    
    public static void renderLayers(ForgeDirection orientation, RenderBlocks renderer, Block block, int x, int y, int z, int meta, float count, float maxXZ, float maxY, float minXZ, float minY, boolean inv) {
        renderer.setRenderFromInside(false);
        
        for(float i = 0.0F; i < count; i++) {
            switch(orientation.ordinal()) {
                case 0:
                    float currentMinXZ = i / count * (maxXZ - minXZ) + minXZ;
                    block.setBlockBounds(currentMinXZ, i / count * (maxY - minY) + minY, currentMinXZ, 
                            1.0F - currentMinXZ, (i + 1) / count * (maxY - minY) + minY, 1.0F - currentMinXZ);
                    break;
                    
                case 1:
                    float currentMaxXZ = 1.0F - (i / count * (maxXZ - minXZ) + minXZ);
                    block.setBlockBounds(currentMaxXZ, 1.0F - ((i + 1) / count * (maxY - minY) + minY), currentMaxXZ, 
                            1.0F - currentMaxXZ, 1.0F - (i / count * (maxY - minY) + minY), 1.0F - currentMaxXZ);
                    break;
                    
                case 2:
                    float currentMinXY = i / count * (maxXZ - minXZ) + minXZ;
                    block.setBlockBounds(currentMinXY, currentMinXY, i / count * (maxY - minY) + minY, 
                            1.0F - currentMinXY, 1.0F - currentMinXY, (i + 1) / count * (maxY - minY) + minY);
                    break;
                    
                case 3:
                    renderer.setRenderFromInside(true);
                    float currentMaxXY = 1.0F - (i / count * (maxXZ - minXZ) + minXZ);
                    block.setBlockBounds(currentMaxXY, 1.0F - currentMaxXY, 1.0F - ((i + 1) / count * (maxY - minY) + minY), 
                            1.0F - currentMaxXY, currentMaxXY, 1.0F - (i / count * (maxY - minY) + minY));
                    break;
                    
                case 4:
                    float currentMinYZ = i / count * (maxXZ - minXZ) + minXZ;
                    block.setBlockBounds(i / count * (maxY - minY) + minY, currentMinYZ, currentMinYZ, 
                            (i + 1) / count * (maxY - minY) + minY, 1.0F - currentMinYZ, 1.0F - currentMinYZ);
                    break;
                    
                case 5:
                    renderer.setRenderFromInside(true);
                    float currentMaxYZ = 1.0F - (i / count * (maxXZ - minXZ) + minXZ);
                    block.setBlockBounds(1.0F - ((i + 1) / count * (maxY - minY) + minY), 1.0F - currentMaxYZ, currentMaxYZ, 
                            1.0F - (i / count * (maxY - minY) + minY), currentMaxYZ, 1.0F - currentMaxYZ);
                    break;
            }
            
            renderer.setRenderBoundsFromBlock(block);
            
            if(block instanceof BlockFluidPump) {
                renderer.setOverrideBlockTexture(((BlockFluidPump) block).headIcon);
            }
            
            if(inv) {
                RenderUtils.renderStandardInvBlock(renderer, block, meta);
            }
            else {
                renderer.renderStandardBlock(block, x, y, z);
            }
            
            renderer.clearOverrideBlockTexture();
        }
    }

    @Override
    public int getRenderId() {
        return renderId;
    }
}
