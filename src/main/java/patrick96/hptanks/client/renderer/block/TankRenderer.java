/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.renderer.block;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.blocks.hpt.BlockTankControl;
import patrick96.hptanks.client.util.RenderUtils;
import patrick96.hptanks.tile.TileEntityTank;
import patrick96.hptanks.tile.hpt.TileEntityTankControl;
import patrick96.hptanks.util.Coords;

public class TankRenderer extends SimpleBlockRenderer {
    
    public static int currentRenderPass;
    public static int renderId = RenderingRegistry.getNextAvailableRenderId();
    
    public TankRenderer() {}
    
    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        renderer.renderStandardBlock(block, x, y, z);

        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        if (tile instanceof  TileEntityTankControl && ((TileEntityTankControl) tile).isPartOfMultiBlock()) {
            TileEntityTankControl control = (TileEntityTankControl) tile;
            renderMultiBlock(control, renderer);
        } else if (tile instanceof TileEntityTank && !((TileEntityTank) tile).getTankManager().isEmpty()) {
            TileEntityTank tank = (TileEntityTank) tile;
            FluidStack fluid = tank.getTankManager().getTank().getFluid();
            renderer.setRenderBounds(0.01, 0.01, 0.01, 0.99, 0.99 * fluid.amount / tank.getTankCapacity(), 0.99);
            RenderUtils.renderFluidBlock(fluid.getFluid(), renderer, x, y, z);
            renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
        }
        return true;
    }
    
    public void renderMultiBlock(TileEntityTankControl control, RenderBlocks renderer) {
        if (control == null || !control.isPartOfMultiBlock() || control.getTankManager().isEmpty()) {
            return;
        }
        
        GL11.glPushMatrix();
        
        renderer.renderAllFaces = false;
        
        int iy, ix, iz;
        for (ix = 1; ix < control.getDimensions().x - 1; ix++) {
            for (iz = 1; iz < control.getDimensions().z - 1; iz++) {
                BlockTankControl.resetRenderSides();
                BlockTankControl.customRenderSides = true;
                
                if (ix == 1) {
                    renderer.renderMinX = 0.001;
                    BlockTankControl.renderSide(ForgeDirection.WEST, true);
                }
                if (iz == 1) {
                    renderer.renderMinZ = 0.001;
                    BlockTankControl.renderSide(ForgeDirection.NORTH, true);
                }
                if (ix == control.getDimensions().x - 2) {
                    renderer.renderMaxX = 0.999;
                    BlockTankControl.renderSide(ForgeDirection.EAST, true);
                }
                if (iz == control.getDimensions().z - 2) {
                    renderer.renderMaxZ = 0.999;
                    BlockTankControl.renderSide(ForgeDirection.SOUTH, true);
                }
                
                for (iy = 1; iy <= control.getFluidLevel(); iy++) {
                    if(renderer.renderMinX != 0 || renderer.renderMinZ != 0 || renderer.renderMaxX != 1 || renderer.renderMaxZ != 1 || iy == 1) {
                        BlockTankControl.renderSide(ForgeDirection.DOWN, false);
                        BlockTankControl.renderSide(ForgeDirection.UP, false);
                        renderer.renderMaxY = 1;

                        if(iy == 1) {
                            BlockTankControl.renderSide(ForgeDirection.DOWN, true);
                        }
                    }
                    if(iy == control.getFluidLevel()) {
                        BlockTankControl.renderSide(ForgeDirection.UP, true);
                        renderer.renderMaxY = control.getFluidAmount() / control.getFluidPerLevel() - control.getFluidLevel() + 1;
                        if(renderer.renderMaxY == 1) {
                            renderer.renderMaxY = 0.999;
                        }
                    }

                    Fluid fluid = control.getTankManager().getTank().getFluid().getFluid();
                    RenderUtils.renderFluidBlock(fluid, renderer, control.getCornerBlock().add(new Coords(ix, iy, iz)));
                }

                BlockTankControl.resetRenderSides();
                renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
            }
        }
        GL11.glPopMatrix();
    }

    @Override
    public int getRenderId() {
        return renderId;
    }
}