/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.renderer.item;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.common.util.ForgeDirection;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.renderer.block.FluidPumpRenderer;
import patrick96.hptanks.client.util.RenderUtils;

public class ItemFluidPumpRenderer implements IItemRenderer {
    
    public ItemFluidPumpRenderer() {}
    
    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }
    
    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
        return true;
    }
    
    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
        if(item == null) {
            return;
        }
        
        Block block = Block.getBlockFromItem(item.getItem());
        RenderBlocks renderer = new RenderBlocks();
        
        if(block == null) {
            return;
        }
        
        switch (type) {
            case EQUIPPED:              
                GL11.glTranslatef(0.4F, 1F, 0.6F);
                break;
            case EQUIPPED_FIRST_PERSON:
                GL11.glTranslatef(0F, 0.7F, 0.5F);      
                break;
                
            default:
        }

        block.setBlockBoundsForItemRender();
        renderer.setRenderBoundsFromBlock(block);
        RenderUtils.renderStandardInvBlock(renderer, block, item.getItemDamage());
        
        GL11.glPushMatrix();
        
        if(type == ItemRenderType.EQUIPPED_FIRST_PERSON) {
            GL11.glRotatef(180, 0F, 1F, 0);
        }
        
        GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
        
        FluidPumpRenderer.renderLayers(ForgeDirection.WEST, renderer, block, 0, 0, 0, item.getItemDamage(), FluidPumpRenderer.headLayerCount, FluidPumpRenderer.headMaxXZ, FluidPumpRenderer.headMaxY, FluidPumpRenderer.headMinXZ, FluidPumpRenderer.headMinY, true);
        
        //GL11.glPushMatrix();
        GL11.glRotatef(180, 0F, 1F, 0F);
        FluidPumpRenderer.renderLayers(ForgeDirection.WEST, renderer, block, 0, 0, 0, item.getItemDamage(), FluidPumpRenderer.connLayerCount, FluidPumpRenderer.connMaxXZ, FluidPumpRenderer.connMaxY, FluidPumpRenderer.connMinXZ, FluidPumpRenderer.connMinY, true);
        //GL11.glPopMatrix();
        
        GL11.glPopMatrix();
    }
    
}
