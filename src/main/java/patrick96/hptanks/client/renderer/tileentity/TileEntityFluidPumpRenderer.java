/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.renderer.tileentity;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.renderer.models.ModelFluidPump;
import patrick96.hptanks.tile.TileEntityFluidPump;
import patrick96.hptanks.util.Utils;

public class TileEntityFluidPumpRenderer extends TileEntitySpecialRenderer {
    
    public ModelFluidPump model;
    
    public static final ResourceLocation texture = Utils.getResourceLocation("textures/models/ModelFluidPump3.png");
    
    public TileEntityFluidPumpRenderer() {
        model = new ModelFluidPump();
    }
    
    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f) {
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x, (float) y, (float) z);
        bindTexture(texture);
        model.render(((TileEntityFluidPump) tile).getRotation().ordinal(), 0.0625F);
        GL11.glPopMatrix();
    }
    
}
