/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.renderer.ctm;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.blocks.BlockHPT;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.List;

import static patrick96.hptanks.client.renderer.ctm.CTMConfiguration.*;

@SideOnly(Side.CLIENT)
public class CTMHelper {

    /**
     * @return an Array with two values:
     * 0: the texture name w/ all the border
     * 1: the texture name w/o borders
     */
    public static String[] getTextureNames(CTMConfiguration config) {
        if(config.getBlock() instanceof BlockHPT) {
            String name = ((BlockHPT) config.getBlock()).getTextureName();

            List<String> names = new ArrayList<String>();

            names.add(name);
            names.add(name + "NoBorders");

            if(config.getType() == TextureType.CORNER) {
                names.add(name + "Vertical");
                names.add(name + "Horizontal");
            }

            return names.toArray(new String[names.size()]);
        }

        return new String[] {"", ""};
    }

    public static IIcon getConnectedBlockTexture(ICTM block, IBlockAccess blockAccess, int x, int y, int z, int side) {
        Coords coords = new Coords(x, y, z);
        int meta = coords.getBlockMetadata(blockAccess);

        CTMConfiguration config = new CTMConfiguration(block);

        ForgeDirection dir = ForgeDirection.getOrientation(side);

        ForgeDirection rightDir, leftDir, upDir = ForgeDirection.UP, downDir = ForgeDirection.DOWN;
        if(dir != ForgeDirection.UP && dir != ForgeDirection.DOWN) {
            // We have to take the opposite because we're not facing dir but actually the opposite
            rightDir = Utils.getRight(dir).getOpposite();
            leftDir = Utils.getLeft(dir).getOpposite();
        }
        else {
            upDir = ForgeDirection.NORTH;
            downDir = ForgeDirection.SOUTH;
            rightDir = ForgeDirection.EAST;
            leftDir = ForgeDirection.WEST;

        }

        if(dir == ForgeDirection.EAST || dir == ForgeDirection.SOUTH) {
            rightDir = rightDir.getOpposite();
            leftDir = leftDir.getOpposite();
        }

        boolean topLeft = block.shouldConnectToBlock(blockAccess, coords.copy().add(upDir).add(leftDir), meta);
        boolean top = block.shouldConnectToBlock(blockAccess, coords.copy().add(upDir), meta);
        boolean topRight = block.shouldConnectToBlock(blockAccess, coords.copy().add(upDir).add(rightDir), meta);
        boolean right = block.shouldConnectToBlock(blockAccess, coords.copy().add(rightDir), meta);
        boolean bottomRight = block.shouldConnectToBlock(blockAccess, coords.copy().add(downDir).add(rightDir), meta);
        boolean bottom = block.shouldConnectToBlock(blockAccess, coords.copy().add(downDir), meta);
        boolean bottomLeft = block.shouldConnectToBlock(blockAccess, coords.copy().add(downDir).add(leftDir), meta);
        boolean left = block.shouldConnectToBlock(blockAccess, coords.copy().add(leftDir), meta);

        if(top && left) {
            if(topLeft) {
                config.set(0, Type.NONE);
            }
            else {
                config.set(0, Type.CORNER);
            }
        }

        if(!top) {
            config.set(0, Type.BORDER);
        }

        if(top && right) {
            if(topRight) {
                config.set(1, Type.NONE);
            }
            else {
                config.set(1, Type.CORNER);
            }
        }

        if(!right) {
            config.set(1, Type.BORDER);
        }

        if(bottom && right) {
            if(bottomRight) {
                config.set(2, Type.NONE);
            }
            else {
                config.set(2, Type.CORNER);
            }
        }

        if(!bottom) {
            config.set(2, Type.BORDER);
        }

        if(bottom && left) {
            if(bottomLeft) {
                config.set(3, Type.NONE);
            }
            else {
                config.set(3, Type.CORNER);
            }
        }

        if(!left) {
            config.set(3, Type.BORDER);
        }

        IIcon[] icons = block.getIcons();

        if(icons instanceof TextureCTM[]) {
            TextureCTM[] textures = (TextureCTM[]) icons;

            for(TextureCTM texture : textures) {
                if(texture.getConfig().getName().equals(config.getName())) {
                    return texture;
                }
            }
        }

        return null;
    }
}
