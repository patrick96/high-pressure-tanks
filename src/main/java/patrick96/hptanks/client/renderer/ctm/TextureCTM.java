/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.renderer.ctm;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.IResource;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import patrick96.hptanks.config.ConfigHandler;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.util.LogHelper;
import patrick96.hptanks.util.Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class TextureCTM extends TextureAtlasSprite {

    private final CTMConfiguration config;
    private BufferedImage output;

    public TextureCTM(CTMConfiguration config) {
        super(config.getName());
        this.config = config;
    }

    @Override
    public boolean hasCustomLoader(IResourceManager manager, ResourceLocation location) {
        return true;
    }

    @Override
    public boolean load(IResourceManager manager, ResourceLocation location) {
        // Generate the texture from the configuration
        int borderWidth = config.getBorderWidth();

        String[] textures = CTMHelper.getTextureNames(config);

        int w, h;
        BufferedImage borderImage, noBorderImage, verticalBorderImage = null, horizontalBorderImage = null;

        try {
            IResource borderResource = manager.getResource(Utils.getBlockResourceLocationFromTexure(textures[0], "hpt"));

            borderImage = ImageIO.read(borderResource.getInputStream());

            w = borderImage.getWidth();
            h = borderImage.getHeight();

            // If the noBorders resource doesn't exist, set it to a transparent image
            ResourceLocation noBordersResourceLocation = Utils.getBlockResourceLocationFromTexure(textures[1], "hpt");
            if(Utils.resourceExists(noBordersResourceLocation)) {
                IResource noBorderResource = manager.getResource(noBordersResourceLocation);
                noBorderImage = ImageIO.read(noBorderResource.getInputStream());
            }
            else {
                noBorderImage = new BufferedImage(w, h, BufferedImage.TYPE_4BYTE_ABGR);
            }

            int w1 = noBorderImage.getWidth();
            int h1 = noBorderImage.getHeight();

            if(config.getType() == CTMConfiguration.TextureType.CORNER) {
                verticalBorderImage = ImageIO.read(manager.getResource(Utils.getBlockResourceLocationFromTexure(textures[2], "hpt")).getInputStream());
                horizontalBorderImage = ImageIO.read(manager.getResource(Utils.getBlockResourceLocationFromTexure(textures[3], "hpt")).getInputStream());
            }

            if(w != h || w != w1 || w1 != h1 || (config.getType() == CTMConfiguration.TextureType.CORNER && (verticalBorderImage.getWidth() != verticalBorderImage.getHeight() || verticalBorderImage.getWidth() != w || horizontalBorderImage.getWidth() != horizontalBorderImage.getHeight() || horizontalBorderImage.getWidth() != w))) {
                LogHelper.error("Failed to generate connected textures: the image dimensions aren't the same!");
                return true;
            }

            if(borderWidth * 2 > w || (config.getType() == CTMConfiguration.TextureType.CORNER && config.getBlock().getCornerWidth() * 2 > w)) {
                LogHelper.error("Failed to generate connected textures: the border width is too big!");
                return true;
            }


        } catch(IOException e) {
            e.printStackTrace();
            return true;
        }

        output = new BufferedImage(w, h, BufferedImage.TYPE_4BYTE_ABGR);

        output.setRGB(0, 0, w, h, noBorderImage.getRGB(0, 0, w, h, null, 0, w), 0, w);

        int cornerWidth = config.getType() == CTMConfiguration.TextureType.CORNER? config.getBlock().getCornerWidth() : borderWidth;

        int originX = 0, originY = 0, maxX = 0, maxY = 0;

        for(int i = 0; i < 4; i++) {
            BufferedImage imageToUse = borderImage;

            switch(config.get(i)) {
                case CORNER:

                    originX = i == 0 || i == 3? 0 : w - cornerWidth;
                    originY = i == 0 || i == 1? 0 : h - cornerWidth;

                    maxX = originX + cornerWidth;
                    maxY = originY + cornerWidth;
                    break;
                case BORDER:

                    if(config.getType() == CTMConfiguration.TextureType.CORNER) {
                        if(i == 1 || i == 3) {
                            imageToUse = verticalBorderImage;
                        }
                        else {
                            imageToUse = horizontalBorderImage;
                        }
                    }

                    originX = i != 1? 0 : w - borderWidth;
                    originY = i != 2? 0 : h - borderWidth;

                    maxX = i != 3? w : borderWidth;
                    maxY = i != 0? h : borderWidth;
                    break;
                case NONE:
                    continue;
            }

            for(int x = originX; x < maxX; x++) {
                for(int y = originY; y < maxY; y++) {
                    output.setRGB(x, y, imageToUse.getRGB(x, y));
                }
            }
        }


        if(config.getType() == CTMConfiguration.TextureType.CORNER) {
            for(int i = 0; i < 4; i++) {
                if(config.get(i) == CTMConfiguration.Type.BORDER && config.getBefore(i) == CTMConfiguration.Type.BORDER) {
                    originX = i == 0 || i == 3 ? 0 : w - cornerWidth;
                    originY = i == 0 || i == 1 ? 0 : h - cornerWidth;

                    for(int x = originX; x < originX + cornerWidth; x++) {
                        for(int y = originY; y < originY + cornerWidth; y++) {
                            output.setRGB(x, y, borderImage.getRGB(x, y));
                        }
                    }
                }
            }
        }

        BufferedImage[] mipmap = new BufferedImage[1 + Minecraft.getMinecraft().gameSettings.mipmapLevels];
        mipmap[0] = output;

        this.loadSprite(mipmap, null, Minecraft.getMinecraft().gameSettings.anisotropicFiltering > 1);

        if(ConfigHandler.dumpConnectedTextures.getBoolean()) {
            saveToFile();
        }

        return false;
    }

    public CTMConfiguration getConfig() {
        return config;
    }

    public BufferedImage getOutput() {
        return output;
    }

    public void saveToFile() {
        File dir = new File(new File(new File(Minecraft.getMinecraft().mcDataDir, "dumps"), Reference.MOD_ID), "ctm");
        File blockDir = new File(dir, CTMHelper.getTextureNames(config)[0]);
        File file = new File(blockDir, config.getName() + ".png");

        try {
            blockDir.mkdirs();

            ImageIO.write(getOutput(), "png", file);
            LogHelper.info("Successfully saved " + config.getName() + " to file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
