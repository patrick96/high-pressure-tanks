/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.util;

import org.lwjgl.opengl.GL11;

public class Color {
    
    protected boolean usesAlpha = true;
    
    private float r, g, b, a = 0F;
    
    public static Color getFromArray(float[] colorArray) {
        if(colorArray.length == 4) {
            return new Color(colorArray[0], colorArray[1], colorArray[2], colorArray[3]);
        }
        else {
            return new Color(colorArray[0], colorArray[1], colorArray[2]);
        }
    }
    
    public Color(float r, float g, float b) {
        this(Math.round(r * 255F), Math.round(b * 255F), Math.round(g * 255F), false);
    }
    
    public Color(float r, float g, float b, float a) {
        this(Math.round(r * 255F), Math.round(b * 255F), Math.round(g * 255F), Math.round(a * 255F));
    }
    
    public Color(int r, int g, int b) {
        this(r, g, b, false);
    }
    
    public Color(int r, int g, int b, boolean usesAlpha) {
        this((r << 16) + (g << 8) + b, usesAlpha);
    }
    
    public Color(int r, int g, int b, int a) {
        this((a << 24) + (r << 16) + (g << 8) + b, true);
    }
    
    public Color(int color) {
        this(color, true);
    }
    
    public Color(int color, boolean usesAlpha) {
        this.usesAlpha = usesAlpha;
        
        b = (float)(color & 255) / 255.0F;
        g = (float)(color >> 8 & 255) / 255.0F;
        r = (float)(color >> 16 & 255) / 255.0F;

        if(usesAlpha) {
            a = (float)(color >> 24 & 255) / 255.0F;
        }
    }
    
    
    public void usesAlpha(boolean usesAlpha) {
        this.usesAlpha = usesAlpha;
    }
    
    public boolean usesAlpha() {
        return usesAlpha;
    }
    
    public int toInt() {
        int b = (int) (this.b * 255);
        int g = (int) (this.g * 255) << 8;
        int r = (int) (this.r * 255) << 16;
        int a = usesAlpha? (int) (this.a * 255) << 24 : 0;
        
        return r + g + b + a;
    }
    
    public String toHex() {
        return Integer.toHexString(toInt()).toUpperCase();
    }
    
    public float[] toFloatArray() {
        return new float[] {
            r,
            g,
            b,
            a
        };
    }
    
    public void setGL11Color() {
        if(usesAlpha) {
            GL11.glColor4f(r, g, b, a);
        }
        else {
            GL11.glColor3f(r, g, b);
        }
    }
    
    public Color combine(Color colorToCombine) {
        float[] color1 = toFloatArray();
        float[] color2 = colorToCombine.toFloatArray();
        
        int length = 3;
        
        if(usesAlpha && colorToCombine.usesAlpha()) {
            length = 4;
        }
        
        float[] result = new float[length];
        
        for(int i = 0; i < length; i++) {
            result[i] = color1[i] * color2[i];
        }
        
        return Color.getFromArray(result);
    }
}
