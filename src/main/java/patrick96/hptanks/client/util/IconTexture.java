/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.util;

import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.IIcon;
import net.minecraft.util.ResourceLocation;
import patrick96.hptanks.client.gui.HPTGui;

public class IconTexture {
    
    private IIcon icon;
    /* Used to determine which map texture to bind */
    private boolean isItem;
    
    public IconTexture(IIcon icon, boolean isItem) {
        this.icon = icon;
        this.isItem = isItem;
    }
    
    public void draw(HPTGui gui, int x, int y) {
        if(icon != null) {
            draw(gui, x, y, icon.getIconWidth(), icon.getIconHeight());
        }
    }
    
    /**
     * Draws the icon and binds the proper texture map.
     *
     * @param gui the gui
     * @param x the x position of the gui screen, not the whole screen
     * @param y the y position
     * @param width the width of the icon
     * @param height the height of the icon
     */
    public void draw(HPTGui gui, int x, int y, int width, int height) {
        GuiUtils.bindTexture(getMap());
        if(icon != null) {
            gui.drawTexturedModelRectFromIcon(gui.getLeft() + x, gui.getTop() + y, icon, width, height);
        }
    }

    public ResourceLocation getMap() {
        return isItem? TextureMap.locationItemsTexture : TextureMap.locationBlocksTexture;
    }
    
    public int getIconWidth() {
        return icon == null? 0 : icon.getIconWidth();
    }
    
    public int getIconHeight() {
        return icon == null? 0 : icon.getIconHeight();
    }
    
}
