/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.util;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.util.Coords;

public class RenderUtils {
    
    public static void renderStandardInvBlock(RenderBlocks renderer, Block block, int meta) {
        GL11.glPushMatrix();
        
        Tessellator tessellator = Tessellator.instance;
        GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, -1F, 0.0F);
        renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 1.0F, 0.0F);
        renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(1, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 0.0F, -1F);
        renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(2, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 0.0F, 1.0F);
        renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(3, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(-1F, 0.0F, 0.0F);
        renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(4, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(1.0F, 0.0F, 0.0F);
        renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(5, meta));
        tessellator.draw();
        GL11.glTranslatef(0.5F, 0.5F, 0.5F);
    
        GL11.glPopMatrix();
    }

    public static void renderFluidBlock(Fluid fluid, RenderBlocks renderer, Coords coords) {
        renderFluidBlock(fluid, renderer, coords.x, coords.y, coords.z, false);
    }

    public static void renderFluidBlock(Fluid fluid, RenderBlocks renderer, int x, int y, int z) {
        renderFluidBlock(fluid, renderer, x, y, z, false);
    }
    
    public static void renderFluidBlock(Fluid fluid, RenderBlocks renderer, int x, int y, int z, boolean flowing) {
        if (fluid == null) {
            return;
        }
        
        IIcon icon = FluidUtils.getFluidIcon(fluid, flowing);
        if (icon == null) {
            return;
        }
        
        renderer.setOverrideBlockTexture(icon);
        //renderer.renderAllFaces = true;
        renderer.renderStandardBlock(ModBlocks.tankControl, x, y, z);
        renderer.clearOverrideBlockTexture();
    }
}
