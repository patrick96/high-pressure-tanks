/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.util;

import net.minecraft.util.MathHelper;

public class GuiResizeHelper {

    /**
     * Calculates the progress with the sinus curve for animation speed changes
     * @param progress the progress of the resize (0 <= progress <= 1)
     */
    public static float getResizeSin(float progress) {
        return (float) Math.sin(progress * Math.PI / 2);
    }
    
    /**
     * Calculates the progress with the cosinus curve for animation speed changes
     * @param progress the progress of the resize (0 <= progress <= 1)
     */
    public static float getResizeCos(float progress) {
        return (float) Math.cos(progress * Math.PI / 2);
    }
    
    /**
     * Calculates the progress in a quadratic function 
     *
     * @param progress the progress
     * @return the resize sq
     */
    public static float getResizeSq(float progress) {
        return progress * progress;
    }
    
    public enum ResizeType {
        sin(),
        cos(),
        sq(),
        normal();
        
        public float getProgress(float progress) {
            return MathHelper.clamp_float(getResizeByType(this, progress), 0, 1);
        }
    }
    
    public static float getResizeByType(ResizeType type, float progress) {
        switch(type) {
            case sin:
                return getResizeSin(progress * (float) Math.PI / 2F);
        
            case cos:
                return getResizeCos(progress * (float) Math.PI / 2F);
                
            case sq:
                return getResizeSq(progress);
                
            default:
                return progress;
        }
    }
}
