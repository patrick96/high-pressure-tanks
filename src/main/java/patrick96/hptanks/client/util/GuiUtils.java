/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.util;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.util.Utils;

public class GuiUtils {

    public static void bindTexture(ResourceLocation res) {
        Minecraft.getMinecraft().getTextureManager().bindTexture(res);
    }

    /**
     * Loads the resource location with the given name out of the gui folder
     * @param textureName the resource's name. Note: every resource file has to have the prefix "Gui", the parameter is w/o this prefix
     * @return The ResourceLocation object of this resource
     */
    public static ResourceLocation getGuiTexture(String textureName) {
        return Utils.getResourceLocation(Reference.GUI_TEXTURE_LOCATION + "Gui" + textureName + ".png");
    }

}
