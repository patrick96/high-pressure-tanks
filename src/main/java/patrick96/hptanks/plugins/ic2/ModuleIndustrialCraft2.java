/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.ic2;

import cpw.mods.fml.common.Optional.Method;
import cpw.mods.fml.common.registry.GameRegistry;
import ic2.api.item.IC2Items;
import ic2.api.recipe.Recipes;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.init.ModItems.ItemPlain;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.plugins.ICompatModule;
import patrick96.hptanks.recipe.*;
import patrick96.hptanks.recipe.condition.UpgradeRecipeCondition;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.ItemStackIdentifier;

// Build 484 is the last one for 1.7.2, after that 1.7.10
public class ModuleIndustrialCraft2 implements ICompatModule {
    
    @Method(modid="IC2")
    @Override
    public void init() {
        GameRegistry.addShapedRecipe(new ItemStack(ModItems.upgrade, 1, 2), "IAI", "CPC", "IAI",
                'P', ItemPlain.UpgradePlate.getStack(),
                'I', ItemPlain.HardenedRod.getStack(),
                'C', IC2Items.getItem("advancedCircuit"),
                'A', IC2Items.getItem("advancedAlloy"));
        
        ItemStackIdentifier rubber = new ItemStackIdentifier(IC2Items.getItem("resin"));
        HPTRegistry.addResinExtractorRecipe(new ResinExtractorRecipeInfo(rubber, 500, 200));

        ItemStackIdentifier rubberWood = new ItemStackIdentifier(IC2Items.getItem("rubberWood"));
        HPTRegistry.addResinExtractorRecipe(new ResinExtractorRecipeInfo(rubberWood, 250, 300));

        /* #### Wire Mill #### */

        ItemStack copperCable = IC2Items.getItem("copperCableItem");
        ItemStack insulatedCopperCable = IC2Items.getItem("insulatedCopperCableItem");
        
        ItemStack goldCable = IC2Items.getItem("goldCableItem");
        ItemStack insulatedGoldCable = IC2Items.getItem("insulatedGoldCableItem");
        
        ItemStack ironCable = IC2Items.getItem("ironCableItem");
        ItemStack insulatedIronCable = IC2Items.getItem("insulatedIronCableItem");
        
        ItemStack tinCable = IC2Items.getItem("tinCableItem");
        ItemStack insulatedTinCable = IC2Items.getItem("insulatedTinCableItem");

        /* ingot -> insulated */
        insulatedCopperCable.stackSize = 5;
        insulatedGoldCable.stackSize = 6;
        insulatedIronCable.stackSize = 6;
        insulatedTinCable.stackSize = 5;

        WireMillRecipeInfo ingotCopper = new WireMillRecipeInfo(new ItemStackIdentifier("ingotCopper"), insulatedCopperCable.copy(), 750, 400);
        ingotCopper.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addWireMillRecipe(ingotCopper);

        WireMillRecipeInfo ingotGold = new WireMillRecipeInfo(new ItemStackIdentifier("ingotGold"), insulatedGoldCable.copy(), 1500, 600);
        ingotGold.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addWireMillRecipe(ingotGold);

        WireMillRecipeInfo ingotIron = new WireMillRecipeInfo(new ItemStackIdentifier("ingotIron"), insulatedIronCable.copy(), 2500, 800);
        ingotIron.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addWireMillRecipe(ingotIron);

        WireMillRecipeInfo ingotTin = new WireMillRecipeInfo(new ItemStackIdentifier("ingotTin"), insulatedTinCable.copy(), 750, 400);
        ingotTin.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addWireMillRecipe(ingotTin);

        /* uninsulated -> insulated */
        insulatedCopperCable.stackSize = 1;
        insulatedGoldCable.stackSize = 1;
        insulatedIronCable.stackSize = 1;
        insulatedTinCable.stackSize = 1;
        
        WireMillRecipeInfo cableCopper = new WireMillRecipeInfo(new ItemStackIdentifier(copperCable), insulatedCopperCable.copy(), 150, 160);
        cableCopper.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addWireMillRecipe(cableCopper);

        WireMillRecipeInfo cableGold = new WireMillRecipeInfo(new ItemStackIdentifier(goldCable), insulatedGoldCable.copy(), 250, 200);
        cableGold.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addWireMillRecipe(cableGold);

        WireMillRecipeInfo cableIron = new WireMillRecipeInfo(new ItemStackIdentifier(ironCable), insulatedIronCable.copy(), 500, 250);
        cableIron.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addWireMillRecipe(cableIron);

        WireMillRecipeInfo cableTin = new WireMillRecipeInfo(new ItemStackIdentifier(tinCable), insulatedTinCable.copy(), 150, 160);
        cableTin.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addWireMillRecipe(cableTin);

        /* #### Rubber Former #### */

        RubberFormerRecipeInfo rubberItem = new RubberFormerRecipeInfo(IC2Items.getItem("rubber"), 160, 160);
        rubberItem.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addRubberFormerRecipe(rubberItem);

        RubberFormerRecipeInfo rubberSheet = new RubberFormerRecipeInfo(IC2Items.getItem("rubberTrampoline"), 300, 160);
        rubberSheet.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addRubberFormerRecipe(rubberSheet);

        RubberFormerRecipeInfo rubberDinghy = new RubberFormerRecipeInfo(IC2Items.getItem("boatRubber"), 800, 320);
        rubberDinghy.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addRubberFormerRecipe(rubberDinghy);

        RubberFormerRecipeInfo rubberBoots = new RubberFormerRecipeInfo(IC2Items.getItem("staticBoots"), 1000, 320);
        rubberBoots.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addRubberFormerRecipe(rubberBoots);

        /* #### HPCutter #### */

        copperCable.stackSize = 3;
        goldCable.stackSize = 4;
        ironCable.stackSize = 4;
        tinCable.stackSize = 3;


        HPCutterRecipeInfo uninsulatedCopperCable = new HPCutterRecipeInfo(new ItemStackIdentifier("ingotCopper"), copperCable.copy(), 1, 32, 320);
        uninsulatedCopperCable.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addHpCutterRecipe(uninsulatedCopperCable);

        HPCutterRecipeInfo uninsulatedGoldCable = new HPCutterRecipeInfo(new ItemStackIdentifier("ingotGold"), goldCable.copy(), 1, 32, 320);
        uninsulatedGoldCable.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addHpCutterRecipe(uninsulatedGoldCable);

        HPCutterRecipeInfo uninsulatedIronCable = new HPCutterRecipeInfo(new ItemStackIdentifier("ingotIron"), ironCable.copy(), 1, 32, 320);
        uninsulatedIronCable.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addHpCutterRecipe(uninsulatedIronCable);

        HPCutterRecipeInfo uninsulatedTinCable = new HPCutterRecipeInfo(new ItemStackIdentifier("ingotTin"), tinCable.copy(), 1, 32, 320);
        uninsulatedTinCable.addCondition(new UpgradeRecipeCondition(UpgradeType.IC2));
        HPTRegistry.addHpCutterRecipe(uninsulatedTinCable);
        
        Recipes.scrapboxDrops.addDrop(ItemPlain.Graphite.getStack(), 0.5F);
        Recipes.scrapboxDrops.addDrop(ItemPlain.CNT.getStack(), 0.1F);
        Recipes.scrapboxDrops.addDrop(ItemPlain.ObsidianStick.getStack(), 1.0F);
    }
    
    @Override
    public void preInit() {}
    
    @Override
    public void postInit() {}
    
    @Override
    public String getDependency() {
        return Reference.PLUGIN_IC2_DEP;
    }
    
    @Override
    public String getCompatName() {
        return Reference.PLUGIN_IC2_NAME;
    }
}


