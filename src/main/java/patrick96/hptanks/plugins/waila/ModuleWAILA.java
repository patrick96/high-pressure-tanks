/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.waila;

import cpw.mods.fml.common.event.FMLInterModComms;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.plugins.ICompatModule;

public class ModuleWAILA implements ICompatModule {
    
    @Override
    public void init() {
        FMLInterModComms.sendMessage("Waila", "register", "patrick96.hptanks.plugins.waila.WailaRegistry.callbackRegister");
    }
    
    @Override
    public void preInit() {}
    
    @Override
    public void postInit() {}
    
    @Override
    public String getDependency() {
        return Reference.PLUGIN_WAILA_DEP;
    }
    
    @Override
    public String getCompatName() {
        return Reference.PLUGIN_WAILA_NAME;
    }
}
