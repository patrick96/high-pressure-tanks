/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.waila;

import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.core.power.IPowerTile;
import patrick96.hptanks.tile.TileEntityFluidPump;
import patrick96.hptanks.tile.hpt.TileEntityTankComponent;

public class WailaRegistry {

    public static void callbackRegister(IWailaRegistrar registrar){
        registrar.registerBodyProvider(new FluidHandlerProvider(), IFluidHandler.class);
        registrar.registerBodyProvider(new PowerTileProvider(), IPowerTile.class);
        registrar.registerBodyProvider(new HPTComponentProvider(), TileEntityTankComponent.class);
        registrar.registerHeadProvider(new HPTComponentProvider(), TileEntityTankComponent.class);
        registrar.registerBodyProvider(new FluidPumpProvider(), TileEntityFluidPump.class);
        registrar.registerHeadProvider(new FluidPumpProvider(), TileEntityFluidPump.class);
    }
    
}
