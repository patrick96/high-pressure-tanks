/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.forgemultipart;

import cpw.mods.fml.common.event.FMLInterModComms;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.blocks.BuildingBlock;
import patrick96.hptanks.blocks.ItemBlockMulti;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.plugins.ICompatModule;
import patrick96.hptanks.util.LogHelper;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;

public class ModuleForgeMultiPart implements ICompatModule {
    
    @Override
    public void init() {
        try {
            addMicroBlock(new ItemStack(ModBlocks.machine, 1, 0));
            addMicroBlock(new ItemStack(ModBlocks.bulletproofGlass));
            addMicroBlock(new ItemStack(ModBlocks.tankControl));
            addMicroBlock(new ItemStack(ModBlocks.tankCasing));
            addMicroBlock(new ItemStack(ModBlocks.tankCCperipheral));
            addMicroBlock(new ItemStack(ModBlocks.tankGlass));
            addMicroBlock(new ItemStack(ModBlocks.tankValve));
            addMicroBlock(new ItemStack(ModBlocks.resin));

            addSubtypesMicroBlock(ModBlocks.metal);
            addSubtypesMicroBlock(ModBlocks.ore);
            addSubtypesMicroBlock(ModBlocks.compressed);

            addBuildingBlockMultiBlock(ModBlocks.resinBrick);
            addBuildingBlockMultiBlock(ModBlocks.resinBrickDark);
            addBuildingBlockMultiBlock(ModBlocks.smoothResin);
            addBuildingBlockMultiBlock(ModBlocks.smoothResinDark);

        } catch (Exception ex) {
            LogHelper.error("There was a Problem Loading " + Reference.PLUGIN_FMP_NAME);
            
            ex.printStackTrace();
        }

        
    }
    
    @Override
    public String getDependency() {
        return Reference.PLUGIN_FMP_DEP;
    }
    
    @Override
    public String getCompatName() {
        return Reference.PLUGIN_FMP_NAME;
    }
    
    private void addSubtypesMicroBlock(Block block) {
        Item item = Item.getItemFromBlock(block);
        
        if(item instanceof ItemBlockMulti) {
            ItemBlockMulti itemMulti = (ItemBlockMulti) item;
            ArrayList<ItemStack> list = new ArrayList<ItemStack>();
            itemMulti.getSubItems(itemMulti, null, list);
            for(ItemStack stack : list) {
                addMicroBlock(stack);
            }
        }
    }

    private void addBuildingBlockMultiBlock(BuildingBlock buildingBlock) {
        if(buildingBlock != null && buildingBlock.getBlock() != null) {
            addMicroBlock(new ItemStack(buildingBlock.getBlock()));
        }
    }

    private void addMicroBlock(ItemStack stack) {
        FMLInterModComms.sendMessage("ForgeMicroblock", "microMaterial", stack);
    }

    @Override
    public void preInit() {}


    @Override
    public void postInit() {}
    
}


