/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.cc;

import com.mojang.authlib.GameProfile;
import dan200.computercraft.api.peripheral.IPeripheral;
import dan200.computercraft.api.turtle.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.command.server.CommandBlockLogic;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.attributes.BaseAttributeMap;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityMinecartHopper;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryEnderChest;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.play.client.C15PacketClientSettings;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.scoreboard.Team;
import net.minecraft.stats.StatBase;
import net.minecraft.stats.StatisticsFile;
import net.minecraft.tileentity.*;
import net.minecraft.util.*;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldSettings;
import net.minecraftforge.common.IExtendedEntityProperties;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.util.Coords;

import java.util.*;

// TODO Upgrade ID in config
// TODO Find a way to make it appear in NEI
public class WrenchingTurtle implements ITurtleUpgrade {

    @Override
    public int getUpgradeID() {
        return 65;
    }

    @Override
    public String getUnlocalisedAdjective() {
        return "Wrenching";
    }

    @Override
    public TurtleUpgradeType getType() {
        return TurtleUpgradeType.Tool;
    }

    @Override
    public ItemStack getCraftingItem() {
        return ModItems.wrench.getStack();
    }

    @Override
    public IPeripheral createPeripheral(ITurtleAccess turtle, TurtleSide side) {
        return null;
    }

    @Override
    public TurtleCommandResult useTool(ITurtleAccess turtle, TurtleSide side, TurtleVerb verb, int direction) {
        if(turtle == null) {
            return null;
        }

        ChunkCoordinates chCoords = turtle.getPosition();
        Coords coords = new Coords(chCoords.posX, chCoords.posY, chCoords.posZ);
        Coords facing = coords.add(ForgeDirection.getOrientation(turtle.getDirection()));
        TileEntity tile = facing.getTileEntity(turtle.getWorld());

        if(!(tile instanceof TileEntityHPTBase)) {
            return TurtleCommandResult.failure();
        }

        TileEntityHPTBase base = (TileEntityHPTBase) tile;

        final List<Object> results = new ArrayList<Object>();
        // TODO create own fakePlayer class
        FakePlayer player = new FakePlayer((WorldServer) turtle.getWorld(), new GameProfile(UUID.randomUUID(), "[HPT]")) {
            @Override
            public void addChatMessage(IChatComponent chatmessagecomponent) {
                results.add(chatmessagecomponent.getUnformattedText());
            }

            @Override public void addToPlayerScore(Entity p_70084_1_, int p_70084_2_) {}
            @Override public boolean isInRangeToRender3d(double p_145770_1_, double p_145770_3_, double p_145770_5_) { return false; }
            @Override public boolean isInRangeToRenderDist(double p_70112_1_) { return false; }
            @Override public boolean writeMountToNBT(NBTTagCompound p_98035_1_) { return false; }
            @Override public boolean writeToNBTOptional(NBTTagCompound p_70039_1_) { return false; }
            @Override public void writeToNBT(NBTTagCompound p_70109_1_) {}
            @Override public void readFromNBT(NBTTagCompound p_70020_1_) {}
            @Override protected boolean shouldSetPosAfterLoading() { return false; }
            @Override protected void applyEntityAttributes() {}
            @Override public int getEntityId() { return 0; }
            @Override public void setEntityId(int p_145769_1_) {}
            @Override protected void entityInit() {}
            @Override public DataWatcher getDataWatcher() { return null; }
            @Override public boolean equals(Object p_equals_1_) { return false; }
            @Override public int hashCode() { return 0; }
            @Override public ItemStack getItemInUse() { return null; }
            @Override public int getItemInUseCount() { return 0; }
            @Override public boolean isUsingItem() { return false; }
            @Override public int getItemInUseDuration() { return 0; }
            @Override public void stopUsingItem() {}
            @Override protected float applyArmorCalculations(DamageSource p_70655_1_, float p_70655_2_) { return 0; }
            @Override protected float applyPotionDamageCalculations(DamageSource p_70672_1_, float p_70672_2_) { return 0; }
            @Override public boolean attackEntityAsMob(Entity p_70652_1_) { return false; }
            @Override public boolean attackEntityFrom(DamageSource p_70097_1_, float p_70097_2_) { return false; }
            @Override public void renderBrokenItemStack(ItemStack p_70669_1_) {}
            @Override public void addChatComponentMessage(IChatComponent chatmessagecomponent) {}
            @Override public ChunkCoordinates getBedLocation() { return null; }
            @Override public boolean isSpawnForced() { return false; }
            @Override public void setSpawnChunk(ChunkCoordinates p_71063_1_, boolean p_71063_2_) {}
            @Override public void triggerAchievement(StatBase p_71029_1_) {}
            @Override protected void onItemUseFinish() {}
            @Override public void handleHealthUpdate(byte p_70103_1_) {}
            @Override protected void kill() {}
            @Override public boolean isOffsetPositionInLiquid(double p_70038_1_, double p_70038_3_, double p_70038_5_) { return false; }
            @Override public void moveEntity(double p_70091_1_, double p_70091_3_, double p_70091_5_) {}
            @Override protected void updateArmSwingProgress() {}
            @Override public IAttributeInstance getEntityAttribute(IAttribute p_110148_1_) { return null; }
            @Override public BaseAttributeMap getAttributeMap() { return null; }
            @Override public EnumCreatureAttribute getCreatureAttribute() { return null; }
            @Override protected boolean isMovementBlocked() { return false; }
            @Override public void setItemInUse(ItemStack p_71008_1_, int p_71008_2_) {}
            @Override public boolean isCurrentToolAdventureModeExempt(int p_82246_1_, int p_82246_2_, int p_82246_3_) { return false; }
            @Override public void addEntityCrashInfo(CrashReportCategory p_85029_1_) {}
            @Override public void addExhaustion(float p_71020_1_) {}
            @Override public FoodStats getFoodStats() { return null; }
            @Override public void addExperience(int p_71023_1_) {}
            @Override public void readEntityFromNBT(NBTTagCompound p_70037_1_) {}
            @Override protected void updatePotionEffects() {}
            @Override public void writeEntityToNBT(NBTTagCompound p_70014_1_) {}
            @Override public void onChunkLoad() {}
            @Override protected NBTTagList newDoubleNBTList(double... p_70087_1_) { return null; }
            @Override protected NBTTagList newFloatNBTList(float... p_70049_1_) { return null; }
            @Override public void addExperienceLevel(int p_82242_1_) {}
            @Override public int xpBarCap() { return 0; }
            @Override public void addMovementStat(double p_71000_1_, double p_71000_3_, double p_71000_5_) {}
            @Override protected void fall(float p_70069_1_) {}
            @Override public boolean isWet() { return false; }
            @Override public boolean isInWater() { return false; }
            @Override public boolean handleWaterMovement() { return false; }
            @Override protected String func_146067_o(int p_146067_1_) { return null; }
            @Override public void performHurtAnimation() {}
            @Override public void onKillEntity(EntityLivingBase p_70074_1_) {}
            @Override protected boolean func_145771_j(double p_145771_1_, double p_145771_3_, double p_145771_5_) { return false; }
            @Override public void setInWeb() {}
            @Override public IIcon getItemIcon(ItemStack p_70620_1_, int p_70620_2_) { return null; }
            @Override public ItemStack getCurrentArmor(int p_82169_1_) { return null; }
            @Override public void addPotionEffect(PotionEffect p_70690_1_) {}
            @Override public boolean isPotionApplicable(PotionEffect p_70687_1_) { return false; }
            @Override public boolean isEntityUndead() { return false; }
            @Override public void removePotionEffectClient(int p_70618_1_) {}
            @Override public void removePotionEffect(int p_82170_1_) {}
            @Override public void addScore(int p_85039_1_) {}
            @Override public void addSelfToInternalCraftingInventory() {}
            @Override protected void resetHeight() {}
            @Override public float getEyeHeight() { return 0; }
            @Override public boolean handleLavaMovement() { return false; }
            @Override public void moveFlying(float p_70060_1_, float p_70060_2_, float p_70060_3_) {}
            @Override public int getBrightnessForRender(float p_70070_1_) { return 0; }
            @Override public float getBrightness(float p_70013_1_) { return 0; }
            @Override public void setWorld(World p_70029_1_) {}
            @Override public void setPositionAndRotation(double p_70080_1_, double p_70080_3_, double p_70080_5_, float p_70080_7_, float p_70080_8_) {}
            @Override public void setLocationAndAngles(double p_70012_1_, double p_70012_3_, double p_70012_5_, float p_70012_7_, float p_70012_8_) {}
            @Override public float getDistanceToEntity(Entity p_70032_1_) { return 0; }
            @Override public double getDistanceSq(double p_70092_1_, double p_70092_3_, double p_70092_5_) { return 0; }
            @Override public double getDistance(double p_70011_1_, double p_70011_3_, double p_70011_5_) { return 0; }
            @Override public double getDistanceSqToEntity(Entity p_70068_1_) { return 0; }
            @Override public void onCollideWithPlayer(EntityPlayer p_70100_1_) {}
            @Override protected void setBeenAttacked() {}
            @Override public float getRotationYawHead() { return 0; }
            @Override public void setRotationYawHead(float p_70034_1_) {}
            @Override public void addStat(StatBase par1StatBase, int par2) {}
            @Override public void jump() {}
            @Override public void moveEntityWithHeading(float p_70612_1_, float p_70612_2_) {}
            @Override protected boolean isAIEnabled() { return false; }
            @Override public float getAIMoveSpeed() { return 0; }
            @Override public void setAIMoveSpeed(float p_70659_1_) {}
            @Override public void mountEntityAndWakeUp() {}
            @Override public void setPlayerHealthUpdated() {}
            @Override public void openGui(Object mod, int modGuiId, World world, int x, int y, int z) {}
            @Override public Vec3 getPosition(float par1) { return null; }
            @Override public MovingObjectPosition rayTrace(double p_70614_1_, float p_70614_3_) { return null; }
            @Override public boolean isClientWorld() { return false; }
            @Override public ChunkCoordinates getBedLocation(int dimension) { return null; }
            @Override public boolean isSpawnForced(int dimension) { return false; }
            @Override public void setSpawnChunk(ChunkCoordinates chunkCoordinates, boolean forced, int dimension) {}
            @Override public boolean isEntityInvulnerable() { return false; }
            @Override public void addVelocity(double p_70024_1_, double p_70024_3_, double p_70024_5_) {}
            @Override public void applyEntityCollision(Entity p_70108_1_) {}
            @Override public void attackTargetEntityWithCurrentItem(Entity p_71059_1_) {}
            @Override public void clonePlayer(EntityPlayer p_71049_1_, boolean p_71049_2_) {}
            @Override protected void onNewPotionEffect(PotionEffect p_70670_1_) {}
            @Override protected void onChangedPotionEffect(PotionEffect p_70695_1_, boolean p_70695_2_) {}
            @Override protected void onFinishedPotionEffect(PotionEffect p_70688_1_) {}
            @Override public void heal(float p_70691_1_) {}
            @Override public void setHealth(float p_70606_1_) {}
            @Override public void setPositionAndUpdate(double p_70634_1_, double p_70634_3_, double p_70634_5_) {}
            @Override public void dismountEntity(Entity p_110145_1_) {}
            @Override public void onCriticalHit(Entity p_71009_1_) {}
            @Override public void onEnchantmentCritical(Entity p_71047_1_) {}
            @Override public void respawnPlayer() {}
            @Override public void setDead() {}
            @Override protected void setSize(float p_70105_1_, float p_70105_2_) {}
            @Override protected void setRotation(float p_70101_1_, float p_70101_2_) {}
            @Override public void setPosition(double p_70107_1_, double p_70107_3_, double p_70107_5_) {}
            @Override public void setAngles(float p_70082_1_, float p_70082_2_) {}
            @Override public boolean isEntityInsideOpaqueBlock() { return false; }
            @Override public boolean interactFirst(EntityPlayer p_130002_1_) { return false; }
            @Override public AxisAlignedBB getCollisionBox(Entity p_70114_1_) { return null; }
            @Override public GameProfile getGameProfile() { return null; }
            @Override public void sendPlayerAbilities() {}
            @Override public WorldServer getServerForPlayer() { return null; }
            @Override public void setGameType(WorldSettings.GameType p_71033_1_) {}
            @Override public String getCommandSenderName() { return null; }
            @Override public Entity[] getParts() { return null; }
            @Override public boolean isEntityEqual(Entity p_70028_1_) { return false; }
            @Override public World getEntityWorld() { return null; }
            @Override public InventoryEnderChest getInventoryEnderChest() { return null; }
            @Override public ItemStack getEquipmentInSlot(int p_71124_1_) { return null; }
            @Override public ItemStack getHeldItem() { return null; }
            @Override public void setCurrentItemOrArmor(int p_70062_1_, ItemStack p_70062_2_) {}
            @Override public boolean isBurning() { return false; }
            @Override public boolean isRiding() { return false; }
            @Override public boolean isSneaking() { return false; }
            @Override public void setSneaking(boolean p_70095_1_) {}
            @Override public boolean isSprinting() { return false; }
            @Override public void setSprinting(boolean p_70031_1_) {}
            @Override public boolean isInvisible() { return false; }
            @Override public boolean isInvisibleToPlayer(EntityPlayer p_98034_1_) { return false; }
            @Override public void setInvisible(boolean p_82142_1_) {}
            @Override public boolean isEating() { return false; }
            @Override public void setEating(boolean p_70019_1_) {}
            @Override protected boolean getFlag(int p_70083_1_) { return false; }
            @Override protected void setFlag(int p_70052_1_, boolean p_70052_2_) {}
            @Override public int getAir() { return 0; }
            @Override public void setAir(int p_70050_1_) {}
            @Override public void onStruckByLightning(EntityLightningBolt p_70077_1_) {}
            @Override public ItemStack[] getLastActiveItems() { return null; }
            @Override protected float getSoundVolume() { return 0; }
            @Override protected float getSoundPitch() { return 0; }
            @Override public boolean getHideCape() { return false; }
            @Override public boolean isPushedByWater() { return false; }
            @Override public Scoreboard getWorldScoreboard() { return null; }
            @Override public Team getTeam() { return null; }
            @Override public boolean isOnSameTeam(EntityLivingBase p_142014_1_) { return false; }
            @Override public boolean isOnTeam(Team p_142012_1_) { return false; }
            @Override public IChatComponent func_145748_c_() { return null; }
            @Override public void func_145781_i(int p_145781_1_) {}
            @Override public NBTTagCompound getEntityData() { return null; }
            @Override public boolean shouldRiderSit() { return false; }
            @Override public ItemStack getPickedResult(MovingObjectPosition target) { return null; }
            @Override public UUID getPersistentID() { return null; }
            @Override public boolean shouldRenderInPass(int pass) { return false; }
            @Override public boolean isCreatureType(EnumCreatureType type, boolean forSpawnCount) { return false; }
            @Override public String registerExtendedProperties(String identifier, IExtendedEntityProperties properties) { return null; }
            @Override public IExtendedEntityProperties getExtendedProperties(String identifier) { return null; }
            @Override public void setAbsorptionAmount(float p_110149_1_) {}
            @Override public float getAbsorptionAmount() { return 0; }
            @Override public boolean canAttackPlayer(EntityPlayer player) { return false; }
            @Override public void onDeath(DamageSource source) {}
            @Override protected String getHurtSound() { return null; }
            @Override protected String getDeathSound() { return null; }
            @Override public void onUpdate() {}
            @Override protected float func_110146_f(float p_110146_1_, float p_110146_2_) { return 0; }
            @Override public int getMaxInPortalTime() { return 0; }
            @Override protected void setOnFireFromLava() {}
            @Override public void setFire(int p_70015_1_) {}
            @Override public void extinguish() {}
            @Override protected String getSwimSound() { return null; }
            @Override protected void func_145775_I() {}
            @Override protected void func_145780_a(int p_145780_1_, int p_145780_2_, int p_145780_3_, Block p_145780_4_) {}
            @Override protected String getSplashSound() { return null; }
            @Override public boolean isInsideOfMaterial(Material p_70055_1_) { return false; }
            @Override public int getPortalCooldown() { return 0; }
            @Override public void setVelocity(double p_70016_1_, double p_70016_3_, double p_70016_5_) {}
            @Override public void playSound(String p_85030_1_, float p_85030_2_, float p_85030_3_) {}
            @Override protected void updateItemUse(ItemStack p_71010_1_, int p_71010_2_) {}
            @Override public void onUpdateEntity() {}
            @Override protected void func_147098_j() {}
            @Override public void travelToDimension(int dim) {}
            @Override public float func_145772_a(Explosion p_145772_1_, World p_145772_2_, int p_145772_3_, int p_145772_4_, int p_145772_5_, Block p_145772_6_) { return 0; }
            @Override public boolean func_145774_a(Explosion p_145774_1_, World p_145774_2_, int p_145774_3_, int p_145774_4_, int p_145774_5_, Block p_145774_6_, float p_145774_7_) { return false; }
            @Override public int getMaxSafePointTries() { return 0; }
            @Override public int getTeleportDirection() { return 0; }
            @Override public void onItemPickup(Entity p_71001_1_, int p_71001_2_) {}
            @Override public EnumStatus sleepInBedAt(int p_71018_1_, int p_71018_2_, int p_71018_3_) { return null; }
            @Override public void wakeUpPlayer(boolean p_70999_1_, boolean p_70999_2_, boolean p_70999_3_) {}
            @Override public float getBedOrientationInDegrees() { return 0; }
            @Override public boolean isPlayerSleeping() { return false; }
            @Override public boolean isPlayerFullyAsleep() { return false; }
            @Override public int getSleepTimer() { return 0; }
            @Override protected boolean getHideCape(int p_82241_1_) { return false; }
            @Override protected void setHideCape(int p_82239_1_, boolean p_82239_2_) {}
            @Override public void mountEntity(Entity p_70078_1_) {}
            @Override public void updateRidden() {}
            @Override public void updateRiderPosition() {}
            @Override public void setPositionAndRotation2(double p_70056_1_, double p_70056_3_, double p_70056_5_, float p_70056_7_, float p_70056_8_, int p_70056_9_) {}
            @Override public float getCollisionBorderSize() { return 0; }
            @Override protected void updateAITick() {}
            @Override public void preparePlayerToSpawn() {}
            @Override protected void updateEntityActionState() {}
            @Override public void setJumping(boolean p_70637_1_) {}
            @Override public void onLivingUpdate() {}
            @Override protected void updateAITasks() {}
            @Override public int getScore() { return 0; }
            @Override public void setScore(int p_85040_1_) {}
            @Override protected void updateFallState(double p_70064_1_, boolean p_70064_3_) {}
            @Override public AxisAlignedBB getBoundingBox() { return null; }
            @Override public void handleFalling(double p_71122_1_, boolean p_71122_3_) {}
            @Override public void func_146100_a(TileEntity p_146100_1_) {}
            @Override public void func_146095_a(CommandBlockLogic p_146095_1_) {}
            @Override public void getNextWindowId() {}
            @Override public void displayGUIWorkbench(int p_71058_1_, int p_71058_2_, int p_71058_3_) {}
            @Override public void displayGUIEnchantment(int p_71002_1_, int p_71002_2_, int p_71002_3_, String p_71002_4_) {}
            @Override public void displayGUIAnvil(int p_82244_1_, int p_82244_2_, int p_82244_3_) {}
            @Override public void displayGUIChest(IInventory p_71007_1_) {}
            @Override public void func_146093_a(TileEntityHopper p_146093_1_) {}
            @Override public void displayGUIHopperMinecart(EntityMinecartHopper p_96125_1_) {}
            @Override public void func_146101_a(TileEntityFurnace p_146101_1_) {}
            @Override public void func_146102_a(TileEntityDispenser p_146102_1_) {}
            @Override public void func_146098_a(TileEntityBrewingStand p_146098_1_) {}
            @Override public void func_146104_a(TileEntityBeacon p_146104_1_) {}
            @Override public void displayGUIMerchant(IMerchant p_71030_1_, String p_71030_2_) {}
            @Override public void displayGUIBook(ItemStack p_71048_1_) {}
            @Override public boolean interactWith(Entity p_70998_1_) { return false; }
            @Override public ItemStack getCurrentEquippedItem() { return null; }
            @Override public void displayGUIHorse(EntityHorse p_110298_1_, IInventory p_110298_2_) {}
            @Override public void sendSlotContents(Container p_71111_1_, int p_71111_2_, ItemStack p_71111_3_) {}
            @Override public void sendContainerToPlayer(Container p_71120_1_) {}
            @Override public void sendContainerAndContentsToPlayer(Container p_71110_1_, List p_71110_2_) {}
            @Override public void sendProgressBarUpdate(Container p_71112_1_, int p_71112_2_, int p_71112_3_) {}
            @Override public void func_147100_a(C15PacketClientSettings pkt) {}
            @Override public EnumChatVisibility func_147096_v() { return null; }
            @Override public void requestTexturePackLoad(String p_147095_1_) {}
            @Override public boolean canPlayerEdit(int p_82247_1_, int p_82247_2_, int p_82247_3_, int p_82247_4_, ItemStack p_82247_5_) { return false; }
            @Override protected int getExperiencePoints(EntityPlayer p_70693_1_) { return 0; }
            @Override protected boolean isPlayer() { return false; }
            @Override public Random getRNG() { return null; }
            @Override public EntityLivingBase getAITarget() { return null; }
            @Override public int func_142015_aE() { return 0; }
            @Override public void setRevengeTarget(EntityLivingBase p_70604_1_) {}
            @Override public EntityLivingBase getLastAttacker() { return null; }
            @Override public int getLastAttackerTime() { return 0; }
            @Override public void setLastAttacker(Entity p_130011_1_) {}
            @Override public int getAge() { return 0; }
            @Override public boolean getAlwaysRenderNameTagForRender() { return false; }
            @Override protected boolean canTriggerWalking() { return false; }
            @Override protected void collideWithEntity(Entity p_82167_1_) {}
            @Override protected void collideWithNearbyEntities() {}
            @Override public boolean canAttackWithItem() { return false; }
            @Override public boolean hitByEntity(Entity p_85031_1_) { return false; }
            @Override public String toString() { return null; }
            @Override public boolean canBeCollidedWith() { return false; }
            @Override public boolean canBePushed() { return false; }
            @Override public boolean canBreatheUnderwater() { return false; }
            @Override public void onEntityUpdate() {}
            @Override public boolean isChild() { return false; }
            @Override protected void onDeathUpdate() {}
            @Override protected boolean func_146066_aG() { return false; }
            @Override public boolean canCommandSenderUseCommand(int i, String s) { return false; }
            @Override public String getPlayerIP() { return null; }
            @Override public ChunkCoordinates getPlayerCoordinates() { return null; }
            @Override public void func_143004_u() {}
            @Override public StatisticsFile func_147099_x() { return null; }
            @Override public void func_152339_d(Entity p_152339_1_) {}
            @Override public long func_154331_x() { return 0; }
            @Override public float getDefaultEyeHeight() { return 0; }
            @Override public String getDisplayName() { return null; }
            @Override public void refreshDisplayName() {}
            @Override public boolean canEat(boolean p_71043_1_) { return false; }
            @Override public boolean shouldHeal() { return false; }
            @Override public boolean canEntityBeSeen(Entity p_70685_1_) { return false; }
            @Override public Vec3 getLookVec() { return null; }
            @Override public void setInPortal() {}
            @Override public Vec3 getLook(float p_70676_1_) { return null; }
            @Override public float getSwingProgress(float p_70678_1_) { return 0; }
            @Override public boolean canHarvestBlock(Block p_146099_1_) { return false; }
            @Override public boolean canRenderOnFire() { return false; }
            @Override public UUID getUniqueID() { return null; }
            @Override public boolean canRiderInteract() { return false; }
            @Override public boolean shouldDismountInWater(Entity rider) { return false; }
            @Override public void clearActivePotions() {}
            @Override public Collection getActivePotionEffects() { return null; }
            @Override public boolean isPotionActive(int p_82165_1_) { return false; }
            @Override public boolean isPotionActive(Potion p_70644_1_) { return false; }
            @Override public PotionEffect getActivePotionEffect(Potion p_70660_1_) { return null; }
            @Override public void clearItemInUse() {}
            @Override public boolean isBlocking() { return false; }
            @Override public void closeContainer() {}
            @Override public void setEntityActionState(float p_110430_1_, float p_110430_2_, boolean p_110430_3_, boolean p_110430_4_) {}
            @Override public void closeScreen() {}
            @Override public void updateHeldItem() {}
            @Override public void copyDataFrom(Entity p_82141_1_, boolean p_82141_2_) {}
            @Override public void copyLocationAndAnglesFrom(Entity p_82149_1_) {}
            @Override public void curePotionEffects(ItemStack curativeItem) {}
            @Override public boolean shouldRiderFaceForward(EntityPlayer player) { return false; }
            @Override public void func_152111_bt() {}
            @Override public void func_152112_bu() {}
            @Override public EntityItem dropPlayerItemWithRandomChoice(ItemStack p_71019_1_, boolean p_71019_2_) { return null; }
            @Override public EntityItem func_146097_a(ItemStack p_146097_1_, boolean p_146097_2_, boolean p_146097_3_) { return null; }
            @Override public void joinEntityItemWithWorld(EntityItem p_71012_1_) {}
            @Override public float getCurrentPlayerStrVsBlock(Block p_146096_1_, boolean p_146096_2_) { return 0; }
            @Override public float getBreakSpeed(Block p_146096_1_, boolean p_146096_2_, int meta) { return 0; }
            @Override public float getBreakSpeed(Block p_146096_1_, boolean p_146096_2_, int meta, int x, int y, int z) { return 0; }
            @Override protected int decreaseAirSupply(int p_70682_1_) { return 0; }
            @Override protected void damageArmor(float p_70675_1_) {}
            @Override public int getTotalArmorValue() { return 0; }
            @Override public float getArmorVisibility() { return 0; }
            @Override protected void damageEntity(DamageSource p_70665_1_, float p_70665_2_) {}
            @Override public CombatTracker func_110142_aN() { return null; }
            @Override public EntityLivingBase func_94060_bK() { return null; }
            @Override public void swingItem() {}
            @Override protected void dealFireDamage(int p_70081_1_) {}
            @Override protected void dropEquipment(boolean p_82160_1_, int p_82160_2_) {}
            @Override public void knockBack(Entity p_70653_1_, float p_70653_2_, double p_70653_3_, double p_70653_5_) {}
            @Override protected void dropFewItems(boolean p_70628_1_, int p_70628_2_) {}
            @Override public boolean isOnLadder() { return false; }
            @Override public boolean isEntityAlive() { return false; }
            @Override protected void dropRareDrop(int p_70600_1_) {}
            @Override public boolean doesEntityNotTriggerPressurePlate() { return false; }
            @Override public EntityItem dropItem(Item p_145779_1_, int p_145779_2_) { return null; }
            @Override public EntityItem func_145778_a(Item p_145778_1_, int p_145778_2_, float p_145778_3_) { return null; }
            @Override public EntityItem entityDropItem(ItemStack p_70099_1_, float p_70099_2_) { return null; }
            @Override public float getShadowSize() { return 0; }
            @Override public EntityItem dropOneItem(boolean p_71040_1_) { return null; }
            @Override public void destroyCurrentEquippedItem() {}
            @Override public double getYOffset() { return 0; }
            @Override public double getMountedYOffset() { return 0; }
        };
        player.inventory.changeCurrentItem(0);
        player.inventory.setInventorySlotContents(0, ModItems.wrench.getStack());

        switch(verb) {
            case Dig:
                player.setSneaking(true);
                break;

            case Attack:
                player.setSneaking(false);
                break;
        }

        // TODO allow to work on any block
        base.onWrench(player, ForgeDirection.getOrientation(turtle.getDirection()).getOpposite());
        // FIXME only return success if it really was a success, e.g. when the tank was formed and not if it returns an error message
        return TurtleCommandResult.success(results.toArray());
    }

    @Override
    public IIcon getIcon(ITurtleAccess turtle, TurtleSide side) {
        return ModItems.wrench.getIconFromDamage(0);
    }

    @Override
    public void update(ITurtleAccess turtle, TurtleSide side) {}
}
