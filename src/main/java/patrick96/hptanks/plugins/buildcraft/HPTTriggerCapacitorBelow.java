/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.buildcraft;

import buildcraft.api.statements.IStatementContainer;
import buildcraft.api.statements.IStatementParameter;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.core.power.HPTPowerHandler;
import patrick96.hptanks.core.power.IPowerTile;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

public class HPTTriggerCapacitorBelow extends HPTTriggerBelow {
    
    public HPTTriggerCapacitorBelow(State state) {
        super(state, "capacitor");
    }
    
    @Override
    public void registerIcons(IIconRegister iconRegister) {
        below75 = iconRegister.registerIcon(Utils.getTexture("triggers/triggerEnergyCapacitorBelow75"));
        below50 = iconRegister.registerIcon(Utils.getTexture("triggers/triggerEnergyCapacitorBelow50"));
        below25 = iconRegister.registerIcon(Utils.getTexture("triggers/triggerEnergyCapacitorBelow25"));
    }
    
    @Override
    public boolean isTriggerActive(TileEntity tile, ForgeDirection side, IStatementContainer source, IStatementParameter[] parameters) {
        if (tile instanceof IPowerTile) {
            IPowerTile powerTile = (IPowerTile) tile;
            HPTPowerHandler powerHandler = powerTile.getPowerHandler();
            
            if(powerHandler == null) {
                return false;
            }
            
            double perc = powerHandler.getEnergyStoredRatio() * 100;
            return perc < state.perc;
        }
        
        return false;
    }
    
    @Override
    public String getDescription() {
        return StringUtils.localize("hpt.bc.gate.capacitor") + " " + super.getDescription();
    }
}
