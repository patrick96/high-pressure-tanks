/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.buildcraft;

import buildcraft.api.statements.IStatementContainer;
import buildcraft.api.statements.IStatementParameter;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.core.power.HPTPowerHandler;
import patrick96.hptanks.core.power.IPowerTile;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

public class HPTTriggerCapacitor extends HPTTrigger {
    
    public enum State {
        Full, Space, Empty, Contains
    }
    
    @SideOnly(Side.CLIENT)
    public IIcon full;
    @SideOnly(Side.CLIENT)
    public IIcon space;
    @SideOnly(Side.CLIENT)
    public IIcon empty;
    @SideOnly(Side.CLIENT)
    public IIcon contains;
    
    public State state;
    
    public HPTTriggerCapacitor(State state) {
        super("capacitor." + state.name().toLowerCase());
        this.state = state;
    }
    
    @Override
    public IIcon getIcon() {
        switch (state) {
            case Full:
                return full;
                
            case Space:
                return space;
                
            case Empty:
                return empty;
                
            case Contains:
                return contains;
                
            default:
                return null;
        }
    }
    
    @Override
    public void registerIcons(IIconRegister iconRegister) {
        full = iconRegister.registerIcon(Utils.getTexture("triggers/triggerEnergyCapacitorFull"));
        space = iconRegister.registerIcon(Utils.getTexture("triggers/triggerEnergyCapacitorSpace"));
        empty = iconRegister.registerIcon(Utils.getTexture("triggers/triggerEnergyCapacitorEmpty"));
        contains = iconRegister.registerIcon(Utils.getTexture("triggers/triggerEnergyCapacitorContains"));
    }
    
    @Override
    public String getDescription() {
        return StringUtils.localize("hpt.bc.gate.capacitor." + state.name().toLowerCase());
    }

    @Override
    public boolean isTriggerActive(TileEntity tile, ForgeDirection side, IStatementContainer source, IStatementParameter[] parameters) {
        if (tile instanceof IPowerTile) {
            IPowerTile powerTile = (IPowerTile) tile;
            HPTPowerHandler powerHandler = powerTile.getPowerHandler();
            
            if(powerHandler == null) {
                return false;
            }
            
            double energy = powerHandler.getEnergyStored();
            double maxEnergy = powerHandler.getMaxEnergyStored();
            
            switch (state) {
                case Full:
                    return Math.floor(Math.abs(energy - maxEnergy)) == 0;
                    
                case Empty:
                    return energy == 0;
                    
                case Contains:
                    return energy > 0;
                    
                case Space:
                    return maxEnergy - energy > 0;
                    
            }
        }
        
        return false;
    }
    
}
