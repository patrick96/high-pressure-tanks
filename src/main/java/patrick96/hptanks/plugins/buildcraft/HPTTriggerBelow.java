/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.buildcraft;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.IIcon;
import patrick96.hptanks.util.StringUtils;

public abstract class HPTTriggerBelow extends HPTTrigger {
    
    public enum State {
        Below75(75), Below50(50), Below25(25);
        
        public int perc;
        
        State(int perc) {
            this.perc = perc;
        }
    }
    
    @SideOnly(Side.CLIENT)
    public IIcon below75;
    @SideOnly(Side.CLIENT)
    public IIcon below50;
    @SideOnly(Side.CLIENT)
    public IIcon below25;
    
    public State state;
    
    public HPTTriggerBelow(State state, String uniqueTag) {
        super(uniqueTag + "." + state.name().toLowerCase());
        
        this.state = state;
    }
    
    @Override
    public IIcon getIcon() {
        switch (state) {
            case Below75:
                return below75;
                
            case Below50:
                return below50;
                
            case Below25:
                return below25;
                
            default:
                return null;
        }
    }
    
    @Override
    public String getDescription() {
        return StringUtils.localize("hpt.bc.gate.below") + " " + state.perc + "%";
    }
    
}
