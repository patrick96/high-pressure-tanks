/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.buildcraft;

import buildcraft.api.statements.IStatementContainer;
import buildcraft.api.statements.IStatementParameter;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.tile.hpt.TileEntityTankComponent;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

public class HPTTriggerFluidBelow extends HPTTriggerBelow {
    
    public HPTTriggerFluidBelow(State state) {
        super(state, "fluid");
    }
    
    @Override
    public void registerIcons(IIconRegister iconRegister) {
        below75 = iconRegister.registerIcon(Utils.getTexture("triggers/triggerTankBelow75"));
        below50 = iconRegister.registerIcon(Utils.getTexture("triggers/triggerTankBelow50"));
        below25 = iconRegister.registerIcon(Utils.getTexture("triggers/triggerTankBelow25"));
    }
    
    @Override
    public boolean isTriggerActive(TileEntity tile, ForgeDirection side, IStatementContainer source, IStatementParameter[] parameters) {
        if (tile instanceof IFluidHandler || (tile instanceof TileEntityTankComponent && ((TileEntityTankComponent) tile).isPartOfMultiBlock())) {
            IFluidHandler handler;

            if(tile instanceof TileEntityTankComponent) {
                handler = ((TileEntityTankComponent) tile).getTankManager();
            }
            else {
                handler = (IFluidHandler) tile;
            }
            
            if(handler == null) {
                return false;
            }

            for(IStatementParameter parameter : parameters) {
                FluidStack fluid = null;

                if(parameter != null && !Utils.isItemStackEmpty(parameter.getItemStack())) {
                    fluid = FluidContainerRegistry.getFluidForFilledItem(parameter.getItemStack());
                }

                FluidTankInfo[] tankInfo = handler.getTankInfo(side);

                if(tankInfo == null) {
                    continue;
                }

                for(FluidTankInfo info : tankInfo) {
                    FluidStack tankFluid = info.fluid;
                    if(FluidUtils.isFluidStackEmpty(tankFluid)) {
                        return true;
                    }

                    if(fluid == null || fluid.isFluidEqual(tankFluid)) {
                        int amount = tankFluid.amount;
                        int cap = info.capacity;
                        double perc = (double) amount / cap * 100D;
                        if(perc < state.perc) {
                            return true;
                        }
                    }

                }
            }
        }
        
        return false;
    }
    
    @Override
    public String getDescription() {
        return StringUtils.localize("hpt.bc.gate.tank") + " " + super.getDescription();
    }
}
