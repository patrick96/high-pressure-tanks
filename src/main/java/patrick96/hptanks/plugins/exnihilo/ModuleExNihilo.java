/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.exnihilo;

import exnihilo.registries.SieveRegistry;
import net.minecraft.init.Blocks;
import patrick96.hptanks.init.ModItems.ItemPlain;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.plugins.ICompatModule;

public class ModuleExNihilo implements ICompatModule {
    
    public ModuleExNihilo() {}
    
    @Override
    public void preInit() {}
    
    @Override
    public void init() {
        // Adds a 6% chance of a sieve sieving gravel producing graphite
        SieveRegistry.register(Blocks.gravel, 0, ItemPlain.Graphite.item, 0, 16);
    }
    
    @Override
    public void postInit() {}
    
    @Override
    public String getDependency() {
        return Reference.PLUGIN_EX_NIHILO_DEP;
    }
    
    @Override
    public String getCompatName() {
        return Reference.PLUGIN_EX_NIHILO_NAME;
    }
    
}
