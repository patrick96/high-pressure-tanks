/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.nei;

import codechicken.nei.PositionedStack;
import codechicken.nei.guihook.GuiContainerManager;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.blocks.BlockMachine;
import patrick96.hptanks.client.gui.rect.GuiRectangleEnergy;
import patrick96.hptanks.client.gui.rect.GuiRectangleFluid;
import patrick96.hptanks.client.util.GuiUtils;
import patrick96.hptanks.config.ConfigHandler;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.power.EnergySystem;
import patrick96.hptanks.core.power.PowerUtils;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.recipe.condition.BaseRecipeCondition;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static codechicken.lib.gui.GuiDraw.changeTexture;
import static patrick96.hptanks.util.StringUtils.localize;

public abstract class MachineRecipeHandler extends BaseRecipeHandler {

    @Override
    public List<String> handleTooltip(GuiRecipe gui, List<String> currenttip, int recipe) {
        if(GuiContainerManager.shouldShowTooltip(gui) && currenttip.size() == 0) {
            MachineRecipeInfo info = getRecipeInfo(recipe);
            Point pos = getRelativeMousePosition(gui);
            Point offset = gui.getRecipePosition(recipe);
            Rectangle fluidRect = getFluidRectangle();
            if(fluidRect != null) {
                fluidRect.translate(offset.x, offset.y);
                if(fluidRect.contains(pos)) {
                    FluidStack fluid = getFluidStack(recipe);
                    if(fluid != null) {
                        currenttip.add(FluidUtils.getFluidName(fluid.getFluid()));
                        String additionalInfo = getAdditionalFluidInformation(recipe);
                        if(!additionalInfo.equals("")) {
                            currenttip.add(additionalInfo);
                        }
                    }
                }
            }

            Rectangle energyRect = getEnergyRectangle();
            if(energyRect != null) {
                energyRect.translate(offset.x, offset.y);
                if(energyRect.contains(pos)) {
                    currenttip.add(localize("hpt.energy.used"));
                    float energyUsed = info.getEnergyPerTick() * info.getTime();
                    for(EnergySystem energySystem : ConfigHandler.displayedEnergySystems) {
                        currenttip.add(StringUtils.prettyPrintNumber(energySystem.convertBack(energyUsed), 0, false) + " " + energySystem.name());
                    }
                    currenttip.addAll(PowerUtils.getEnergyPerTickList(info.getEnergyPerTick()));
                }
            }

            Rectangle progressArrow = getProgressArrowRectangle();
            if(progressArrow != null) {
                progressArrow.translate(offset.x, offset.y);
                if(progressArrow.contains(pos)) {
                    super.handleTooltip(gui, currenttip, recipe);
                    currenttip.add(StringUtils.prettyPrintNumber(info.getTime() / 20F, 1, true) + " " + localize("hpt.recipe.seconds"));
                    return currenttip;
                }
            }

            List<BaseRecipeCondition> conditions = info.getConditions();
            for (int i = 0; i < conditions.size(); i++) {
                BaseRecipeCondition condition = conditions.get(i);
                Rectangle conditionRect = getConditionsRect(i);
                conditionRect.translate(offset.x, offset.y);
                if (conditionRect.contains(pos)) {
                    currenttip.addAll(condition.getTooltip());
                }
            }

        }

        return super.handleTooltip(gui, currenttip, recipe);
    }

    public boolean transferRects(GuiRecipe gui, int recipe, boolean usage) {
        return transferRectFluid(gui, recipe, usage) || transferRectCondition(gui, recipe, usage);
    }

    public boolean transferRectFluid(GuiRecipe gui, int recipe, boolean usage) {
        Rectangle fluidRect = getFluidRectangle();

        if(fluidRect == null) {
            return false;
        }

        Point pos = getRelativeMousePosition(gui);
        Point offset = gui.getRecipePosition(recipe);
        fluidRect.translate(offset.x, offset.y);
        if(fluidRect.contains(pos)) {
            FluidStack fluid = getFluidStack(recipe);
            if(fluid == null) {
                return false;
            }
            Object[] ingreds = new Object[] {fluid};
            if(usage) {
                GuiUsageRecipe.openRecipeGui("liquid", ingreds);
            }
            else {
                GuiCraftingRecipe.openRecipeGui("liquid", ingreds);
            }
            return true;
        }

        return false;
    }

    public boolean transferRectCondition(GuiRecipe gui, int recipe, boolean usage) {
        MachineRecipeInfo info = getRecipeInfo(recipe);
        List<BaseRecipeCondition> conditions = info.getConditions();

        for (int i = 0; i < conditions.size(); i++) {
            BaseRecipeCondition condition = conditions.get(i);

            if(!condition.hasTransferRect()) {
                continue;
            }

            Rectangle rect = getConditionsRect(i);

            Point pos = getRelativeMousePosition(gui);
            Point offset = gui.getRecipePosition(recipe);
            rect.translate(offset.x, offset.y);
            if (rect.contains(pos)) {
                condition.transfer(usage);
                return true;
            }
        }

        return false;
    }

    @Override
    public void loadTransferRects() {
        super.loadTransferRects();
        Rectangle progressArrow = getProgressArrowRectangle();
        if(progressArrow != null) {
            transferRects.add(new RecipeTransferRect(progressArrow, getOverlayIdentifier()));
        }
    }

    @Override
    public void loadCraftingRecipes(String outputId, Object... results) {
        if(outputId.equals(getOverlayIdentifier())) {
            for(MachineRecipeInfo info : getRecipeList()) {
                addMachineRecipe(getCachedMachineRecipe(info));
            }
        } else {
            super.loadCraftingRecipes(outputId, results);
        }
    }

    @Override
    public void loadCraftingRecipes(ItemStack result) {
        for(MachineRecipeInfo info : getRecipeList()) {
            if(info.hasStackResult() && Utils.areItemStacksEqualIgnoreStackSize(result, info.getResult())) {
                addMachineRecipe(getCachedMachineRecipe(info));
            }
        }
    }

    @Override
    public void loadUsageRecipes(ItemStack ingredient) {
        for(MachineRecipeInfo info : getRecipeList()) {
            if(info.hasStackInput() && info.getInput().matches(ingredient)) {
                addMachineRecipe(getCachedMachineRecipe(info));
            }
        }
    }

    public void addMachineRecipe(CachedMachineRecipe recipe) {
        if(recipe.info.hasStackInput() && recipe.info.getInput().getStacks().size() <= 0) {
            return;
        }
        arecipes.add(recipe);
    }

    /**
     * Wrapper method so that subclasses can add their own cached recipe classes w/o having to handle the recipe loading themselves
     * @param info The MachineRecipeInfo for the recipe
     * @return a CachedMachineRecipe instance
     */
    public CachedMachineRecipe getCachedMachineRecipe(MachineRecipeInfo info) {
        return new CachedMachineRecipe(info);
    }

    public abstract BlockMachine.MachineType getMachineType();

    @Override
    public String getGuiTexture() {
        return getMachineType().guiInfo.getGuiTexture().toString();
    }

    @Override
    public String getRecipeName() {
        return StringUtils.localize("container.hpt." + getOverlayIdentifier());
    }

    public abstract List<? extends MachineRecipeInfo> getRecipeList();

    @Override
    public void drawExtras(int recipe) {
        super.drawExtras(recipe);

        MachineRecipeInfo info = getRecipeInfo(recipe);

        Rectangle energyRect = getEnergyRectangle();
        if(energyRect != null) {
            GuiRectangleEnergy energyBar = new GuiRectangleEnergy(fakeGui, null, energyRect.x, energyRect.y, energyRect.width, energyRect.height);
            fakeGui.drawEnergyBar(energyBar, getMachineType().energyCap, getMachineType().energyCap, 0);
        }

        Rectangle fluidRect = getFluidRectangle();
        if(fluidRect != null) {
            TankManager tankManager = new TankManager(new FluidTank(new FluidStack(getFluidStack(recipe), Reference.BASIC_INTERNAL_TANK_STORAGE),
                    Reference.BASIC_INTERNAL_TANK_STORAGE));

            GuiRectangleFluid fluidBar = new GuiRectangleFluid(fakeGui, tankManager, fluidRect.x, fluidRect.y, GuiRectangleFluid.FluidScaleType.SMALL);
            fluidBar.draw();
        }

        Rectangle progressArrowRect = getProgressArrowRectangle();
        if(progressArrowRect != null) {
            changeTexture(GuiUtils.getGuiTexture("Icons"));
            drawProgressBar(progressArrowRect.x, progressArrowRect.y, 0, 64, progressArrowRect.width, progressArrowRect.height, (int) (info.getTime() / 5F), 0);
        }

        List<BaseRecipeCondition> conditions = info.getConditions();
        for (int i = 0; i < conditions.size(); i++) {
            BaseRecipeCondition condition = conditions.get(i);
            Rectangle conditionRect = getConditionsRect(i);
            GL11.glPushMatrix();
            GL11.glTranslated(conditionRect.getX(), conditionRect.getY(), 0);
            condition.drawExtras();
            GL11.glPopMatrix();
        }
    }

    public Point getInputPosition() {
        return new Point();
    }

    public Point getOutputPosition() {
        return new Point();
    }

    /**
     * @param n
     * @return the rect of the nth condition
     */
    public Rectangle getConditionsRect(int n) {
        Rectangle base = getBaseConditionalRect();
        return getOffsetRectangle(base.x + n * 18, base.y, base.width, base.height);
    }

    /**
     * Use new Rectangle for this and not getOffsetRectangle
     */
    public Rectangle getBaseConditionalRect() {
        return new Rectangle(33, 27, 16, 16);
    }

    /**
     * Method for subclasses to declare the position of their progress arrow
     * @return The rectangle of the progress arrow
     */
    public Rectangle getProgressArrowRectangle() {
        return null;
    }

    public Rectangle getEnergyRectangle() {
        return getOffsetRectangle(8, 8, 12, 59);
    }

    /**
     * Subclasses can return they're fluid rectangles here.
     * Multiple rectangles have to be managed manually
     * @return The Fluid Rectangle
     */
    public Rectangle getFluidRectangle() {
        return null;
    }

    /**
     * Methodes for machines to provide the GUI with information on the fluid in the current recipe
     * @param recipe the ID of the recipe that is shown
     * @return The FluidStack of the fluid that the machine uses or produces (in the getFluidRectangle())
     */
    public FluidStack getFluidStack(int recipe) {
        return null;
    }

    /**
     * Generates additional information in a fluid tooltip
     * Preferably telling the user how much is used/produced
     * @param recipe the recipe for which
     * @return the tooltip info
     */
    public String getAdditionalFluidInformation(int recipe) {
        return "";
    }

    @Override
    public CachedMachineRecipe getCachedRecipe(int recipe) {
        return (CachedMachineRecipe) super.getCachedRecipe(recipe);
    }

    public MachineRecipeInfo getRecipeInfo(int recipe) {
        return getCachedRecipe(recipe).info;
    }

    @Override
    public String getOverlayIdentifier() {
        return getMachineType().invName;
    }

    public class CachedMachineRecipe extends CachedRecipe {
        protected MachineRecipeInfo info;
        protected PositionedStack input = null, output = null;

        public CachedMachineRecipe(MachineRecipeInfo info) {
            this.info = info;

            if(info.hasStackInput()) {
                input = new PositionedStack(info.getInput().getStacks(), getInputPosition().x - getOffsetX(), getInputPosition().y - getOffsetY());
            }

            if(info.hasStackResult()) {
                output = new PositionedStack(info.getResult(), getOutputPosition().x - getOffsetX(), getOutputPosition().y - getOffsetY());
            }
        }

        @Override
        public PositionedStack getResult() {
            return output;
        }

        @Override
        public List<PositionedStack> getIngredients() {
            return input != null? getCycledIngredients(cycleticks / 20, Collections.singletonList(input)) : new ArrayList<PositionedStack>();
        }

        @Override
        public PositionedStack getIngredient() {
            return input;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof CachedMachineRecipe && ((CachedMachineRecipe) obj).info.equals(info);
        }
    }
}
