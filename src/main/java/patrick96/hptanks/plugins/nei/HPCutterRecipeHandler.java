/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.PositionedStack;
import codechicken.nei.guihook.GuiContainerManager;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;
import com.mojang.realmsclient.gui.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.blocks.BlockMachine;
import patrick96.hptanks.client.gui.GuiHPCutter;
import patrick96.hptanks.recipe.HPCutterRecipeInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.util.ItemStackIdentifier;
import patrick96.hptanks.util.StringUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Map;

public class HPCutterRecipeHandler extends MachineRecipeHandler {

    public HPCutterRecipeHandler() {
        super();
        randomLaserPositions();
    }

    @Override
    public List<String> handleTooltip(GuiRecipe gui, List<String> currenttip, int recipe) {
        if(GuiContainerManager.shouldShowTooltip(gui) && currenttip.size() == 0) {
            CachedHPCutterRecipe cachedRecipe = getCachedRecipe(recipe);
            Rectangle tierRect = getCutterTierRect();
            Point pos = getRelativeMousePosition(gui);
            Point offset = gui.getRecipePosition(recipe);

            if(tierRect != null) {
                tierRect.translate(offset.x, offset.y);
                if(tierRect.contains(pos)) {
                    currenttip.add(ChatFormatting.AQUA + new Formatter().format(StringUtils.localize("misc.hpt.mincuttertier"),
                            StringUtils.numberToRomanNumeral(getRecipeInfo(recipe).minimalCutterTier)).toString());
                }
            }
        }

        return super.handleTooltip(gui, currenttip, recipe);
    }

    @Override
    public void loadUsageRecipes(String inputId, Object... ingredients) {
        if(inputId.equals("cuttertier")) {
            int tier = (Integer) ingredients[0];
            for(HPCutterRecipeInfo info : HPTRegistry.hpCutterRecipes) {
                if(info.minimalCutterTier == tier) {
                    addMachineRecipe(getCachedMachineRecipe(info));
                }
            }
        }

        super.loadUsageRecipes(inputId, ingredients);
    }

    @Override
    public void loadCraftingRecipes(String outputId, Object... results) {
        if(outputId.equals("cuttertier")) {
            int tier = (Integer) results[0];
            for(HPCutterRecipeInfo info : HPTRegistry.hpCutterRecipes) {
                if(info.minimalCutterTier == tier) {
                    addMachineRecipe(getCachedMachineRecipe(info));
                }
            }
        }

        super.loadCraftingRecipes(outputId, results);
    }

    @Override
    public void loadUsageRecipes(ItemStack ingredient) {
        super.loadUsageRecipes(ingredient);
        HPCutterRecipeInfo.CutterConfiguration configuration = HPTRegistry.getCutter(ingredient);
        if(configuration != null) {
            for(MachineRecipeInfo info : getRecipeList()) {
                if(((HPCutterRecipeInfo) info).minimalCutterTier <= configuration.getTier()) {
                    addMachineRecipe(getCachedMachineRecipe(info));
                }
            }
        }
    }

    @Override
    public boolean transferRects(GuiRecipe gui, int recipe, boolean usage) {
        return super.transferRects(gui, recipe, usage) || transferRectCutterTier(gui, recipe, usage);
    }

    public boolean transferRectCutterTier(GuiRecipe gui, int recipe, boolean usage) {
        HPCutterRecipeInfo info = getRecipeInfo(recipe);

        Rectangle cutterTierRect = getCutterTierRect();

        if(cutterTierRect == null) {
            return false;
        }

        Point pos = getRelativeMousePosition(gui);
        Point offset = gui.getRecipePosition(recipe);
        cutterTierRect.translate(offset.x, offset.y);
        if(cutterTierRect.contains(pos)) {
            int tier = info.minimalCutterTier;
            if(usage) {
                GuiUsageRecipe.openRecipeGui("cuttertier", tier);
            } else {
                GuiCraftingRecipe.openRecipeGui("cuttertier", tier);
            }
            return true;
        }

        return false;
    }

    @Override
    public Point getInputPosition() {
        return new Point(33, 46);
    }

    @Override
    public Point getOutputPosition() {
        return new Point(93, 47);
    }

    @Override
    public Rectangle getBaseConditionalRect() {
        return new Rectangle(93, 25, 16, 16);
    }

    public Rectangle getCutterTierRect() {
        return getOffsetRectangle(32, 8, 16, 16);
    }

    @Override
    public Rectangle getProgressArrowRectangle() {
        return getOffsetRectangle(57, 47, 22, 16);
    }

    protected boolean[] laserPositions = new boolean[4];

    private void randomLaserPositions() {
        for(int i = 0; i < laserPositions.length; i++) {
            laserPositions[i] = Minecraft.getMinecraft().theWorld.rand.nextFloat() > 0.6;
        }
    }

    @Override
    public void drawExtras(int recipe) {
        super.drawExtras(recipe);
        if(cycleticks % 10 == 0) {
            randomLaserPositions();
        }
        Tessellator t = Tessellator.instance;
        GL11.glPushMatrix();
        GL11.glTranslatef(-getOffsetX(), -getOffsetY(), 0);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, 1, 0);
        GL11.glLineWidth(2);
        for(int i = 0; i < laserPositions.length; i++) {
            if(laserPositions[i]) {
                GL11.glColor4f(1F, 0, 0, 1F);
            } else {
                GL11.glColor4f(139 / 255F, 139 / 255F, 139 / 255F, 1F);
            }
            t.startDrawing(GL11.GL_LINES);
            t.addVertex(50, 39 - i * (18 / 4D), 0);
            t.addVertex(63 + (i / 2D), 53, 0);
            t.draw();
        }
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glPopMatrix();

        String minTier = StringUtils.numberToRomanNumeral(getRecipeInfo(recipe).minimalCutterTier);
        Rectangle tierRect = getCutterTierRect();
        GuiDraw.drawString(minTier, tierRect.x, tierRect.y, 0xFF55FF55);
    }

    @Override
    public BlockMachine.MachineType getMachineType() {
        return BlockMachine.MachineType.HP_CUTTER;
    }

    @Override
    public List<? extends MachineRecipeInfo> getRecipeList() {
        return HPTRegistry.hpCutterRecipes;
    }

    @Override
    public Class<? extends GuiContainer> getGuiClass() {
        return GuiHPCutter.class;
    }

    @Override
    public CachedMachineRecipe getCachedMachineRecipe(MachineRecipeInfo info) {
        return new CachedHPCutterRecipe(info);
    }

    @Override
    public HPCutterRecipeInfo getRecipeInfo(int recipe) {
        return (HPCutterRecipeInfo) super.getRecipeInfo(recipe);
    }

    @Override
    public CachedHPCutterRecipe getCachedRecipe(int recipe) {
        return (CachedHPCutterRecipe) super.getCachedRecipe(recipe);
    }

    public class CachedHPCutterRecipe extends CachedMachineRecipe {

        public CachedHPCutterRecipe(MachineRecipeInfo info) {
            super(info);
        }

        @Override
        public List<PositionedStack> getIngredients() {
            ArrayList<PositionedStack> list = new ArrayList<PositionedStack>();
            list.addAll(super.getIngredients());

            Map<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration> cutters = HPTRegistry.getCutters(((HPCutterRecipeInfo) info).minimalCutterTier);
            List<ItemStack> cutterStacks = new ArrayList<ItemStack>();

            for(Map.Entry<ItemStackIdentifier, HPCutterRecipeInfo.CutterConfiguration> entry : cutters.entrySet()) {
                cutterStacks.addAll(entry.getKey().getStacks());
            }

            if(cutterStacks.size() > 0) {
                list.add(new PositionedStack(cutterStacks, 33 - getOffsetX(), 25 - getOffsetY()));
            }
            return getCycledIngredients(cycleticks / 20, list);
        }
    }
}
