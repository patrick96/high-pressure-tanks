/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.nei;

import codechicken.nei.PositionedStack;
import codechicken.nei.guihook.GuiContainerManager;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.gui.rect.GuiRectangle;
import patrick96.hptanks.client.gui.rect.draw.GuiRectDrawType;
import patrick96.hptanks.client.gui.rect.draw.GuiRectTextured;
import patrick96.hptanks.client.util.GuiUtils;
import patrick96.hptanks.recipe.CVDFurnaceRecipeInfo;
import patrick96.hptanks.util.StringUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The Recipe Handler for all the Catalysts available
 */
public class CatalystRecipeHandler extends BaseRecipeHandler {

    @Override
    public void loadUsageRecipes(ItemStack ingredient) {
        for(CVDFurnaceRecipeInfo.Catalyst catalyst : CVDFurnaceRecipeInfo.Catalyst.values()) {
            if(catalyst.matches(ingredient)) {
                arecipes.add(new CachedCatalystRecipe(catalyst));
            }
        }
    }

    @Override
    public void loadCraftingRecipes(String outputId, Object... results) {
        if(outputId.equals("catalyst")) {
            for(CVDFurnaceRecipeInfo.Catalyst catalyst : CVDFurnaceRecipeInfo.Catalyst.values()) {
                if(results == null || results.length <= 0 || results[0] == catalyst) {
                    arecipes.add(new CachedCatalystRecipe(catalyst));
                }
            }
        }
        super.loadCraftingRecipes(outputId, results);
    }

    @Override
    public List<String> handleTooltip(GuiRecipe gui, List<String> currenttip, int recipe) {
        if(GuiContainerManager.shouldShowTooltip(gui) && currenttip.size() == 0) {
            CachedCatalystRecipe cachedRecipe = getCachedRecipe(recipe);
            Rectangle catalystRect = getCatalystRectangle();
            Point pos = getRelativeMousePosition(gui);
            Point offset = gui.getRecipePosition(recipe);

            if(catalystRect != null) {
                catalystRect.translate(offset.x, offset.y);
                if(catalystRect.contains(pos)) {
                    currenttip.add(cachedRecipe.catalyst.localize());
                }
            }
        }

        return super.handleTooltip(gui, currenttip, recipe);
    }

    @Override
    public boolean transferRects(GuiRecipe gui, int recipe, boolean usage) {
        return transferRectCatalyst(gui, recipe, usage);
    }

    public boolean transferRectCatalyst(GuiRecipe gui, int recipe, boolean usage) {
        Rectangle catalystRect = getCatalystRectangle();

        if(catalystRect != null) {
            Point pos = getRelativeMousePosition(gui);
            Point offset = gui.getRecipePosition(recipe);
            catalystRect.translate(offset.x, offset.y);
            if(catalystRect.contains(pos)) {
                CachedCatalystRecipe cachedRecipe = getCachedRecipe(recipe);
                Object[] ingreds = new Object[] {cachedRecipe.catalyst};
                if(usage) {
                    GuiUsageRecipe.openRecipeGui("catalyst", ingreds);
                } else {
                    GuiCraftingRecipe.openRecipeGui("catalyst", ingreds);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void drawExtras(int recipe) {
        super.drawExtras(recipe);
        Rectangle catalystRect = getCatalystRectangle();
        if(catalystRect != null) {
            CachedCatalystRecipe cachedRecipe = getCachedRecipe(recipe);
            CVDFurnaceRecipeInfo.Catalyst cat = cachedRecipe.catalyst;
            fakeGui.drawString(cat.getShortName(), fakeGui.getLeft() + catalystRect.x + 2, fakeGui.getTop() + catalystRect.y + 2, 0xFF202020);
            fakeGui.drawFrame(catalystRect.x + 2, catalystRect.y + 18, 26, 10, 0xFF202020);
            GuiRectDrawType barType = new GuiRectTextured(82, 0, GuiUtils.getGuiTexture("Icons"));
            GL11.glColor4f(1, 1, 1, 1);
            new GuiRectangle(fakeGui, barType, catalystRect.x + 3, catalystRect.y + 19, (int) (cycleticks % 40 / 40F * 24), 8).draw();
        }

        Rectangle progressArrowRect = getProgressArrow();
        if(progressArrowRect != null) {
            GL11.glColor4f(1, 1, 1, 1);
            GL11.glEnable(GL11.GL_ALPHA_TEST);
            new GuiRectTextured(23, 64, GuiUtils.getGuiTexture("Icons"))
                    .draw(fakeGui, progressArrowRect.x, progressArrowRect.y, progressArrowRect.width, progressArrowRect.height);
            GL11.glDisable(GL11.GL_ALPHA_TEST);
        }
    }

    @Override
    public void loadTransferRects() {
        super.loadTransferRects();
        transferRects.add(new RecipeTransferRect(getProgressArrow(), "catalyst"));
    }

    public Rectangle getProgressArrow() {
        return getOffsetRectangle(57, 25, 22, 16);
    }

    public Rectangle getCatalystRectangle() {
        return getOffsetRectangle(86, 19, 30, 30);
    }

    @Override
    public String getGuiTexture() {
        return GuiUtils.getGuiTexture("Catalyst").toString();
    }

    @Override
    public String getRecipeName() {
        return StringUtils.localize("hpt.catalyst");
    }

    @Override
    public int getGuiHeight() {
        return super.getGuiHeight() - getOffsetY();
    }

    @Override
    public CachedCatalystRecipe getCachedRecipe(int recipe) {
        return (CachedCatalystRecipe) super.getCachedRecipe(recipe);
    }

    public class CachedCatalystRecipe extends CachedRecipe {

        public final CVDFurnaceRecipeInfo.Catalyst catalyst;
        public final PositionedStack input;

        public CachedCatalystRecipe(CVDFurnaceRecipeInfo.Catalyst catalyst) {
            this.catalyst = catalyst;
            if(catalyst != null) {
                this.input = new PositionedStack(catalyst.stack.getStacks(), 33 - getOffsetX(), 24 - getOffsetY());
            }
            else {
                this.input = null;
            }
        }

        @Override
        public List<PositionedStack> getIngredients() {
            return input != null? getCycledIngredients(cycleticks / 20, Collections.singletonList(input)) : new ArrayList<PositionedStack>();
        }

        @Override
        public PositionedStack getResult() {
            return null;
        }
    }
}
