/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import patrick96.hptanks.client.renderer.block.BlockResinRenderer;
import patrick96.hptanks.lib.BlockInfo;

public class BlockResin extends BlockHPT {

    public BlockResin() {
        super(Material.clay, BlockInfo.BLOCK_RESIN_UNLOCALIZED_NAME);
        setBlockTextureName(BlockInfo.BLOCK_RESIN_TEXTURE);
        this.slipperiness = 0.8F;
        setHardness(10F);
        setResistance(10F);
        setHarvestLevel("pickaxe", 0);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public int getRenderType() {
        return BlockResinRenderer.renderId;
    }

    @Override
    public int getRenderBlockPass() {
        return 1;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess blockAccess, int x, int y, int z, int side) {
        return !Block.isEqualTo(this, blockAccess.getBlock(x, y, z)) && super.shouldSideBeRendered(blockAccess, x, y, z, side);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z) {
        return AxisAlignedBB.getBoundingBox(x, y, z, x + 1, y + 0.5, z + 1);
    }

    @Override
    public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
        // TODO when updating to 1.8 or later use the same code as the slime block, because there the motion when hitting the block is more accurate
        // The motion calculated this way is more consistent, since I don't really know if the motion here is m/s or m/t or whatever, I'll just use the position difference and try out coefficients
        double motion = entity.posY - entity.prevPosY;
        // the max motion when walking onto the block from the same level is about -0.23 so this check should prevent bouncing when stepping from a normal block onto this one
        if(motion < -0.4 && !entity.isSneaking()) {
            entity.motionY = -1.2 * motion;
        }
        entity.fallDistance = 0;
    }
}
