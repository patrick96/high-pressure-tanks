/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import patrick96.hptanks.client.renderer.ctm.CTMConfiguration;
import patrick96.hptanks.client.renderer.ctm.CTMGenerator;
import patrick96.hptanks.client.renderer.ctm.CTMHelper;
import patrick96.hptanks.client.renderer.ctm.ICTM;
import patrick96.hptanks.config.ConfigHandler;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.util.Coords;

import java.util.Random;

public class BlockGlassBulletProof extends BlockHPT implements ICTM {
    
    public BlockGlassBulletProof() {
        super(Material.glass, BlockInfo.GLASS_BULLETPROOF_UNLOCALIZED_NAME);
        setHardness(5F);
        setResistance(Integer.MAX_VALUE);
        setStepSound(soundTypeGlass);
        setBlockTextureName(BlockInfo.GLASS_BULLETPROOF_TEXTURE);
        setHarvestLevel("pickaxe", 2);
    }
    
    @SideOnly(Side.CLIENT)
    private IIcon[] icons;
    
    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(IBlockAccess blockAccess, int x, int y, int z, int side) {
        if(ConfigHandler.CTMBPGlass.getBoolean()) {
            return CTMHelper.getConnectedBlockTexture(this, blockAccess, x, y, z, side);
        }
        return super.getIcon(blockAccess, x, y, z, side);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register) {
        super.registerBlockIcons(register);

        if(ConfigHandler.CTMBPGlass.getBoolean()) {
            icons = CTMGenerator.generateTextures(this, register);
        }
    }
    
    @Override
    public int quantityDropped(int meta, int fortune, Random random) {
        return 1;
    }

    @Override
    public boolean canEntityDestroy(IBlockAccess world, int x, int y, int z, Entity entity) {
        return false;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public int getRenderBlockPass() {
        return 0;
    }
    
    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess blockAccess, int x, int y, int z, int side) {
        return !Block.isEqualTo(this, blockAccess.getBlock(x, y, z)) && super.shouldSideBeRendered(blockAccess, x, y, z, side);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldConnectToBlock(IBlockAccess par1IBlockAccess, Coords coords, int meta) {
        return Block.isEqualTo(this, coords.getBlock(par1IBlockAccess));
    }

    @Override
    public IIcon[] getIcons() {
        return icons;
    }

    @Override
    public int getBorderWidth() {
        return 1;
    }

    @Override
    public CTMConfiguration.TextureType getType() {
        return CTMConfiguration.TextureType.LINEAR;
    }

    @Override
    public int getCornerWidth() {
        return 0;
    }
}
