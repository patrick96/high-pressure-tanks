/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.blocks;

import net.minecraft.block.material.Material;
import patrick96.hptanks.lib.BlockInfo;

/**
 * A Class for all block versions of an item (graphite block etc.) that are not metal
 */
public class BlockCompressed extends BlockMulti {

    public BlockCompressed() {
        super(Material.rock, BlockInfo.COMPRESSED_BLOCK_UNLOCALIZED_NAME);
        setBlockTextureName(BlockInfo.COMPRESSED_BLOCK_TEXTURE);
        setTextureSuffixes(BlockInfo.COMPRESSED_BLOCK_TEXTURE_SUFFIX);


        setHardness(5.0F);
        setResistance(10.0F);
        setStepSound(soundTypePiston);
        setHarvestLevel("pickaxe", 1);
    }

}
