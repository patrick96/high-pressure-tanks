/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.init.ModItems.ItemPlain;
import patrick96.hptanks.lib.BlockInfo;

import java.util.ArrayList;
import java.util.Random;

public class BlockOre extends BlockMulti {
    
    public BlockOre() {
        super(Material.rock, BlockInfo.ORE_UNLOCALIZED_NAME);
        setBlockTextureName(BlockInfo.ORE_TEXTURE);

        setTextureSuffixes(BlockInfo.ORE_TEXTURE_SUFFIX);
        
        setHardness(3.0F);
        setResistance(5.0F);
        setStepSound(soundTypeStone);
        setHarvestLevel("pickaxe", 1);
    }
    
    @Override
    public Item getItemDropped(int meta, Random rand, int fortune) {
        switch(meta) {
            // Bauxite to Bauxite Dust
            case 0:
                return ModItems.dust;

            // Graphite
            case 2:
                return ItemPlain.Graphite.item;

            default:
                return super.getItemDropped(meta, rand, fortune);
        }
    }

    @Override
    public int quantityDropped(Random par1Random) {
        return 1;
    }

    /**
     * Returns the usual quantity dropped by the block plus a bonus of 1 to 'i' (inclusive).
     */
    @Override
    public int quantityDropped(int meta, int fortune, Random random) {
        // Only for Graphite
        if (fortune > 0 && Item.getItemFromBlock(this) != getItemDropped(meta, random, fortune)) {
            int amount = Math.max(0, random.nextInt(fortune + 2) - 1);
            return this.quantityDropped(random) * (amount + 1);
        }
        else {
            return this.quantityDropped(random);
        }
    }
    
    @Override
    public int getExpDrop(IBlockAccess world, int meta, int bonusLevel) {
        Random rand = HPTanks.RANDOM;
        if (getItemDropped(meta, rand, bonusLevel) != Item.getItemFromBlock(this)) {
            int amount = 0;

            // Bauxite
            if(meta == 0) {
                amount = MathHelper.getRandomIntegerInRange(rand, 2, 5);
            }
            // Graphite
            else if(meta == 2) {
                amount = MathHelper.getRandomIntegerInRange(rand, 3, 7);
            }

            return amount;
        }

        return 0;
    }
    
    @Override
    public int damageDropped(int meta) {
        switch(meta) {
            // Bauxite
            case 0:
            // Graphite
            case 2:
                return 0;

            default:
                return super.damageDropped(meta);
        }
    }

    @Override
    public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune) {
        ArrayList<ItemStack> list =  super.getDrops(world, x, y, z, metadata, fortune);

        if(metadata == 2) {
            int amount = (int) Math.round(Math.max(0, world.rand.nextDouble() * (fortune * 0.5 + 1)));
            list.add(ItemPlain.CNT.getStack(amount));
        }

        return list;
    }
}
