/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks.hpt;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import patrick96.hptanks.client.renderer.ctm.CTMConfiguration;
import patrick96.hptanks.client.renderer.ctm.CTMGenerator;
import patrick96.hptanks.client.renderer.ctm.CTMHelper;
import patrick96.hptanks.client.renderer.ctm.ICTM;
import patrick96.hptanks.config.ConfigHandler;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.tile.hpt.IHPTFrameComponent;
import patrick96.hptanks.tile.hpt.IHPTWallComponent;
import patrick96.hptanks.tile.hpt.TileEntityTankCasing;
import patrick96.hptanks.tile.hpt.TileEntityTankComponent;
import patrick96.hptanks.util.Coords;

public class BlockTankCasing extends BlockTankComponent implements ICTM {

    private IIcon[] icons;

    public BlockTankCasing() {
        super(BlockInfo.TANK_CASING_UNLOCALIZED_NAME);
        setBlockTextureName(BlockInfo.TANK_CASING_TEXTURE);
    }

    @Override
    public void registerBlockIcons(IIconRegister register) {
        super.registerBlockIcons(register);

        if(ConfigHandler.CTMTankCasing.getBoolean()) {
            icons = CTMGenerator.generateTextures(this, register);
        }
    }

    @Override
    public IIcon getIcon(IBlockAccess blockAccess, int x, int y, int z, int side) {
        if(ConfigHandler.CTMTankCasing.getBoolean()) {
            return CTMHelper.getConnectedBlockTexture(this, blockAccess, x, y, z, side);
        }
        return super.getIcon(blockAccess, x, y, z, side);
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileEntityTankCasing();
    }

    @Override
    public int getBorderWidth() {
        return 1;
    }

    @Override
    public boolean shouldConnectToBlock(IBlockAccess blockAccess, Coords coords, int meta) {
        TileEntity tile = coords.getTileEntity(blockAccess);
        return tile instanceof TileEntityTankComponent && tile instanceof IHPTFrameComponent && !(tile instanceof IHPTWallComponent)
                && ((TileEntityTankComponent) tile).isPartOfMultiBlock() && Block.isEqualTo(this, coords.getBlock(blockAccess));
    }

    @Override
    public IIcon[] getIcons() {
        return icons;
    }

    @Override
    public CTMConfiguration.TextureType getType() {
        return CTMConfiguration.TextureType.LINEAR;
    }

    @Override
    public int getCornerWidth() {
        return 0;
    }
}
