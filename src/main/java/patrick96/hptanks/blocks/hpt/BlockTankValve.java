/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks.hpt;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.tile.hpt.TileEntityTankValve;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Helper;
import patrick96.hptanks.util.Utils;

public class BlockTankValve extends BlockTankComponent {
    
    public BlockTankValve() {
        super(BlockInfo.TANK_VALVE_UNLOCALIZED_NAME);
        setBlockTextureName(BlockInfo.TANK_VALVE_TEXTURE);
    }
    
    @SideOnly(Side.CLIENT)
    protected IIcon valveSide;
    @SideOnly(Side.CLIENT)
    protected IIcon valveOutput;
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register) {
        super.registerBlockIcons(register);
        valveSide = register.registerIcon(Utils.getTexture(BlockInfo.TANK_VALVE_SIDE_TEXTURE));
        valveOutput = register.registerIcon(Utils.getTexture("TankValveOrange"));
        
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(IBlockAccess blockAccess, int x, int y, int z, int side) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(blockAccess);
        if (tile instanceof TileEntityTankValve) {
            TileEntityTankValve valve = (TileEntityTankValve) tile;
            
            
            if(valve.isPartOfMultiBlock()) {
                if(Helper.isComponent(blockAccess, coords.add(ForgeDirection.getOrientation(side)))) {
                    return valveSide;
                }
                else if (valve.doEject()) {
                    return valveOutput;
                }
            }
        }
        
        return super.getIcon(blockAccess, x, y, z, side);
    }
    
    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileEntityTankValve();
    }
}
