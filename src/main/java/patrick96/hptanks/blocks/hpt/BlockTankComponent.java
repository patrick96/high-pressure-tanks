/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks.hpt;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import patrick96.hptanks.blocks.BlockHPT;
import patrick96.hptanks.tile.hpt.TileEntityTankComponent;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Helper;

public abstract class BlockTankComponent extends BlockHPT {
    
    public BlockTankComponent(String unlocalizedName) {
        super(Material.iron, unlocalizedName);
        setHardness(10F);
        setResistance(Integer.MAX_VALUE);
        setStepSound(soundTypeMetal);
        setHarvestLevel("pickaxe", 2);
    }

    @Override
    public boolean hasTileEntity(int metadata) {
        return true;
    }
    
    @Override
    public int getMobilityFlag() {
        return 2;
    }
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float par7, float par8, float par9) {
        if(super.onBlockActivated(world, x, y, z, player, side, par7, par8, par9)) {
            return true;
        }
        
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        
        if (Helper.isComponent(world, coords)) {
            TileEntityTankComponent component = (TileEntityTankComponent) tile;
            
            if (world.isRemote) {
                return component.isPartOfMultiBlock();
            }

            component.openGui(player);

            return component.isPartOfMultiBlock();
            
        }
        
        return false;
    }

    @Override
    public boolean canEntityDestroy(IBlockAccess world, int x, int y, int z, Entity entity) {
        return !(entity instanceof EntityWither) && super.canEntityDestroy(world, x, y, z, entity);
    }

    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, Block block) {
        super.onNeighborBlockChange(world, x, y, z, block);
        
        if (world.isRemote) {
            return;
        }
        
        TileEntity tile = new Coords(x, y, z).getTileEntity(world);
        if (tile instanceof TileEntityTankComponent) {
            ((TileEntityTankComponent) tile).onChanged();
        }
    }
}
