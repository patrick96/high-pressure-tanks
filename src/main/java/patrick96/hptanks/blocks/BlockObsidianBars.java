/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockPane;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.util.Utils;

public class BlockObsidianBars extends BlockPane {
    
    public BlockObsidianBars(String texture) {
        super(Utils.getTexture(texture), Utils.getTexture(texture), Material.iron, true);
        setHardness(10F);
        setResistance(Integer.MAX_VALUE);
        setHarvestLevel("pickaxe", 2);
        setBlockName("hpt." + BlockInfo.OBSIDIAN_BARS_UNLOCALIZED_NAME);
        setStepSound(soundTypeMetal);
        setCreativeTab(HPTanks.tabsHPT);
    }
    
    @Override
    public boolean canPaneConnectTo(IBlockAccess access, int x, int y, int z, ForgeDirection dir) {
        return super.canPaneConnectTo(access, x, y, z, dir) || Block.isEqualTo(access.getBlock(x+dir.offsetX, y+dir.offsetY, z+dir.offsetZ), ModBlocks.tankControl);
    }
}
