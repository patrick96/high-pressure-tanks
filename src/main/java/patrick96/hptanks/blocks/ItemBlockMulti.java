/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import patrick96.hptanks.util.StringUtils;

import java.util.List;

public class ItemBlockMulti extends ItemBlock {
    
    protected String[] unlocalizedSuffixes;
    
    public ItemBlockMulti(Block block) {
        super(block);
        setHasSubtypes(true);
    }
    
    public void setSuffixes(String... names) {
        unlocalizedSuffixes = names;
    }
    
    @Override
    public int getMetadata(int par1) {
        return par1;
    }
    
    @Override
    public void getSubItems(Item item, CreativeTabs par2CreativeTabs, List list) {
        for (int i = 0; i < unlocalizedSuffixes.length; i++) {
            list.add(new ItemStack(item, 1, i));
        }
    }
    
    @Override
    public String getUnlocalizedName(ItemStack item) {
        int damage = MathHelper.clamp_int(item.getItemDamage(), 0, unlocalizedSuffixes.length - 1);
        return this.getUnlocalizedName() + "." + unlocalizedSuffixes[damage];
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean p_77624_4_) {
        super.addInformation(stack, player, list, p_77624_4_);

        if(StringUtils.hasLocalization(this.getUnlocalizedName() + ".descr")) {
            list.add(StringUtils.localize(this.getUnlocalizedName(stack) + ".descr"));
        }
    }
    
}
