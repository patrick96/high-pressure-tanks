/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.blocks;

import net.minecraft.block.BlockStairs;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.HPTanks;

public class BlockStairsHPT extends BlockStairs {

    // The Unlocalized name without any prefixes, so that I don't have to cut them
    public final String unlocalizedNameRaw;

    public final BlockHPT blockBase;

    public BlockStairsHPT(BlockHPT block, int meta) {
        super(block, meta);
        setCreativeTab(HPTanks.tabsHPT);

        this.useNeighborBrightness = true;
        this.blockBase = block;

        setHarvestLevel(blockBase.getHarvestTool(0), blockBase.getHarvestLevel(0));

        unlocalizedNameRaw = block.unlocalizedNameRaw + ".stairs";
        setBlockName("hpt." + unlocalizedNameRaw);
    }

    public ItemStack getStack() {
        return getStack(1, 0);
    }

    public ItemStack getStack(int size) {
        return getStack(size, 0);
    }

    public ItemStack getStack(int size, int meta) {
        return new ItemStack(this, size, meta);
    }
}
