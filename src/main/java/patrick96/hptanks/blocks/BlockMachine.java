/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidRegistry;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.tile.machine.*;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Utils;

import java.util.Random;

public class BlockMachine extends BlockMulti {

    private IIcon bottomIcon;
    private IIcon topIcon;
    /* The top icon of the regular machine block */
    private IIcon blockTopIcon;
    private IIcon sideIcon;
    private IIcon[] activeIcons;
    /* Icon to be rendered on pass 1 when machine doesn't need one */
    private IIcon transparentIcon;

    /* This information is specific to the block that is currently rendered, it helps canRenderInPass() and getIcon() to communicate */
    public int renderPass;

    public BlockMachine() {
        super(Material.iron, BlockInfo.MACHINE_UNLOCALIZED_NAME);
        setBlockTextureName(BlockInfo.MACHINE_TEXTURE);
        setTextureSuffixes(BlockInfo.MACHINE_TEXTURE_SUFFIX);

        setHardness(15F);
        setResistance(10F);
        setHarvestLevel("pickaxe", 2);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        if(super.onBlockActivated(world, x, y, z, player, par6, par7, par8, par9)) {
            return true;
        }

        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof TileEntityMachine && !player.isSneaking()) {
            if(world.isRemote) {
                return true;
            }

            player.openGui(HPTanks.instance, ((TileEntityMachine) tile).getGuiInfo().getID(), world, x, y, z);
            return true;
        }

        return false;
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        if(metadata == 0) {
            return null;
        }

        switch(MachineType.values()[metadata - 1]) {
            case WIRE_MILL:
                return new TileEntityWireMill();
            case RESIN_EXTRACTOR:
                return new TileEntityResinExtractor();
            case CVD_FURNACE:
                return new TileEntityCVDFurnace();
            case HP_CUTTER:
                return new TileEntityHPCutter();
            case RUBBER_FORMER:
                return new TileEntityRubberFormer();
            default:
                return null;
        }
    }

    @Override
    public boolean hasTileEntity(int metadata) {
        return metadata != 0;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register) {
        super.registerBlockIcons(register);

        if(textureSuffixes != null) {
            activeIcons = new IIcon[textureSuffixes.length];

            for(int i = 0; i < icons.length; i++) {
                String suffix = textureSuffixes[i];
                String name = getTextureName() + textureSuffixes[i] + "Active";
                if(!textureSuffixes[i].equals("") && Utils.resourceExists(Utils.getBlockResourceLocationFromTexure(name, "hpt"))) {
                    activeIcons[i] = register.registerIcon(Utils.getTexture(name));
                }
                else {
                    activeIcons[i] = null;
                }
            }
        }

        bottomIcon = register.registerIcon(Utils.getTexture(getTextureName() + "Bottom"));
        /* The machine block should have a different top texture. */
        blockTopIcon = register.registerIcon(Utils.getTexture(getTextureName() + "BlockTop"));
        topIcon = register.registerIcon(Utils.getTexture(getTextureName() + "Top"));
        sideIcon = register.registerIcon(Utils.getTexture(getTextureName() + "Side"));
        transparentIcon = register.registerIcon(Utils.getTexture(getTextureName() + "Transparent"));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        switch (side) {
            case 0:
                return bottomIcon;
            case 1:
                return meta == 0? blockTopIcon : topIcon;
            case 3:
                return meta == 0? sideIcon : icons[meta];
            default:
                return sideIcon;
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(IBlockAccess blockAccess, int x, int y, int z, int side) {
        Coords coords = new Coords(x, y, z);
        int meta = coords.getBlockMetadata(blockAccess);

        TileEntity tile = coords.getTileEntity(blockAccess);

        if(!(tile instanceof TileEntityMachine)) {
            return super.getIcon(blockAccess, x, y, z, side);
        }

        TileEntityMachine machine = (TileEntityMachine) tile;
        int rotation = machine.getRotation().ordinal();

        /* If the active Icon of the machine should be rendered */
        boolean renderActive = machine.isActive() && side == rotation && hasActiveIcon(meta);

        if(renderActive) {
            if(renderPass == 1) {
                return activeIcons[meta];
            } else {
                switch(MachineType.values()[meta - 1]) {
                    case RESIN_EXTRACTOR:
                    case RUBBER_FORMER:
                        return FluidUtils.getFluidIcon(ModBlocks.fluidResin);

                    case CVD_FURNACE:
                        return FluidUtils.getFluidIcon(FluidRegistry.WATER);

                    default:
                        return null;
                }
            }
        }

        /**
         *  If no double pass rendering is needed, only render in pass 0,
         *  to prevent the machine from being rendered over everything else
         */

        if(renderPass == 1) {
            return transparentIcon;
        }

        if(side == 0) {
            return bottomIcon;
        } else if(side == 1) {
            return meta == 0 ? blockTopIcon : topIcon;
        } else if(side == rotation) {
            return icons[meta];
        } else {
            return sideIcon;
        }
    }

    public boolean hasActiveIcon(int meta) {
        return activeIcons[meta] != null;
    }

    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack) {
        int direction = MathHelper.floor_double((double)(entity.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
        Coords coords = new Coords(x, y, z);

        TileEntity tile = coords.getTileEntity(world);
        if(tile instanceof TileEntityMachine && ((TileEntityMachine) tile).isRotatable()) {
            byte rotation = 3;

            switch(direction) {
                case 0:
                    rotation = 2;
                    break;

                case 1:
                    rotation = 5;
                    break;

                case 2:
                    rotation = 3;
                    break;

                case 3:
                    rotation = 4;
                    break;
            }

            ((TileEntityMachine) tile).setRotation(rotation);
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World world, int x, int y, int z, Random rand)
    {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        if(tile instanceof TileEntityMachine && ((TileEntityMachine) tile).isActive()) {
            float xCoord = (float) x + 0.5F;
            float yCoord = (float) y + 1.02F;
            float zCoord = (float) z + 0.5F;
            for(int i = 0; i < 6; i++) {
                float f1 = (rand.nextFloat() - 0.5F) * 0.1F;
                float f2 = (rand.nextFloat() - 0.5F) * 0.1F;
                float f3 = (rand.nextFloat() - 0.5F) * 0.1F;
                world.spawnParticle("smoke", xCoord + f1, yCoord + f2, zCoord + f3, 0.0D, 0.0D, 0.0D);
            }
        }
    }

    @Override
    public int tickRate(World p_149738_1_) {
        return 1;
    }

    @Override
    public boolean hasComparatorInputOverride() {
        return true;
    }

    @Override
    public boolean canRenderInPass(int pass)
    {
        renderPass = pass;
        return true;
    }

    @Override
    public int getRenderBlockPass() {
        return 1;
    }

    public enum MachineType {
        WIRE_MILL(Reference.BASIC_ENERGY_STORAGE_CAPACITY, TileEntityWireMill.guiInfo),
        RESIN_EXTRACTOR(Reference.BASIC_ENERGY_STORAGE_CAPACITY, TileEntityResinExtractor.guiInfo),
        CVD_FURNACE(Reference.BASIC_ENERGY_STORAGE_CAPACITY * 8, TileEntityCVDFurnace.guiInfo),
        HP_CUTTER(Reference.BASIC_ENERGY_STORAGE_CAPACITY * 8, TileEntityHPCutter.guiInfo),
        RUBBER_FORMER(Reference.BASIC_ENERGY_STORAGE_CAPACITY, TileEntityRubberFormer.guiInfo);

        public final int energyCap;
        // The guiTexture is only the name of the file w/o the Gui prefix
        public final String invName;
        public final GuiInfo guiInfo;

        MachineType(int maxEnergy, GuiInfo guiInfo) {
            this(maxEnergy, guiInfo.getTextureName().toLowerCase(), guiInfo);
        }

        MachineType(int maxEnergy, String invName, GuiInfo guiInfo) {
            this.energyCap = maxEnergy;
            this.invName = invName;
            this.guiInfo = guiInfo;
        }
    }
}
