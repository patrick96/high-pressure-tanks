/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import patrick96.hptanks.util.Utils;

public class BlockMulti extends BlockHPT {
    
    protected String[] textureSuffixes;
    @SideOnly(Side.CLIENT)
    protected IIcon[] icons;
    
    public BlockMulti(Material material, String unlocalizedName) {
        super(material, unlocalizedName);
    }
    
    
    public void setTextureSuffixes(String... textures) {
        textureSuffixes = textures;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return icons[MathHelper.clamp_int(meta, 0, icons.length - 1)];
    }
    
    @Override
    public int damageDropped(int meta) {
        return meta;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register) {
        if(textureSuffixes == null) {
            return;
        }
        
        icons = new IIcon[textureSuffixes.length];
        
        for (int i = 0; i < icons.length; i++) {
            if(!textureSuffixes[i].equals("")) {
                icons[i] = register.registerIcon(Utils.getTexture(getTextureName() + textureSuffixes[i]));
            }
        }
    }
}
