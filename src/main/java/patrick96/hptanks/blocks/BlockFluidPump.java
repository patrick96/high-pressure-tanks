/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.client.renderer.block.FluidPumpRenderer;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.tile.TileEntityFluidPump;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class BlockFluidPump extends BlockHPT {
    
    @SideOnly(Side.CLIENT)
    public IIcon headIcon;
    @SideOnly(Side.CLIENT)
    public IIcon poweredIcon;
    
    public static final float[] defaultBounds = {
        0.3F, // MinX
        0.3F, // MinY
        0.3F, // MinZ
        0.7F, // MaxX
        0.7F, // MaxY
        0.7F // MaxZ
    };
    
    public BlockFluidPump() {
        super(Material.iron, BlockInfo.FLUID_PUMP_UNLOCALIZED_NAME);
        setBlockTextureName("FluidPump");
        
        setHardness(5.0F);
        setResistance(Integer.MAX_VALUE);
        setHarvestLevel("pickaxe", 2);
        setStepSound(soundTypeMetal);
    }
    
    @Override
    public void registerBlockIcons(IIconRegister register) {
        super.registerBlockIcons(register);
        
        poweredIcon = register.registerIcon(Utils.getTexture(getTextureName()) + "Powered");
        headIcon = register.registerIcon(Utils.getTexture("FluidPumpHead"));
    }
    
    @Override
    public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
        TileEntity tile = world.getTileEntity(x, y, z);
        
        if(tile instanceof TileEntityFluidPump && ((TileEntityFluidPump) tile).isLocked()) {
            return poweredIcon;
        }
        else {
            return super.getIcon(world, x, y, z, side);
        }
    }

    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, Block block) {
        super.onNeighborBlockChange(world, x, y, z, block);
        
        
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        
        if(tile != null && tile instanceof TileEntityFluidPump) {
            TileEntityFluidPump pump = (TileEntityFluidPump) tile;
            pump.scheduleUpdateConnections();
        }
    }
    
    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        
        float minX = defaultBounds[0];
        float minY = defaultBounds[1];
        float minZ = defaultBounds[2];
        float maxX = defaultBounds[3];
        float maxY = defaultBounds[4];
        float maxZ = defaultBounds[5];
        
        if(tile != null && tile instanceof TileEntityFluidPump) {
            TileEntityFluidPump pump = (TileEntityFluidPump) tile;
            List<ForgeDirection> connections = pump.getConnections();
            List<ForgeDirection> occupiedSides = new ArrayList<ForgeDirection>();
            occupiedSides.addAll(connections);
            occupiedSides.add(pump.getRotation());
            
            for(ForgeDirection dir : occupiedSides) {
                int side = dir.ordinal();
                if(side > 1) {
                    maxY = Math.max(0.875F, maxY);
                    minY = Math.min(0.125F, minY);
                }
                
                if(side == 0 || side == 1) {
                    maxX = Math.max(0.875F, maxX);
                    minX = Math.min(0.125F, minX);
                    maxZ = Math.max(0.875F, maxZ);
                    minZ = Math.min(0.125F, minZ);
                }
                else if(side == 2 || side == 3) {
                    maxY = Math.max(0.875F, maxY);
                    minY = Math.min(0.125F, minY);
                    maxX = Math.max(0.875F, maxX);
                    minX = Math.min(0.125F, minX);
                }
                else if(side == 4 || side == 5) {
                    maxY = Math.max(0.875F, maxY);
                    minY = Math.min(0.125F, minY);
                    maxZ = Math.max(0.875F, maxZ);
                    minZ = Math.min(0.125F, minZ);
                }
                
                switch(side) {
                    case 0:
                        minY = 0;
                        break;
                        
                    case 1:
                        maxY = 1;
                        break;
                        
                    case 2:
                        minZ = 0;
                        break;
                        
                    case 3:
                        maxZ = 1;
                        break;
                        
                    case 4:
                        minX = 0;
                        break;
                        
                    case 5:
                        maxX = 1;
                        break;
                }
            }
        }
        setBlockBounds(minX, minY, minZ, maxX, maxY, maxZ);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z) {
        setBlockBoundsBasedOnState(world, x, y, z);
        return super.getSelectedBoundingBoxFromPool(world, x, y, z);
    }
    
    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z) {
        setBlockBoundsBasedOnState(world, x, y, z);
        return super.getCollisionBoundingBoxFromPool(world, x, y, z);
    }   
    
    @Override
    public MovingObjectPosition collisionRayTrace(World world, int x, int y, int z, Vec3 start, Vec3 end) {
        setBlockBoundsBasedOnState(world, x, y, z);
        return super.collisionRayTrace(world, x, y, z, start, end);
    }       
    
    @Override
    public void setBlockBoundsForItemRender() {
        setBlockBounds(defaultBounds[0], defaultBounds[1], defaultBounds[2], defaultBounds[3], defaultBounds[4], defaultBounds[5]);
    }
    
    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock()
    {
        return false;
    }
    
    @Override
    public int getRenderType() {
        return FluidPumpRenderer.renderId;
    }
    
    @Override
    public int getRenderBlockPass() {
        return 1;
    }

    @Override
    public boolean hasTileEntity(int metadata) {
        return true;
    }
    
    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileEntityFluidPump();
    }
    
    @Override
    public boolean hasComparatorInputOverride() {
        return true;
    }
}
