/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import patrick96.hptanks.core.inventory.IHPTSidedInventory;
import patrick96.hptanks.core.inventory.InventoryHandler;

public abstract class TileEntityInventory extends TileEntityHPTBase implements IHPTSidedInventory {
    
    protected InventoryHandler inventoryHandler = new InventoryHandler(this);
    
    public TileEntityInventory() {
        super();
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        inventoryHandler.writeToNBT(compound);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        inventoryHandler.readFromNBT(compound);
    }
    
    @Override
    public InventoryHandler getInventoryHandler() {
        return inventoryHandler;
    }
    
    @Override
    public int[] getAccessibleSlotsFromSide(int side) {
        return inventoryHandler.getAccessibleSlotsFromSide(side);
    }
    
    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer) {
        return inventoryHandler.isUseableByPlayer(entityplayer);
    }

    @Override
    public int getSizeInventory() {
        return inventoryHandler.getSizeInventory();
    }

    @Override
    public ItemStack getStackInSlot(int i) {
       return inventoryHandler.getStackInSlot(i);
    }

    @Override
    public ItemStack decrStackSize(int i, int amount) {
        return inventoryHandler.decrStackSize(i, amount);
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i) {
        return inventoryHandler.getStackInSlotOnClosing(i);
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack itemstack) {
        inventoryHandler.setInventorySlotContents(i, itemstack);
    }

    @Override
    public String getInventoryName() {
        return inventoryHandler.getInvName();
    }

    @Override
    public boolean hasCustomInventoryName() {
        return inventoryHandler.isInvNameLocalized();
    }

    @Override
    public int getInventoryStackLimit() {
        return inventoryHandler.getInventoryStackLimit();
    }
    
    @Override
    public boolean canInsertItem(int slotNum, ItemStack stack, int sideNum) {
        return inventoryHandler.canInsertItem(slotNum, stack, sideNum);
    }
    
    @Override
    public boolean canExtractItem(int slotNum, ItemStack stack, int sideNum) {
        return inventoryHandler.canExtractItem(slotNum, stack, sideNum);
    }

    @Override
    public void openInventory() {}

    @Override
    public void closeInventory() {}

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemstack) {
        return true;
    }
    
}
