/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile.machine;

import io.netty.buffer.ByteBuf;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.blocks.BlockMachine.MachineType;
import patrick96.hptanks.client.gui.IGuiTile;
import patrick96.hptanks.client.gui.tabs.GuiTab.TabSide;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.core.power.PowerUtils;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.tile.TileEntityPowered;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.upgrades.types.UpgradeSpeed;
import patrick96.hptanks.util.Helper;
import patrick96.hptanks.util.Utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class TileEntityMachine extends TileEntityPowered implements IGuiTile/*, IPeripheral*/ {

    public static GuiInfo guiInfo;

    protected int progress = 0;
    /**
     * Client Only flag, sent from the server to tell the Client-TE if it is currently processing something
     */
    protected boolean isActive = false;

    /** the tabs that are currently open on each side */
    protected static Map<TabSide, Integer> openTabs = new HashMap<TabSide, Integer>();
    protected MachineRecipeInfo currentRecipe = null;
    
    protected MachineType type;

    protected ItemStack inputLastCheck = null;

    public TileEntityMachine(MachineType type) {
        super(type.energyCap);
        this.type = type;
        inventoryHandler.setName(type.invName);
    }

    @Override
    public void updateEntity() {
        super.updateEntity();

        if(worldObj.isRemote) {
            return;
        }


        if(!(this instanceof ICustomProcess)) {
            updateProcess();
        }

        if (tick % 60 == 0) {
            ejectItem();
        }

    }

    @Override
    protected void init() {
        super.init();
        newInput(true);
    }

    public void updateProcess() {
        if (couldProcess()) {
            if(hasEnoughResources()) {
                progress++;
                process();
                if (progress >= getProcessTime()) {
                    progress = 0;
                    if(couldProcess()) {
                        processFinished();
                        updateProcess();
                    }
                }
                markTileDirty();
            }
        } else {
            progress = 0;
        }
    }

    public MachineRecipeInfo getCurrentRecipe() {
        return currentRecipe;
    }

    public boolean isActive() {
        return worldObj.isRemote? isActive : progress > 0 && hasEnoughResources();
    }

    public float getSpeedUpgradeMultiplier() {
        Upgrade up = upgradeManager.getUpgrade(UpgradeType.SPEED);
        if(up != null) {
            return ((UpgradeSpeed) up).getSpeedPercentage();
        }

        return 1;
    }

    public int getProcessTime() {
        int processTime = currentRecipe == null? 0 : currentRecipe.getTime();
        Upgrade up = upgradeManager.getUpgrade(UpgradeType.SPEED);
        if(up != null && processTime > 0) {
            return Math.max(Math.round(processTime * ((UpgradeSpeed) up).getSpeedPercentage()), 1);
        }
        
        return processTime;
    }
    
    @Override
    public boolean canDismantle() {
        return true;
    }

    public boolean isRotatable() {
        return getCoords().getBlockMetadata(worldObj) != 0;
    }
    
    @Override
    public boolean onlyYAxisRotation() {
        return true;
    }
    
    @Override
    public void markDirty() {
        super.markDirty();

        if(!isInit()) {
            return;
        }

        if(!Utils.areItemStacksEqualIgnoreStackSize(getInputStack(), inputLastCheck)) {
            newInput();
            inputLastCheck = getInputStack();
        }
    }

    /**
     * Called everytime when the item in the input slot changes to another item
     */
    public void newInput() {
        newInput(false);
    }

    /**
     * Called everytime when the item in the input slot changes to another item
     * @param init if this call comes from the init method (prevents the reset of progress)
     */
    public void newInput(boolean init) {
        MachineRecipeInfo newInfo = getRecipeInfo();

        if(newInfo == currentRecipe) {
            return;
        }

        currentRecipe = newInfo;
        if(!init) {
            progress = 0;
        }
        markTileDirty();
    }

    /**
     * Ejects the items of the Output slot to the first available inventory
     * Machines w/o output slot don't have to override this because they don't accept the item auto eject upgrade
     * TODO round-robin mode (Upgrade?)
     */
    public void ejectItem() {
        if(!upgradeManager.hasUpgrade(UpgradeType.ITEM_AUTO_EJECT)) {
            return;
        }

        for(ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS) {
            TileEntity tile = getCoords().add(dir).getTileEntity(worldObj);
            if(tile instanceof IInventory && !Utils.isItemStackEmpty(getOutputStack())) {
                setInventorySlotContents(getOutputSlotNumber(), Utils.addStackToInv((IInventory) tile, getOutputStack(), dir.getOpposite()));
            }
        }
    }
    
    public float getEnergyPerTick() {
        /*
         * The EPT can just be divided by the SpeedUpgrade multiplier, since otherwise the EPT would be multiplied by the time in the recipe
         * and then divided by the actual time the recipe needs, which is just the recipe time multiplied by the SpeedUpgrade multiplier, so the times cancel
         * each other out
         * This way also the EPT isn't influenced by time multipliers in other parts of the machine (like subclasses adding special time coefficients) and these
         * machines don't have to implement an extra case for this
         */
        return currentRecipe == null ? 0 : currentRecipe.getEnergyPerTick() / getSpeedUpgradeMultiplier();
    }
    
    /**
     * @return the progress
     */
    public int getProgress() {
        return progress;
    }
    
    public int getProgressScaled(int max) {
        return getProcessTime() > 0? (int) ((progress / (getProcessTime() * 1.0F)) * max): 0;
    }

    @Override
    public GuiInfo getGuiInfo() {
        return this.type.guiInfo;
    }

    /**
     * Called each tick the machine can process
     * Used to drain energy out of a machine each tick
     */
    public void process() {
        getPowerHandler().drainEnergy(getEnergyPerTick(), true);
    }

    /**
     * @return the ItemStack that is in the input slot
     */
    public ItemStack getInputStack() {
        if(getInputSlotNumber() < 0) {
            return null;
        }

        return inventoryHandler.getStackInSlot(getInputSlotNumber());
    }

    /**
     * @return the ItemStack that is in the output slot
     */
    public ItemStack getOutputStack() {
        return inventoryHandler.getStackInSlot(getOutputSlotNumber());
    }
    
    /**
     * Machines implementing a slot number 0 that is not the input slot HAVE TO change this
     * @return the number of the input slot
     */
    public int getInputSlotNumber() {
        return 0;
    }

    /**
     * Machines that don't have an output slot shouldn't override this but HAVE TO override @getOutputStack() to null
     * @return the number of the output slot
     */
    public int getOutputSlotNumber() {
        return 1;
    }
    
    /**
     * Method checks the Registry or another Recipe list if the item stack can be used to make stuff
     * @param stack the item Stack to test
     * @return whether or not the stack can be used as an input
     */
    public abstract boolean canStackBeInput(ItemStack stack);
    
    /**
     * Only used internally
     * @return The Recipe Object for the machine and the current input item
     */
    protected abstract MachineRecipeInfo getRecipeInfo();
    
    /**
     * Called when the progress equals the process time in other words when one process is finished
     * Used to remove one item from the input slot and put its result into the output slot
     */
    public abstract void processFinished();
    
    /**
     * Returns whether or not the machine COULD process the current item, meaning if there is enough space in the output slot
     * and if the item can even be processed
     * If it returns false the progress WILL reset
     */
    public boolean couldProcess() {
        if(currentRecipe == null) {
            return false;
        }

        if(!currentRecipe.hasStackInput() || !Utils.isItemStackEmpty(getInputStack())) {
            ItemStack result = currentRecipe.getResult();
            return canInsertIntoOutputSlot(result);
        }
        return false;
    }

    public boolean canInsertIntoOutputSlot(ItemStack item) {
        return Utils.canMergeItemStack(item, getOutputStack(), getInventoryStackLimit());
    }

    /**
     * Transfers the given item into the output slot
     * Only call this method if you are sure that the output slot can accept it.
     * When you call it from processFinished the output slot will be able to accept it.
     * @param item
     */
    public void transferStackIntoOutput(ItemStack item) {
        ItemStack output = getOutputStack();
        if(Utils.isItemStackEmpty(output)) {
            setInventorySlotContents(getOutputSlotNumber(), item);
        } else {
            output.stackSize += item.stackSize;
            setInventorySlotContents(getOutputSlotNumber(), output);
        }
    }

    /**
     * Returns whether or not the machine has enough resouces to keep going for the next tick
     * If it returns false the progress won't reset
     */
    public boolean hasEnoughResources() {
        return powerHandler.getEnergyStored() >= getEnergyPerTick();
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setShort("progress", (short) progress);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        progress = compound.getShort("progress");
    }
    
    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);

        data.writeShort((short) progress);
        /*
         * Sends down an isActive flag because the progress can be > 0 even if the machine isn't running (e.g. not enough energy)
         * And the client may not have all the inforamtion needed to figure out if it's running
         */
        data.writeBoolean(isActive());
    }
    
    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);
        // Update Input first, because the progress gets reset if the item has changed
        this.progress = packet.input.readShort();
        this.isActive = packet.input.readBoolean();
        getCoords().markBlockForUpdate(worldObj);
    }

    @Override
    public void handleGuiButtonClick(short buttonId) {}

    /**
     * if the machine implements other slots that can accept input then you HAVE TO override this
     */
    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        return i == getInputSlotNumber() && canStackBeInput(itemStack);

    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        List<UpgradeType> list = super.getSupportedUpgrades();
        list.add(UpgradeType.SPEED);

        if(getOutputSlotNumber() >= 0) {
            list.add(UpgradeType.ITEM_AUTO_EJECT);
        }

        return list;
    }

    @Override
    public int applyUpgrade(Upgrade upgrade) {
        int num = super.applyUpgrade(upgrade);

        if(num > 0) {
            newInput();
        }

        return num;
    }

    /**
     * @return whether or not to use IC2 recipes inside the machine
     */
    protected boolean useIC2Recipes() {
        return upgradeManager.hasUpgrade(UpgradeType.IC2) && Helper.isIC2Loaded();
    }

    @Override
    public int getComparatorOverride(int side) {
        return PowerUtils.calcRedstoneFromPowerHandler(getPowerHandler());
    }
    
    @Override
    public Map<TabSide, Integer> getOpenTabs() {
        return openTabs;
    }
    
    @Override
    public void setOpenTabs(Map<TabSide, Integer> tabs) {
        openTabs = tabs;
    }

    /* ##### IPeripheral ##### */
    /* Disabled for now */
    /*public static final String[] methods = {"isActive", "getProgress", "getEnergyPerTick", "getMachineType"};

    @Override
    public String getType() {
        return "Machine";
    }

    @Override
    public String[] getMethodNames() {
        return methods;
    }

    @Override
    public Object[] callMethod(IComputerAccess computer, ILuaContext context, int method, Object[] arguments) throws LuaException, InterruptedException{
        if(method >= methods.length) {
            return new Object[] {};
        }

        List<Object> list = new ArrayList<Object>();

        switch(method) {
            case 0:
                list.add(isActive());
                break;

            case 1:
                list.add(getProgress() * 1F / getProcessTime());
                break;

            case 2:
                list.add(getEnergyPerTick());
                break;

            case 3:
                list.add(type.toString());
                break;
        }

        return list.toArray(new Object[list.size()]);
    }

    @Override
    public void attach(IComputerAccess computer) {
    }

    @Override
    public void detach(IComputerAccess computer) {
    }

    @Override
    public boolean equals(IPeripheral other) {
        return other == this;
    }*/
}
