/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.tile.machine;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.blocks.BlockMachine;
import patrick96.hptanks.core.fluids.BucketType;
import patrick96.hptanks.core.fluids.IBucketable;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.inventory.slot.SlotInputRestricted;
import patrick96.hptanks.core.inventory.slot.SlotOutput;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.recipe.CVDFurnaceRecipeInfo;
import patrick96.hptanks.recipe.CVDFurnaceRecipeInfo.Catalyst;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.tile.IFluidTile;
import patrick96.hptanks.util.Utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TileEntityCVDFurnace extends TileEntityMachine implements IFluidTile, IBucketable {

    public static final GuiInfo guiInfo = new GuiInfo(GuiInfo.CVD_FURNACE_ID, "CVDFurnace", true);

    protected Map<Catalyst, Float> catalysts = new HashMap<CVDFurnaceRecipeInfo.Catalyst, Float>();

    /**
     * The current multiplier for the time taken
     * used if an optional catalyst is missing
     */
    private float timeMultiplier = 1F;

    public TileEntityCVDFurnace() {
        super(BlockMachine.MachineType.CVD_FURNACE);

        tankManager = new TankManager(new FluidTank(getTankCapacity()));

        // Input
        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 33, 46));

        // Output
        inventoryHandler.addSlot(new SlotOutput(this, inventoryHandler.getNextAvailableId(), 93, 47));

        // Catalysts
        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 178, 53));
        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 228, 53));

        // Wafer
        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 60, 25));
    }

    @Override
    public void updateProcess() {
        tryAddCatalysts();
        super.updateProcess();
    }

    @Override
    public void newInput() {
        super.newInput();

        onCatalystChange();
    }

    @Override
    public int getProcessTime() {
        return Math.round(super.getProcessTime() * timeMultiplier);
    }


    /**
     * Checks if the slot checked is a catalyst slot
     */
    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        if(!super.isItemValidForSlot(i, itemStack)) {

            // Wafer slot
            if(i == getWaferSlotNum() && Utils.areItemStacksEqualIgnoreStackSize(itemStack, ModItems.ItemPlain.Wafer.getStack())) {
                return true;
            }

            int[] catSlotNums = getCatalystSlotNums();
            for (int catSlotNum : catSlotNums) {
                if (catSlotNum == i) {
                    for(Catalyst cat : Catalyst.values()) {
                        if(cat.matches(itemStack)) {
                            // Ensures that the item is only allowed in the slot if the other catalyst slot(s) do NOT contain the same item
                            for(int catSlotNum2 : catSlotNums) {
                                if(catSlotNum2 != catSlotNum) {
                                    if(Utils.areItemStacksEqualIgnoreStackSize(getStackInSlot(catSlotNum2), itemStack)
                                               && !Utils.areItemStacksEqualIgnoreStackSize(getStackInSlot(catSlotNum), itemStack)) {
                                        return false;
                                    }
                                }
                            }
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        return true;
    }

    public int getWaferSlotNum() {
        return 4;
    }

    public int[] getCatalystSlotNums() {
        return new int[] { 2, 3 };
    }

    @Override
    public boolean canStackBeInput(ItemStack stack) {
        return HPTRegistry.getCvdFurnaceRecipeInfo(stack, this) != null;
    }

    @Override
    public CVDFurnaceRecipeInfo getRecipeInfo() {
        return HPTRegistry.getCvdFurnaceRecipeInfo(getInputStack(), this);
    }

    @Override
    public void processFinished() {
        CVDFurnaceRecipeInfo info = (CVDFurnaceRecipeInfo) currentRecipe;

        getTankManager().drain(ForgeDirection.UNKNOWN, info.getWaterUsed(), true);

        ItemStack result = info.getResult();

        List<CVDFurnaceRecipeInfo.CatalystConfiguration> recipeCatalysts = info.getCatalysts();
        for(CVDFurnaceRecipeInfo.CatalystConfiguration catalyst : recipeCatalysts) {
            Catalyst cat = catalyst.catalyst;
            float resultStored, used = catalyst.used;

            // IF NOT Optional or if there is enough for an optional catalyst
            if(!info.isOptional(cat) || hasCatalyst(cat, used)) {
                // So we don't get inexact values
                resultStored = Math.round((getCatalystStored(cat) - used) * 1000) / 1000F;
            }
            else {
                resultStored = getCatalystStored(cat);
            }

            if(resultStored <= 0) {
                catalysts.remove(cat);
            }
            else {
                catalysts.put(cat, resultStored);
            }
        }

        decrStackSize(getInputSlotNumber(), 1);
        decrStackSize(getWaferSlotNum(), 1);
        transferStackIntoOutput(result);

        ejectItem();
    }

    @Override
    public boolean hasEnoughResources() {
        if(super.hasEnoughResources() && getTankManager().getTank().getFluidAmount() >= ((CVDFurnaceRecipeInfo) currentRecipe).getWaterUsed() && !Utils.isItemStackEmpty(getStackInSlot(getWaferSlotNum()))) {
            List<CVDFurnaceRecipeInfo.CatalystConfiguration> recipeCatalysts = ((CVDFurnaceRecipeInfo) currentRecipe).getCatalysts();
            for(CVDFurnaceRecipeInfo.CatalystConfiguration catalyst : recipeCatalysts) {
                if(!catalyst.optional && !hasCatalyst(catalyst.catalyst, catalyst.used)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Only Called if a catalyst that is used in the current recipe changed
     * Updates the timeMultiplier
     */
    public void onCatalystChange() {
        if(currentRecipe == null) {
            return;
        }

        timeMultiplier = 1.0F;

        CVDFurnaceRecipeInfo recipe = ((CVDFurnaceRecipeInfo) currentRecipe);
        List<CVDFurnaceRecipeInfo.CatalystConfiguration> recipeCatalysts = recipe.getCatalysts();
        for(CVDFurnaceRecipeInfo.CatalystConfiguration catalyst : recipeCatalysts) {
            if(catalyst.optional && !hasCatalyst(catalyst.catalyst, catalyst.used)) {
                timeMultiplier *= recipe.getCatMissingTimeMult();
            }
        }
    }

    protected void tryAddCatalysts() {
        for(int slot : getCatalystSlotNums()) {
            ItemStack stack = getStackInSlot(slot);
            if(!Utils.isItemStackEmpty(stack)) {
                for(Catalyst cat : Catalyst.values()) {
                    if(cat.matches(stack) && addCatalyst(cat)) {
                        decrStackSize(slot, 1);
                        if(currentRecipe != null && ((CVDFurnaceRecipeInfo) currentRecipe).hasCatalyst(cat)) {
                            onCatalystChange();
                        }
                        markTileDirty();
                        break;
                    }
                }
            }
        }

    }

    /**
     * Adds one piece of catalyst to the storage if the limit isn't exceeded
     * @param catalyst the catalyst to add
     * @return if the catalyst could be added
     */
    protected boolean addCatalyst(Catalyst catalyst) {
        if(catalyst != null) {
            if(catalysts.containsKey(catalyst)) {
                float value = catalysts.get(catalyst);
                if(value <= catalyst.getLimit() - 1) {
                    catalysts.put(catalyst, value + 1F);
                    return true;
                }
            }
            else if(getCatalystTypeLimit() > catalysts.size()) {
                catalysts.put(catalyst, 1F);
                return true;
            }
        }

        return false;
    }

    /**
     * @return How many catalyst types this machine can have. Should be equal to the amount of types that can be displayed in the GUI
     */
    public static int getCatalystTypeLimit() {
        return 4;
    }

    protected boolean hasCatalyst(Catalyst catalyst) {
        return hasCatalyst(catalyst, 0);
    }

    /**
     * Checks the catalyst storage if the catalyst is present in the machine with at least the specified amount
     * @param catalyst The catalyst to check
     * @param amount (optional) the amount of that catalyst that should at least be present, used to check for resources
     * @return whether or not the catalyst is in the machine
     */
    protected boolean hasCatalyst(Catalyst catalyst, float amount) {
        return catalysts.containsKey(catalyst) && (amount <= 0 || catalysts.get(catalyst) >= amount);
    }

    public float getCatalystStored(Catalyst catalyst) {
        return hasCatalyst(catalyst)? catalysts.get(catalyst) : 0;
    }

    public int getCatalystFilledScaled(Catalyst catalyst, int max) {
        return hasCatalyst(catalyst)? Math.round(getCatalystStored(catalyst) / catalyst.getLimit() * max) : 0;
    }

    public Map<Catalyst, Float> getCatalysts() {
        return catalysts;
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);

        NBTTagList catalystList = new NBTTagList();

        for (Map.Entry<Catalyst, Float> catalyst : catalysts.entrySet()) {
            if (catalyst.getValue() > 0 && catalyst.getKey() != null) {
                NBTTagCompound cat = new NBTTagCompound();
                cat.setString("name", catalyst.getKey().name());
                cat.setFloat("amount", catalyst.getValue());
                catalystList.appendTag(cat);
            }
        }

        compound.setTag("catalysts", catalystList);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);

        // 10 is the ID of the NBTTagCompound
        NBTTagList catalystList = compound.getTagList("catalysts", Constants.NBT.TAG_COMPOUND);

        for (int i = 0; i < catalystList.tagCount(); i++) {
            NBTTagCompound cat = catalystList.getCompoundTagAt(i);
            Catalyst catalyst = Catalyst.valueOf(cat.getString("name"));
            float amount = cat.getFloat("amount");
            if(catalyst != null && amount > 0) {
                catalysts.put(catalyst, amount);
            }

        }
    }

    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);

        data.writeInt(catalysts.size());
        for (Map.Entry<Catalyst, Float> catalyst : catalysts.entrySet()) {
            ByteBufUtils.writeUTF8String(data, catalyst.getKey().name());
            data.writeFloat(catalyst.getValue());
        }
    }

    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);

        int catalystSize = packet.input.readInt();
        catalysts.clear();
        for(int i = 0; i < catalystSize; i++) {
            Catalyst cat = Catalyst.valueOf(ByteBufUtils.readUTF8String(packet.input));
            float amount = packet.input.readFloat();

            if(cat != null && amount > 0) {
                catalysts.put(cat, amount);
            }
        }

        onCatalystChange();
    }

    @Override
    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return fluid == FluidRegistry.WATER;
    }

    @Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return false;
    }

    @Override
    public BucketType getBucketType() {
        return BucketType.FILL;
    }

    @Override
    public IFluidHandler getFluidHandler() {
        return this;
    }
}
