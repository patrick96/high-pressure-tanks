/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.tile.machine;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.blocks.BlockMachine;
import patrick96.hptanks.core.fluids.BucketType;
import patrick96.hptanks.core.fluids.IBucketable;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.inventory.slot.SlotOutput;
import patrick96.hptanks.core.network.PacketHandler;
import patrick96.hptanks.core.network.packets.PacketRubberFormerSelectedStack;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.recipe.RubberFormerRecipeInfo;
import patrick96.hptanks.tile.IFluidTile;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.LogHelper;
import patrick96.hptanks.util.Utils;

import java.io.IOException;
import java.util.List;

public class TileEntityRubberFormer extends TileEntityMachine implements IFluidTile, IBucketable {

    public static GuiInfo guiInfo = new GuiInfo(GuiInfo.RUBBER_FORMER_ID, "RubberFormer", true);

    protected ItemStack selectedStack = null;

    public TileEntityRubberFormer() {
        super(BlockMachine.MachineType.RUBBER_FORMER);
        inventoryHandler.addSlot(new SlotOutput(this, inventoryHandler.getNextAvailableId(), 93, 47));

        tankManager = new TankManager(new FluidTank(getTankCapacity()));
    }

    @Override
    public boolean canStackBeInput(ItemStack stack) {
        return false;
    }

    @Override
    public int getInputSlotNumber() {
        return -1;
    }

    @Override
    public int getOutputSlotNumber() {
        return 0;
    }

    @Override
    protected MachineRecipeInfo getRecipeInfo() {
        return HPTRegistry.getRubberFormerRecipe(selectedStack, this);
    }

    public ItemStack getSelectedStack() {
        return currentRecipe != null? currentRecipe.getResult() : null;
    }

    public void setSelectedStack(ItemStack selectedStack) {
        if(!Utils.areItemStacksEqualIgnoreStackSize(selectedStack, this.selectedStack)) {
            this.selectedStack = selectedStack;
            newInput();
            PacketHandler.sendToServer(new PacketRubberFormerSelectedStack(this).getPacket(), worldObj);
        }
    }

    public void onSelectedStackChanged(PacketRubberFormerSelectedStack packet) {
        if(!worldObj.isRemote) {
            this.selectedStack = ByteBufUtils.readItemStack(packet.input);
            newInput();
            markTileDirty();
        }
    }

    public void processFinished() {
        RubberFormerRecipeInfo info = ((RubberFormerRecipeInfo) currentRecipe);

        getTankManager().drain(ForgeDirection.UNKNOWN, info.getResinUsed(), true);

        ItemStack result = info.getResult();
        if(Utils.isItemStackEmpty(result)) {
            return;
        }

        transferStackIntoOutput(result);
        ejectItem();
    }

    @Override
    public boolean hasEnoughResources() {
        return super.hasEnoughResources() && currentRecipe != null && getTankManager().getFluidAmount() >= ((RubberFormerRecipeInfo) currentRecipe).getResinUsed();
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        if(currentRecipe != null) {
            NBTTagCompound selectedStack = new NBTTagCompound();
            currentRecipe.getResult().writeToNBT(selectedStack);
            compound.setTag("selectedStack", selectedStack);
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        if(compound.hasKey("selectedStack")) {
            selectedStack = ItemStack.loadItemStackFromNBT(compound.getCompoundTag("selectedStack"));
            newInput();
        }

        super.readFromNBT(compound);
    }

    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);
        ByteBufUtils.writeItemStack(data, getSelectedStack());
    }

    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);
        ItemStack selectedStackOld = this.getSelectedStack();
        selectedStack = ByteBufUtils.readItemStack(packet.input);
        if(!Utils.areItemStacksEqualIgnoreStackSize(selectedStackOld, selectedStack)) {
            newInput();
        }
    }

    @Override
    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return fluid == ModBlocks.fluidResin;
    }

    @Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return false;
    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        List<UpgradeType> list = super.getSupportedUpgrades();
        list.add(UpgradeType.IC2);
        return list;
    }

    @Override
    public BucketType getBucketType() {
        return BucketType.FILL;
    }

    @Override
    public IFluidHandler getFluidHandler() {
        return this;
    }
}
