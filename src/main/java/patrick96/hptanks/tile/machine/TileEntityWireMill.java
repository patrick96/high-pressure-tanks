/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile.machine;

import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.blocks.BlockMachine.MachineType;
import patrick96.hptanks.core.fluids.BucketType;
import patrick96.hptanks.core.fluids.IBucketable;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.inventory.slot.SlotInputRestricted;
import patrick96.hptanks.core.inventory.slot.SlotOutput;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.recipe.WireMillRecipeInfo;
import patrick96.hptanks.tile.IFluidTile;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.Utils;

import java.util.List;

public class TileEntityWireMill extends TileEntityMachine implements IFluidTile, IBucketable {

    public static final GuiInfo guiInfo = new GuiInfo(GuiInfo.WIRE_MILL_ID, "WireMill", true);

    public TileEntityWireMill() {
        super(MachineType.WIRE_MILL);
        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 33, 46));
        inventoryHandler.addSlot(new SlotOutput(this, inventoryHandler.getNextAvailableId(), 93, 47));

        tankManager = new TankManager(new FluidTank(getTankCapacity()));
    }

    @Override
    public WireMillRecipeInfo getRecipeInfo() {
        return HPTRegistry.getwireMillResult(getInputStack(), this);
    }
        
    @Override
    public void processFinished() {
        WireMillRecipeInfo info = ((WireMillRecipeInfo) currentRecipe);

        getTankManager().drain(ForgeDirection.UNKNOWN, info.getResinUsed(), true);
        
        ItemStack result = info.getResult();
        if (Utils.isItemStackEmpty(result)) {
            return;
        }

        decrStackSize(getInputSlotNumber(), 1);
        transferStackIntoOutput(result);
        
        ejectItem();
    }

    @Override
    public boolean hasEnoughResources() {
        return super.hasEnoughResources() && getTankManager().getFluidAmount() >= ((WireMillRecipeInfo) currentRecipe).getResinUsed();
    }

    @Override
    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return fluid == ModBlocks.fluidResin;
    }
    
    @Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return false;
    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        List<UpgradeType> list = super.getSupportedUpgrades();
        list.add(UpgradeType.IC2);
        return list;
    }
    
    @Override
    public boolean canStackBeInput(ItemStack stack) {
        return HPTRegistry.getwireMillResult(stack, this) != null;
    }

    @Override
    public BucketType getBucketType() {
        return BucketType.FILL;
    }

    @Override
    public IFluidHandler getFluidHandler() {
        return this;
    }
}
