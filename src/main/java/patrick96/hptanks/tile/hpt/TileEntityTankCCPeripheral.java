/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile.hpt;

import cpw.mods.fml.common.Optional;
import dan200.computercraft.api.lua.ILuaContext;
import dan200.computercraft.api.lua.LuaException;
import dan200.computercraft.api.peripheral.IComputerAccess;
import dan200.computercraft.api.peripheral.IPeripheral;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.power.EnergySystem;
import patrick96.hptanks.core.power.HPTPowerHandler;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.List;

@Optional.Interface(modid = Reference.PLUGIN_CC_DEP, iface = "dan200.computercraft.api.peripheral.IPeripheral", striprefs = true)
public class TileEntityTankCCPeripheral extends TileEntityTankComponent implements IHPTWallComponent, IHPTFrameComponent, dan200.computercraft.api.peripheral.IPeripheral {

    // public static final String[] methods = {"getCapacity", "getAmount", "getFluidID", "getFluidName", "fluidToString", "getDimensions", "getFluidPerAirBlock", "getFluidPerLevel"};
    public static final String[] methods = {
            "isPartOfMultiBlock",
            "getCapacity",
            "getStored",
            "getFluid",
            "getStoredScaled",
            "getDimensions",
            "getFluidPerAirBlock",
            "getFluidPerLevel",
            "getFluidLevel",
            "getEnergyCapacity",
            "getEnergyStored",
            "getEnergyStoredScaled"
    };

    public TileEntityTankCCPeripheral() {
        super();
    }

    @Override
    public String getType() {
        return "HPTank";
    }

    @Override
    public String[] getMethodNames() {
        // The peripheral has to be rewrapped every time the tank is (de)constructed
        return isPartOfMultiBlock() ? methods : new String[] { "isPartOfMultiBlock" };
    }

    @Override
    public Object[] callMethod(IComputerAccess computer, ILuaContext context, int method, Object[] arguments) throws LuaException, InterruptedException {
        if(!isPartOfMultiBlock()) {
            return new Object[] { false };
        }

        if(method >= methods.length) {
            return new Object[] { null };
        }

        List<Object> list = new ArrayList<Object>();
        EnergySystem es = getEnergySystemFromArgs(arguments);

        switch(method) {
            // isPartOfMultiBlock
            case 0:
                list.add(isPartOfMultiBlock());
                break;

            // getCapacity
            case 1:
                list.add(getTankCapacity());
                break;

            // getStored
            case 2:
                list.add(getControl().getFluidAmount());
                break;

            // getFluid
            case 3:
                list.add(!getTankManager().isEmpty() ? FluidUtils.getFluidName(getTankManager().getTank().getFluid().getFluid()) : "");
                break;

            // getStoredScaled
            case 4:
                int max = 1;
                if(arguments != null && arguments.length >= 1) {
                    Object arg = arguments[0];
                    max = Utils.parseInt(arg);
                }
                int cap = getControl().getTankCapacity();
                int amount = getControl().getFluidAmount();
                float percent = cap == 0? 0 : amount * 1F / cap;
                list.add(percent * max);
                break;

            // getDimensions
            case 5:
                for(int coord : getControl().getDimensions().toArray()) {
                    list.add(coord);
                }
                break;

            // getFluidPerAirBlock
            case 6:
                list.add(getControl().getFluidPerAirBlock());
                break;

            // getFluidPerLevel
            case 7:
                list.add(getControl().getFluidPerLevel());
                break;

            // getFluidLevel
            case 8:
                list.add(getControl().getFluidLevel());
                break;

            // getEnergyCapacity
            case 9:
                list.add(getControl().getPowerHandler().getMaxEnergyStored(es));
                break;

            // getEnergyStored
            case 10:
                list.add(getControl().getPowerHandler().getEnergyStored(es));
                break;

            // getEnergyStoredScaled
            case 11:
                HPTPowerHandler pw = getControl().getPowerHandler();
                double energyMax = 1;
                if(arguments != null && arguments.length >= 1) {
                    Object arg = arguments[0];
                    energyMax = Utils.parseDouble(arg);
                }

                double energyCap = pw.getMaxEnergyStored();
                double energyAmount = pw.getEnergyStored();
                double energyPercent = energyCap == 0? 0 : energyAmount * 1F / energyCap;
                System.out.println(energyAmount + "/ " + energyCap + " * " + energyMax);
                list.add(energyPercent * energyMax);
                break;
        }

        return list.toArray(new Object[list.size()]);
    }

    private EnergySystem getEnergySystemFromArgs(Object[] args) {
        EnergySystem es = EnergySystem.HPT;
        if(args != null && args.length >= 1 && args[0] instanceof String) {
            String arg = ((String) args[0]).toUpperCase();
            if(arg.equals("HPT")) {
                es = EnergySystem.HPT;
            } else if(arg.equals("EU")) {
                es = EnergySystem.EU;
            } else if(arg.equals("RF")) {
                es = EnergySystem.RF;
            }
        }

        return es;
    }

    @Override
    public void attach(IComputerAccess computer) {}

    @Override
    public void detach(IComputerAccess computer) {}

    @Override
    public boolean equals(IPeripheral other) {
        return other == this;
    }
}