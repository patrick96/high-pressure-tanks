/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile;

import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.core.power.EnergySystem;
import patrick96.hptanks.core.power.HPTPowerHandler;
import patrick96.hptanks.core.power.IPowerTile;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeManager;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class TileEntityPowered extends TileEntityInventory implements IPowerTile, IUpgradeable {
    
    public HPTPowerHandler powerHandler;
    
    protected UpgradeManager upgradeManager;
    
    public TileEntityPowered(int maxEnergyStored) {
        powerHandler = new HPTPowerHandler(this, maxEnergyStored);
    
        this.upgradeManager = new UpgradeManager(this);
    }

    @Override
    public void updateEntity() {
        super.updateEntity();
        
        if (worldObj.isRemote) {
            return;
        }
        
        powerHandler.update();
    }
    
    public HPTPowerHandler getPowerHandler() {
        return powerHandler;
    }
    
    @Override
    public boolean hasChanged() {
        return super.hasChanged() || powerHandler.hasChanged() || upgradeManager.hasChanged();
    }
    
    @Override
    public void markClean() {
        super.markClean();
        powerHandler.markClean();
        upgradeManager.markClean();
    }
    
    @Override
    public void validate() {
        super.validate();
        powerHandler.addToIC2Net();
    }
    
    @Override
    public void invalidate() {
        super.invalidate();
        powerHandler.removeFromIC2Net();
    }
    
    public void onBlockBroken() {
        powerHandler.removeFromIC2Net();
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        upgradeManager.writeToNBT(compound);
        powerHandler.writeToNBT(compound);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        upgradeManager.readFromNBT(compound);
        powerHandler.loadFromNBT(compound);
    }
    
    @Override
    public boolean acceptsEnergyFrom(TileEntity emitter, ForgeDirection direction) {
        return true;
    }
    
    @Override
    public double getDemandedEnergy() {
        return powerHandler.getEnergyRequested(EnergySystem.EU);
    }
    
    @Override
    public double injectEnergy(ForgeDirection directionFrom, double amount, double voltage) {
        return powerHandler.addEu(amount);
    }
    
    @Override
    public int getSinkTier() {
        return powerHandler.getIC2Voltage();
    }
    
    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);
        powerHandler.writeData(data);
        upgradeManager.writePacketData(data);
    }
    
    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);
        powerHandler.readData(packet.input);
        upgradeManager.readPacketData(packet.input);
    }

    /* -- CoFH Core IEnergyHandler -- */
    
    @Override
    public int receiveEnergy(ForgeDirection from, int maxReceive, boolean simulate) {
        return (int) powerHandler.addEnergy(EnergySystem.RF, maxReceive, !simulate);
    }

    @Override
    public int extractEnergy(ForgeDirection from, int maxExtract, boolean simulate) {
        /* No Energy should be extracted */
        return 0;
    }

    @Override
    public boolean canConnectEnergy(ForgeDirection from) {
        return true;
    }

    @Override
    public int getEnergyStored(ForgeDirection from) {
        return (int) powerHandler.getEnergyStored(EnergySystem.RF);
    }

    @Override
    public int getMaxEnergyStored(ForgeDirection from) {
        return (int) powerHandler.getMaxEnergyStored(EnergySystem.RF);
    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        return new ArrayList<UpgradeType>() {{
            add(UpgradeType.ENERGY_STORAGE);
        }};
    }

    @Override
    public int applyUpgrade(Upgrade upgrade) {
        return upgradeManager.addUpgrade(upgrade);
    }

    @Override
    public UpgradeManager getUpgradeManager() {
        return upgradeManager;
    }
    
}
