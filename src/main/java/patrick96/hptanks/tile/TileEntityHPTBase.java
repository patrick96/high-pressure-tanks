/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile;

import buildcraft.api.tools.IToolWrench;
import cpw.mods.fml.common.Optional.Interface;
import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import ic2.api.tile.IWrenchable;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.network.ISyncedTile;
import patrick96.hptanks.core.network.PacketHandler;
import patrick96.hptanks.core.network.packets.PacketTileRequestUpdate;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Utils;

import java.io.IOException;

@Interface(iface = "ic2.api.tile.IWrenchable", modid = "IC2")
public abstract class TileEntityHPTBase extends TileEntity implements ISyncedTile, IWrenchable {
    
    private Coords thisCoords;
    protected boolean doUpdate = false;
    
    protected int rotationIndex = 3;
    
    // Variable to check if the tile existed at least one updateTick
    private boolean init = false;

    protected long tick = 0;

    /**
     * The TankManager is only implemented here, so that all the sub classes don't have to implement redundant methods
     */
    protected TankManager tankManager;

    public TileEntityHPTBase() {}
    
    @Override
    public void updateEntity() {
        if(!init) {
            init();
            init = true;
        }
        super.updateEntity();

        tick++;

        if(hasChanged() && !worldObj.isRemote) {
            sendUpdate();
        }
    }
    
    /**
     * Used to make sure that TEs that don't need to sync don't send packets
     * @return whether or not this tile should request an update packet from the server on load on the client side.
     */
    protected boolean shouldRequestInfoFromServer() {
        return isRotatable();
    }

    public boolean isInit() {
        return init;
    }

    protected void init() {
        if(worldObj.isRemote && shouldRequestInfoFromServer()) {
            sendRequestPacket();
        }
    }
    
    /**
     * @return {@link Coords} the coordinates of this block
     */
    public Coords getCoords() {
        if(thisCoords == null) {
            thisCoords = new Coords();
        }

        if(!thisCoords.initialized()) {
            thisCoords.init(xCoord, yCoord, zCoord);
        }
        
        return thisCoords.copy();
    }

    public World getWorld() {
        return worldObj;
    }
    
    public void onWrench(EntityPlayer player, ForgeDirection from) {
        Item current = player.getCurrentEquippedItem().getItem();
        
        if(player.isSneaking()) {
            if(canDismantle()) {
                dismantle();
                if(current instanceof IToolWrench) {
                    ((IToolWrench) current).wrenchUsed(player, xCoord, yCoord, zCoord);
                }
            }
        }
        else if(isRotatable()) {
            rotate();
            if(current instanceof IToolWrench) {
                ((IToolWrench) current).wrenchUsed(player, xCoord, yCoord, zCoord);
            }
        }
    }
    
    /* -- Dismantle Methods -- */
    public boolean canDismantle() {
        return false;
    }
    
    public void dismantle() {
        if(canDismantle()) {
            ItemStack machine = new ItemStack(getBlockType(), 1, getCoords().getBlockMetadata(worldObj));
            
            worldObj.setBlockToAir(xCoord, yCoord, zCoord);
            
            Utils.dropItemStack(worldObj, machine, xCoord, yCoord, zCoord);
        }
    }
    
    /* -- Rotation Methods -- */
    /**
     * Determines in what order the tile should be rotated
     */
    public byte[] getRotationMatrix() {
        if(onlyXAxisRotation()) {
            return new byte[] {0, 2, 1, 3};
        }
        
        if(onlyYAxisRotation()) {
            return new byte[] {2, 5, 3, 4};
        }
        
        if(onlyZAxisRotation()) {
            return new byte[] {0, 5, 1, 4};
        }
        
        return new byte[] {0, 1, 2, 5, 3, 4};
    }
    
    /**
     * If true the tile will only be rotated around the x axis.
     */
    public boolean onlyXAxisRotation() {
        return false;
    }
    
    /**
     * If true the tile will only be rotated around the y axis.
     */
    public boolean onlyYAxisRotation() {
        return false;
    }
    
    /**
     * If true the tile will only be rotated around the Z axis.
     */
    public boolean onlyZAxisRotation() {
        return false;
    }
    
    public boolean isRotatable() {
        return false;
    }
    
    /**
     * Rotates the tile
     */
    public void rotate() {
        if(!isRotatable()) {
            return;
        }
        
        rotationIndex++;
        
        if(rotationIndex >= getRotationMatrix().length) {
            rotationIndex = 0;
        }
        markTileDirty();
        getCoords().markBlockForRenderUpdate(worldObj);
    }
    
    public void setRotation(byte rotation) {
        byte[] matrix = getRotationMatrix();
        rotation = (byte) Math.max(rotation, 0);
        for(int i = 0; i < matrix.length; i++) {
            if(rotation == matrix[i]) {
                this.rotationIndex = i;
                markTileDirty();
                return;
            }
        }
    }
    
    public boolean isValidRotation(byte rotation) {
        byte[] matrix = getRotationMatrix();
        rotation = (byte) Math.max(rotation, 0);
        for(int i = 0; i < matrix.length; i++) {
            if(rotation == matrix[i]) {
                return true;
            }
        }
        return false;
    }
    
    public ForgeDirection getRotation() {
        return ForgeDirection.getOrientation(getRotationMatrix()[rotationIndex]);
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setByte("rotationIndex", (byte) rotationIndex);
        if(shouldHandleTankManager()) {
            getTankManager().writeToNBT(compound);
        }
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        rotationIndex = compound.getByte("rotationIndex");
        if(shouldHandleTankManager()) {
            getTankManager().readFromNBT(compound);
        }
    }

    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        data.writeByte(rotationIndex);
        if(shouldHandleTankManager()) {
            getTankManager().writeData(data);
        }
    }
    
    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        rotationIndex = packet.input.readByte();
        if(shouldHandleTankManager()) {
            getTankManager().readData(packet.input);
        }
    }
    
    @Override
    public boolean hasChanged() {
        return doUpdate || (shouldHandleTankManager() && getTankManager().hasChanged());
    }
    
    @Override
    public void markClean() {
        doUpdate = false;
        if(shouldHandleTankManager()) {
            getTankManager().markClean();
        }
    }
    
    @Override
    public void markTileDirty() {
        doUpdate = true;
    }
    
    @Override
    public void sendUpdate() {
        PacketHandler.sendToPlayers(getUpdatePacket(), worldObj, xCoord, yCoord, zCoord, Reference.NETWORK_UPDATE_RANGE);
        markDirty();
        markClean();
    }
    
    @Override
    public FMLProxyPacket getUpdatePacket() {
        PacketTileUpdate packet = new PacketTileUpdate(this);
        
        return packet.getPacket();
    }
    
    @Override
    public void sendRequestPacket() {
        PacketHandler.sendToServer(new PacketTileRequestUpdate(this).getPacket(), worldObj);
    }

    @Override
    public void onRequestPacket() {
        markTileDirty();
    }

    /**
     * Method should return a RS signal strength for the comparators
     * Should be overriden by all TEs that want to have a comparator output
     * @param side the side the comparator is on
     * @return the RS signal strength
     */
    public int getComparatorOverride(int side) {
        return 0;
    }

    @Override
    public boolean wrenchCanSetFacing(EntityPlayer entityPlayer, int side) {
        return isRotatable() && isValidRotation((byte) side) && !entityPlayer.isSneaking() && side != getRotation().ordinal();
    }

    @Override
    public short getFacing() {
        return (short) getRotation().ordinal();
    }

    @Override
    public void setFacing(short facing) {
        setRotation((byte) facing); 
    }

    @Override
    public boolean wrenchCanRemove(EntityPlayer entityPlayer) {
        return canDismantle();
    }

    @Override
    public float getWrenchDropRate() {
        return 1;
    }

    @Override
    public ItemStack getWrenchDrop(EntityPlayer entityPlayer) {
        return null;
    }

    /* ===== IFluidHandler Methods ===== */
    public int fill(ForgeDirection from, FluidStack resource, boolean doFill) {
        if(!FluidUtils.isFluidStackEmpty(resource) && !((IFluidHandler) this).canFill(from, resource.getFluid())) {
            return 0;
        }

        return getTankManager().fill(from, resource, doFill);
    }

    public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain) {
        if(!((IFluidHandler) this).canDrain(from, null)) {
            return null;
        }

        return getTankManager().drain(from, maxDrain, doDrain);
    }

    public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain) {
        if(!FluidUtils.isFluidStackEmpty(resource) && !((IFluidHandler) this).canDrain(from, resource.getFluid())) {
            return null;
        }

        return getTankManager().drain(from, resource, doDrain);
    }

    public FluidTankInfo[] getTankInfo(ForgeDirection from) {
        return getTankManager().getTankInfo(from);
    }

    /**
     * @return Whether or not this class should handle the tankManager, if true this class will read/write NBT & Network Packages,
     * if false it will leave it to the subclasses
     */
    public boolean shouldHandleTankManager() {
        return this instanceof IFluidHandler;
    }

    public TankManager getTankManager() {
        return tankManager;
    }

    public int getTankCapacity() {
        return Reference.BASIC_INTERNAL_TANK_STORAGE;
    }
}
