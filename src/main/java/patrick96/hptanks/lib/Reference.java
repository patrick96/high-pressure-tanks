/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.lib;

import net.minecraftforge.fluids.FluidContainerRegistry;


public class Reference {

    public static final String MOD_ID = "hpt";
    public static final String MOD_NAME = "High Pressure Tanks";
    public static final String VERSION = "@VERSION@";
    public static final String CHANNEL_NAME = MOD_ID;

    public static final String MOD_DEPENDENCIES = "before:PneumaticCraft";

    public static final String CLIENT_PROXY = "patrick96.hptanks.proxy.ClientProxy";
    public static final String SERVER_PROXY = "patrick96.hptanks.proxy.ServerProxy";

    public static final String TEXTURE_LOCATION = "hpt";
    public static final String GUI_TEXTURE_LOCATION = "textures/gui/";

    public static final int NETWORK_UPDATE_RANGE = 128;

    /* -- HPTank related Properties -- */
    public static final int TANK_MIN_SIZE = 3;
    public static final int TANK_MAX_SIZE = 15;
    public static final int TANK_DEFAULT_STORAGE_PER_AIR_BLOCK = 128 * FluidContainerRegistry.BUCKET_VOLUME;
    public static final int TANK_MAX_STORAGE_PER_AIR_BLOCK = 512 * FluidContainerRegistry.BUCKET_VOLUME;

    public static int TANK_STORAGE_PER_AIR_BLOCK = TANK_DEFAULT_STORAGE_PER_AIR_BLOCK;

    /* The default amount of fluid storage for blocks with internal tanks */
    public static final int BASIC_INTERNAL_TANK_STORAGE = 32 * FluidContainerRegistry.BUCKET_VOLUME;

    /* Amount of mB that the tank can move per tick */
    public static final int DEFAULT_TROUGHPUT_PER_TICK_LIMIT = 500;

    /* Amount of energy used to input/output 1mB of fluid */
    public static final float TANK_ENERGY_PER_MB_IN = 0.01F;
    public static final float TANK_ENERGY_PER_MB_OUT = TANK_ENERGY_PER_MB_IN * 0.5F;


    /* -- Energy Properties -- */
    public static final int BASIC_ENERGY_STORAGE_CAPACITY = 50000;

    public static final int MAX_BC_ENERGY_RECEIVED = 100;
    
    /* -- Mod Names and IDs for plugins -- */

    /* WAILA */
    public static final String PLUGIN_WAILA_NAME = "WAILA";
    public static final String PLUGIN_WAILA_DEP = "Waila";

    /* NEI */
    public static final String PLUGIN_NEI_NAME = "Not Enough Items";
    public static final String PLUGIN_NEI_DEP = "NotEnoughItems";

    /* IC2 */
    public static final String PLUGIN_IC2_NAME = "Industrial Craft 2";
    public static final String PLUGIN_IC2_DEP = "IC2";

    /* Forge Multipart */
    public static final String PLUGIN_FMP_NAME = "Forge Mutlipart";
    public static final String PLUGIN_FMP_DEP = "McMultipart";

    /* Ex-Nihilo */
    public static final String PLUGIN_EX_NIHILO_NAME = "Compat: Ex-Nihilo";
    public static final String PLUGIN_EX_NIHILO_DEP = "exnihilo";

    /* ComputerCraft */
    public static final String PLUGIN_CC_NAME = "ComputerCraft";
    public static final String PLUGIN_CC_DEP = "ComputerCraft";

    /* Buildcraft */
    public static final String PLUGIN_BC_NAME = "BuildCraft";
    public static final String PLUGIN_BC_DEP = "BuildCraft|Core";
}
