/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.lib;

import patrick96.hptanks.blocks.BlockMachine;

public class BlockInfo {
    
    public static final String TANK_CASING_UNLOCALIZED_NAME = "tankcasing";
    public static final String TANK_CASING_TEXTURE = "TankCasing";
    
    public static final String TANK_GLASS_UNLOCALIZED_NAME = "tankglass";
    public static final String TANK_GLASS_TEXTURE = "TankGlass";
    
    public static final String TANK_VALVE_UNLOCALIZED_NAME = "tankvalve";
    public static final String TANK_VALVE_TEXTURE = "TankValve";
    public static final String TANK_VALVE_SIDE_TEXTURE = "TankValveSide";
    
    public static final String TANK_UNLOCALIZED_NAME = "tank";
    public static final String TANK_TEXTURE = "Tank";
    
    public static final String TANK_CONTROL_UNLOCALIZED_NAME = "tankcontrol";
    public static final String TANK_CONTROL_TEXTURE = "TankControl";
    
    public static final String TANK_CC_PERIPHERAL_UNLOCALIZED_NAME = "tankccperipheral";
    public static final String TANK_CC_PERIPHERAL_TEXTURE = "TankCCPort";
    
    public static final String OBSIDIAN_BARS_UNLOCALIZED_NAME = "obsidianbars";
    public static final String OBSIDIAN_BARS_TEXTURE = "ObsidianBars";
    
    public static final String GLASS_BULLETPROOF_UNLOCALIZED_NAME = "glassbulletproof";
    public static final String GLASS_BULLETPROOF_TEXTURE = "TankGlass";
    
    public static final String ORE_UNLOCALIZED_NAME = "ore";
    public static final String[] ORE_UNLOCALIZED_NAME_SUFFIX = {"bauxite", "copper", "graphite"};
    public static final String ORE_TEXTURE = "Ore";
    public static final String[] ORE_TEXTURE_SUFFIX = {"Bauxite", "Copper", "Graphite"};
    
    public static final String FLUID_RESIN_UNLOCALIZED_NAME = "resin";
    public static final String FLUID_RESIN_TEXTURE = "Resin";
    
    public static final String METAL_BLOCK_UNLOCALIZED_NAME = "blockMetal";
    public static final String[] METAL_BLOCK_UNLOCALIZED_NAME_SUFFIX = {"aluminium", "copper"};
    public static final String METAL_BLOCK_TEXTURE = "Block";
    public static final String[] METAL_BLOCK_TEXTURE_SUFFIX = {"Aluminium", "Copper"};

    public static final String COMPRESSED_BLOCK_UNLOCALIZED_NAME = "blockCompressed";
    public static final String[] COMPRESSED_BLOCK_UNLOCALIZED_NAME_SUFFIX = {"graphite"};
    public static final String COMPRESSED_BLOCK_TEXTURE = "Block";
    public static final String[] COMPRESSED_BLOCK_TEXTURE_SUFFIX = {"Graphite"};

    public static final String MACHINE_UNLOCALIZED_NAME = "machine";
    public static BlockMachine.MachineType[] types = BlockMachine.MachineType.values();
    public static final String[] MACHINE_UNLOCALIZED_NAME_SUFFIX = new String[types.length + 1];
    public static final String MACHINE_TEXTURE = "Machine";
    public static final String[] MACHINE_TEXTURE_SUFFIX = new String[types.length + 1];

    static {
        MACHINE_UNLOCALIZED_NAME_SUFFIX[0] = "machineblock";
        MACHINE_TEXTURE_SUFFIX[0] = "";

        int i = 1;
        for(BlockMachine.MachineType type : types) {
            MACHINE_UNLOCALIZED_NAME_SUFFIX[i] = type.guiInfo.getTextureName().toLowerCase();
            MACHINE_TEXTURE_SUFFIX[i] = type.guiInfo.getTextureName();
            i++;
        }
    }


    public static final String WIRE_MILL_UNLOCALIZED_NAME = "WireMill";
    public static final String RESIN_EXTRACTOR_UNLOCALIZED_NAME = "ResinExtractor";
    public static final String CVD_FURNACE_UNLOCALIZED_NAME = "CVDFurnace";
    public static final String HP_CUTTER_UNLOCALIZED_NAME = "HighPrecisionCutter";
    public static final String RUBBER_FORMER_UNLOCALIZED_NAME = "RubberFormer";

    public static final String FLUID_PUMP_UNLOCALIZED_NAME = "fluidpump";
    public static final String FLUID_PUMP_TEXTURE = "fluidpump";

    public static final String BLOCK_RESIN_UNLOCALIZED_NAME = "blockresin";
    public static final String BLOCK_RESIN_TEXTURE = "BlockResin";
}
