/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.lib;

import patrick96.hptanks.upgrades.UpgradeType;

public class ItemInfo {
    
    public static final String WRENCH_UNLOCALIZED_NAME = "wrench";
    public static final String WRENCH_TEXTURE = "Wrench";
    
    public static final String CIRCUIT_BOARD_UNLOCALIZED_NAME = "circuitboard";
    public static final String[] CIRCUIT_BOARD_UNLOCALIZED_NAME_SUFFIX = {"simple"};
    public static final String CIRCUIT_BOARD_TEXTURE = "CircuitBoard";
    public static final String[] CIRCUIT_BOARD_TEXTURE_SUFFIX = {"Simple"};
    
    public static final String INGOT_UNLOCALIZED_NAME = "ingot";
    public static final String[] INGOT_UNLOCALIZED_NAME_SUFFIX = {"aluminium", "copper"};
    public static final String INGOT_TEXTURE = "Ingot";
    public static final String[] INGOT_TEXTURE_SUFFIX = {"Aluminium", "Copper"};
    
    public static final String BUCKET_RESIN_UNLOCALIZED_NAME = "bucketResin";
    public static final String BUCKET_RESIN_TEXTURE = "Resin";
    
    public static final String WIRE_UNLOCALIZED_NAME = "wire";
    public static final String[] WIRE_UNLOCALIZED_NAME_SUFFIX = {"copper"};
    public static final String WIRE_TEXTURE = "Wire";
    public static final String[] WIRE_TEXTURE_SUFFIX = {"Copper"};
    
    public static final String ENERGY_CORE_UNLOCALIZED_NAME = "energycore";
    public static final String ENERGY_CORE_TEXTURE = "EnergyCore";
    
    public static final String UPGRADES_UNLOCALIZED_NAME = "upgrade";
    public static final String[] UPGRADES_UNLOCALIZED_NAME_SUFFIX = new String[UpgradeType.UPGRADES.length];
    public static final String UPGRADES_TEXTURE = "upgrades/";
    public static final String[] UPGRADES_TEXTURE_SUFFIX = new String[UpgradeType.UPGRADES.length];

    public static final String DUST_UNLOCALIZED_NAME = "dust";
    public static final String[] DUST_UNLOCALIZED_NAME_SUFFIX = {"bauxite"};
    public static final String DUST_TEXTURE = "Dust";
    public static final String[] DUST_TEXTURE_SUFFIX = {"Bauxite"};

    public static final String DEV_TOOL_UNLOCALIZED_NAME = "devtool";
    public static final String DEV_TOOL_TEXTURE = "DevTool";
    
}
