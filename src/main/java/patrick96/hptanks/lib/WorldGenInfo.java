/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.lib;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public class WorldGenInfo {
    
    /**
     * These Values are only the default ones, the actual ones can be set in the config file
     */
    private static final int bauxiteMinY = 32;
    private static final int bauxiteMaxY = 68;
    private static final int bauxiteMaxVeinSize = 6;
    private static final int bauxiteVeinsPerChunk = 6;
    
    private static final int copperMinY = 32;
    private static final int copperMaxY = 60;
    private static final int copperMaxVeinSize = 6;
    private static final int copperVeinsPerChunk = 8;
    
    private static final int graphiteMinY = 16;
    private static final int graphiteMaxY = 60;
    private static final int graphiteMaxVeinSize = 4;
    private static final int graphiteVeinsPerChunk = 4;
    
    public static Specs[] specs = {
        new Specs("bauxite", 0, bauxiteMinY, bauxiteMaxY, bauxiteMaxVeinSize, bauxiteVeinsPerChunk),
        new Specs("copper", 1, copperMinY, copperMaxY, copperMaxVeinSize, copperVeinsPerChunk),
        new Specs("graphite", 2, graphiteMinY, graphiteMaxY, graphiteMaxVeinSize, graphiteVeinsPerChunk, Blocks.coal_ore)
    };

    public static Specs[] defaultSpecs = new Specs[specs.length];

    static {
        for(int i = 0; i < specs.length; i++) {
            defaultSpecs[i] = specs[i].copy();
        }
    }
    
    public static class Specs {
        
        public String name;
        
        public int meta;
        
        public int minY, maxY, maxVeinSize, veinsPerChunk;
        
        private boolean doGenerate = true;

        public Block overwrite;

        public Specs(String name, int meta, int minY, int maxY, int maxVeinSize, int veinsPerChunk) {
            this(name, meta, minY, maxY, maxVeinSize, veinsPerChunk, Blocks.stone);
        }

        public Specs(String name, int meta, int minY, int maxY, int maxVeinSize, int veinsPerChunk, Block overwrite) {
            this.name = name;
            
            if(meta >= 0) {
                this.meta = meta;
            }
            else {
                this.meta = 0;
            }

            this.overwrite = overwrite;
            setSpecs(minY, maxY, maxVeinSize, veinsPerChunk);
        }
        
        public void setGenerate(boolean generate) {
            doGenerate = generate;
        }
        
        public boolean doGenerate() {
            return doGenerate;
        }
        
        public void setSpecs(int minY, int maxY, int maxVeinSize, int veinsPerChunk) {
            if(maxY - minY > 0) {
                this.minY = minY;
                this.maxY = maxY;
            }
            
            if(maxVeinSize > 0) {
                this.maxVeinSize = maxVeinSize;
            }
            
            if(veinsPerChunk > 0) {
                this.veinsPerChunk = veinsPerChunk;
            }
        }

        public Specs copy() {
            return new Specs(name, meta, minY, maxY, maxVeinSize, veinsPerChunk, overwrite);
        }

    }
}
