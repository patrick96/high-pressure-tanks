/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.config;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import org.apache.logging.log4j.Level;
import patrick96.hptanks.core.power.EnergySystem;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.lib.WorldGenInfo;
import patrick96.hptanks.util.LogHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ConfigHandler {

    public static Property fluidPerAirBlock, witherDropShard;
    public static Property CTMEnabled, CTMTankCasing, CTMTankGlass, CTMBPGlass;
    public static Property dumpItems, dumpBlocks, dumpConnectedTextures, dumpRecipes, dumpWorldGen, dumpCatalysts, dumpCutters;

    public static List<EnergySystem> displayedEnergySystems = new ArrayList<EnergySystem>();
    
    public static void init(File file) {
        Configuration config = new Configuration(file, true);
        
        config.load();

        loadGeneral(config);
        loadWorldGen(config);
        loadConnectedTextures(config);
        loadDev(config);

        config.save();

        if(!CTMEnabled.getBoolean()) {
            // Setting all the CTM Configs to false (after save!!) so that we don't have to check against CTMEnabled every time
            CTMTankGlass.set(false);
            CTMBPGlass.set(false);
            CTMTankCasing.set(false);
        }
    }

    private static void loadGeneral(Configuration config) {
        fluidPerAirBlock = config.get(Configuration.CATEGORY_GENERAL, "fluidPerAirBlock", Reference.TANK_DEFAULT_STORAGE_PER_AIR_BLOCK);
        fluidPerAirBlock.comment = "How many millibuckets a multi block tank can store per air block it contains.\n"
                + "Note: 1 Bucket = 1000 millibuckets\n"
                + "Warning: When decreasing the storage amount, existing tanks lose contents if the new capacity is smaller than what was already in them\n"
                + "Default: " + Reference.TANK_DEFAULT_STORAGE_PER_AIR_BLOCK + "\n"
                + "Max: " + Reference.TANK_MAX_STORAGE_PER_AIR_BLOCK;

        if(fluidPerAirBlock.getInt() > 0 && fluidPerAirBlock.getInt() <= Reference.TANK_MAX_STORAGE_PER_AIR_BLOCK) {
            Reference.TANK_STORAGE_PER_AIR_BLOCK = fluidPerAirBlock.getInt();
        } else {
            LogHelper.warn("Config option fluidPerAirBlock has an invaild argument, using default: " + Reference.TANK_DEFAULT_STORAGE_PER_AIR_BLOCK + "mB");
        }

        Property logLevel = config.get(Configuration.CATEGORY_GENERAL, "loggingLevel", Level.INFO.intLevel());
        logLevel.comment = "The Logging level up to which the console should show messages.\n"
                + "0 - Off, 1 - Fatal, 2 - Error, 3 - Warning, 4 - Info (default), 5 - Debugging, 6 - Tracing\n"
                + "The higher the number the more messages you will get, from 5 upwards only for debugging purposes";

        LogHelper.setLogLevel(LogHelper.getLevelByInt(logLevel.getInt()));

        Property energySystems = config.get(Configuration.CATEGORY_GENERAL, "displayedEnergySystems", "RF,EU");
        energySystems.comment = "List of the different energy Systems that should be displayed in tooltips and other energy displays."
                + "\nNote: This does not affect the types of energy a machine can accept."
                + "\nPossible Values: RF (default), EU (default)"
                + "\nMultiple values should be seperated by commas ','";

        String[] systemList = energySystems.getString().split(",");
        displayedEnergySystems.clear();

        for(String energySystemString : systemList) {
            if(energySystemString == null) {
                continue;
            }
            energySystemString = energySystemString.trim().toUpperCase();

            if(energySystemString.equals("")) {
                continue;
            }

            EnergySystem energySystem = EnergySystem.valueOf(energySystemString);

            if(energySystem != null && energySystem != EnergySystem.HPT && !displayedEnergySystems.contains(energySystem)) {
                displayedEnergySystems.add(energySystem);
            }
        }

        if(displayedEnergySystems.isEmpty()) {
            LogHelper.warn("No valid energy System to display. Reverting to RF and EU.");
            displayedEnergySystems.add(EnergySystem.RF);
            displayedEnergySystems.add(EnergySystem.EU);
        }

        LogHelper.info("Displayed Energy Systems set to: " + displayedEnergySystems.toString());

        witherDropShard = config.get(Configuration.CATEGORY_GENERAL, "witherDropsShards", true);
        witherDropShard.comment = "Whether or not the Wither should drop NetherStarShards upon death. (default: true)";
    }

    private static void loadConnectedTextures(Configuration config) {
        String category = "ConnectedTextures";

        CTMBPGlass = config.get(category, "ConnectedTexturesBulletProofGlass", true);
        CTMBPGlass.comment = "Connected Textures for the Bullet-proof Glass";

        CTMTankGlass = config.get(category, "ConnectedTexturesTankGlass", true);
        CTMTankGlass.comment = "Connected Textures for the Tank Glass";

        CTMTankCasing = config.get(category, "ConnectedTexturesTankCasing", true);
        CTMTankCasing.comment = "Connected Textures for the Tank Casing";

        CTMEnabled = config.get(category, "ConnectedTexturesEnabled", true);
        CTMEnabled.comment = "Whether or not connected textures should be enabled\n"
                + "If set to false this will override all other connected texture configuration and no block will have connected textures";
    }
    
    private static void loadWorldGen(Configuration config) {
        for(WorldGenInfo.Specs specs : WorldGenInfo.specs) {
            String oreCategory = "WorldGen" + Configuration.CATEGORY_SPLITTER + specs.name;
            
            Property minY = config.get(oreCategory, specs.name + "MinY", specs.minY);
            minY.comment = "The minimum Y-level the ore should spawn at (default: " + specs.minY + ")";
            specs.minY = minY.getInt();
            
            Property maxY = config.get(oreCategory, specs.name + "MaxY", specs.maxY);
            maxY.comment = "The maximum Y-level the ore should spawn at (default: " + specs.maxY + ")";
            specs.maxY = maxY.getInt();
            
            Property maxVeinSize = config.get(oreCategory, specs.name + "MaxVeinSize", specs.maxVeinSize);
            maxVeinSize.comment = "The maximum size of the ore vein (default: " + specs.maxVeinSize + ")";
            specs.maxVeinSize = maxVeinSize.getInt();
            
            Property veinsPerChunk = config.get(oreCategory, specs.name + "VeinsPerChunk", specs.veinsPerChunk);
            veinsPerChunk.comment = "The amount of veins generated per chunk (default: " + specs.veinsPerChunk + ")";
            specs.veinsPerChunk = veinsPerChunk.getInt();
            
            Property doGenerate = config.get(oreCategory, specs.name + "Generation", true);
            doGenerate.comment = "If set to true the ore will generate otherwise it won't. (default: true)";
            
            specs.setGenerate(doGenerate.getBoolean(true));
            specs.setSpecs(minY.getInt(), maxY.getInt(), maxVeinSize.getInt(), veinsPerChunk.getInt());
        }
    }

    private static void loadDev(Configuration config) {
        String category = "dev";

        config.addCustomCategoryComment(category, "Dumps will not run on dedicated servers.");

        dumpBlocks = config.get(category, "dumpBlocks", false);
        dumpBlocks.comment = "Dumps a list of all this mod's blocks into a file inside the dumps folder (default: false)";

        dumpItems = config.get(category, "dumpItems", false);
        dumpItems.comment = "Dumps a list of all this mod's items into a file inside the dumps folder (default: false)";

        dumpConnectedTextures = config.get(category, "dumpConnectedTextures", false);
        dumpConnectedTextures.comment = "Dumps the generated texture files into the dumps folder, this only works for the blocks that are enabled in the 'ConnectedTextures' section of this config (default: false)";

        dumpRecipes = config.get(category, "dumpRecipes", false);
        dumpRecipes.comment = "Dumps all the recipes using/crafting at least one item of this mod or using a machine of this mod (default: false)";

        dumpWorldGen = config.get(category, "dumpWorldGen", false);
        dumpWorldGen.comment = "Dumps worldgen info for ores in this mods (default: false)";

        dumpCatalysts = config.get(category, "dumpCatalysts", false);
        dumpCatalysts.comment = "Dumps CVD Furnace Catalyst info (default: false)";

        dumpCutters = config.get(category, "dumpCutters", false);
        dumpCutters.comment = "Dumps Cutter info (default: false)";
    }
    
}
