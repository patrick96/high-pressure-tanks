/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

import java.util.List;

public class ItemHPT extends Item {
    protected boolean hasEffect = false;

    public ItemHPT(String unlocalizedName) {
        super();
        setCreativeTab(HPTanks.tabsHPT);
        setUnlocalizedName("hpt." + unlocalizedName.toLowerCase());
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister register) {
        itemIcon = register.registerIcon(Utils.getTexture(getIconString()));
    }
    
    public ItemStack getStack() {
        return getStack(1);
    }
    
    public ItemStack getStack(int size) {
        return getStack(size, 0);
    }
    
    public ItemStack getStack(int size, int meta) {
        return new ItemStack(this, size, meta);
    }

    public void setHasEffect(boolean hasEffect) {
        this.hasEffect = hasEffect;
    }

    @Override
    public boolean hasEffect(ItemStack p_77636_1_, int pass) {
        return hasEffect || super.hasEffect(p_77636_1_, pass);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean p_77624_4_) {
        super.addInformation(stack, player, list, p_77624_4_);

        if(StringUtils.hasLocalization(this.getUnlocalizedName() + ".descr")) {
            list.add(StringUtils.localize(this.getUnlocalizedName(stack) + ".descr"));
        }
    }
}
