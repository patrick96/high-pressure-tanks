/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import patrick96.hptanks.lib.ItemInfo;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeManager;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class ItemUpgrade extends ItemMulti {

    public ItemUpgrade() {
        super(ItemInfo.UPGRADES_UNLOCALIZED_NAME);
        setTextureName(ItemInfo.UPGRADES_TEXTURE);
        setTextureSuffixes(ItemInfo.UPGRADES_TEXTURE_SUFFIX);
        setSuffixes(ItemInfo.UPGRADES_UNLOCALIZED_NAME_SUFFIX);
        setMaxStackSize(16);
    }

    public static UpgradeType getUpgradeType(int meta) {
        return UpgradeType.UPGRADES.length > meta ? UpgradeType.UPGRADES[meta] : null;
    }

    public static Upgrade getUpgrade(ItemStack stack) {
        if(!Utils.isItemStackEmpty(stack) && stack.getItem() instanceof ItemUpgrade) {
            Upgrade upgrade = Upgrade.getUpgrade(ItemUpgrade.getUpgradeType(stack.getItemDamage()));
            if(upgrade != null && stack.hasTagCompound()) {
                upgrade.readInfoFromNBT(stack.getTagCompound());
            }

            return upgrade;
        }

        return null;
    }

    @Override
    public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if(tile instanceof IUpgradeable && stack.getItem() instanceof ItemUpgrade) {
            player.swingItem();

            Upgrade upgrade = getUpgrade(stack);

            IUpgradeable tileUp = (IUpgradeable) tile;

            if(upgrade != null && UpgradeManager.canAcceptUpgrade(upgrade.getType(), tileUp) && tileUp.applyUpgrade(upgrade) > 0) {
                if(!player.capabilities.isCreativeMode) {
                    Utils.consumeItem(stack, false);
                }
                return !world.isRemote;
            }
        }

        return false;
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {
        if(!Utils.isItemStackEmpty(stack)) {
            if(Utils.isShiftKeyDown()) {
                Upgrade upgrade = getUpgrade(stack);
                upgrade.setAmount(stack.stackSize);
                ArrayList<String> toolTip = upgrade == null ? new ArrayList<String>() : upgrade.getTooltip();
                list.addAll(toolTip);
            } else {
                list.add(StringUtils.holdShiftForInfo);
            }
        }
    }

}
