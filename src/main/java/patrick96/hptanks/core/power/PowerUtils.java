/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.power;

import net.minecraft.util.EnumChatFormatting;
import patrick96.hptanks.config.ConfigHandler;
import patrick96.hptanks.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PowerUtils {

    public static List<String> powerHandlerToList(HPTPowerHandler handler) {
        return powerHandlerToList(handler, true);
    }

    /**
     * Creates a list containing a line of info for the energy handler per entry
     * @param handler The Power Handler to get the info from
     * @param special whether or not special characters should be used
     * @return the list
     */
    public static List<String> powerHandlerToList(HPTPowerHandler handler, boolean special) {
        ArrayList<String> energyList = new ArrayList<String>();
        for(EnergySystem system : ConfigHandler.displayedEnergySystems) {
            if(handler.accepts(system)) {
                energyList.add(StringUtils.prettyPrintNumber(Math.round(handler.getEnergyStored(system)))
                                       + " /" + StringUtils.prettyPrintNumber((int) Math.ceil(handler.getMaxEnergyStored(system)))
                                       + " " + system.name());
            }
        }

        return energyList;
    }

    public static List<String> getEnergyPerTickList(double HPTEnergyPerTick) {
        List<String> list = new ArrayList<String>();
        for(EnergySystem system : ConfigHandler.displayedEnergySystems) {
            list.add(StringUtils.prettyPrintNumber(system.convertBack(HPTEnergyPerTick), 1, true) + " " + system.name() + "/t");
        }
        return list;
    }

    public static int calcRedstoneFromPowerHandler(HPTPowerHandler handler) {
        return handler.getEnergyStored() > 0? (int) Math.ceil(15F * handler.getEnergyStoredRatio()): 0;
    }
    
}
