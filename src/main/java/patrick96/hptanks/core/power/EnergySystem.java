/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.core.power;

public enum EnergySystem {

    HPT(1),
    EU(2),
    RF(0.5F);

    /**
     * How much HPTanks energy one Unit of energy from this energy systems converts to
     */
    public final double energyConversion;

    EnergySystem(double energyConversion) {
        this.energyConversion = energyConversion;
    }

    /**
     * Converts @systemEnergy of that particular system into HPTanks energy
     * @param systemEnergy amount of energy
     * @return amount of HPTanks energy
     */
    public double convert(double systemEnergy) {
        return systemEnergy * energyConversion;
    }

    /**
     * Converts @HPTEnergy of HPTanks energy to energy of this energy System
     * @param HPTEnergy amount of HPTanks energy
     * @return amount of this system's energy
     */
    public double convertBack(double HPTEnergy) {
        return HPTEnergy / energyConversion;
    }

}
