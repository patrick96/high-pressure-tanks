/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.power;

import cofh.api.energy.IEnergyHandler;
import cpw.mods.fml.common.Optional.Method;
import ic2.api.energy.event.EnergyTileLoadEvent;
import ic2.api.energy.event.EnergyTileUnloadEvent;
import ic2.api.energy.tile.IEnergyTile;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.MinecraftForge;
import patrick96.hptanks.core.network.IChangeable;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeManager;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.Helper;

import java.io.IOException;

import static patrick96.hptanks.core.power.EnergySystem.*;

public class HPTPowerHandler implements IChangeable {
    
    private boolean acceptsEU = false;
    private boolean acceptsRF = false;
    
    private TileEntity tile;
    
    protected boolean hasChanged = false;
    private double energyLastTick = 0;
    
    /* -- EU Vars -- */
    private boolean isAddedToIC2Net = false;
    private boolean addToIC2NetOnNextTick = false;
    
    private int euMaxSafeInput = Integer.MAX_VALUE;

    /* -- Storage Vars -- */
    // This var will change depending on upgrades
    private double maxEnergyStored;
    
    public final double defaultMaxEnergyStored;
    private double energyStored = 0;
    
    private double energyRequested;
    
    public HPTPowerHandler(TileEntity tile, double maxEnergyStored) {
        
        this.tile = tile;
        this.maxEnergyStored = maxEnergyStored;
        this.defaultMaxEnergyStored = maxEnergyStored;
        
        if (Helper.isIC2Loaded() && tile instanceof IEnergyTile) {
            acceptsEU = true;
        }
        
        if(tile instanceof IEnergyHandler) {
            acceptsRF = true;
        }
        
        validateEnergy();
    }
    
    public void updatePowerCapacity() {
        if(tile instanceof IUpgradeable) {
            if(((IUpgradeable) tile).getUpgradeManager() != null) {
                UpgradeManager upgradeManager = ((IUpgradeable) tile).getUpgradeManager();
                int amount = upgradeManager.getUpgradeAmount(UpgradeType.ENERGY_STORAGE);
                if(amount > 0) {
                    double originalMax = getMaxEnergyStored();
                    double newMax = defaultMaxEnergyStored * (amount + 1);
                    if(newMax != originalMax) {
                        setMaxEnergyStored(newMax);
                        markTileDirty();
                    }
                }
            }
        }
    }

    public boolean accepts(EnergySystem energySystem) {
        if(energySystem != null) {
            switch(energySystem) {
                case HPT:
                    return true;
                case EU:
                    return acceptsEU;
                case RF:
                    return acceptsRF;
            }
        }

        return false;
    }
    
    public void setEnergy(double energyStored) {
        this.energyStored = energyStored;
        validateEnergy();
    }
    
    /**
     * @param maxEnergyStored the maxEnergyStored to set
     */
    public void setMaxEnergyStored(double maxEnergyStored) {
        this.maxEnergyStored = maxEnergyStored;
    }
    
    public void configEU(int maxSafeInput) {
        euMaxSafeInput = maxSafeInput;
    }
    
    /**
     * This should be called every tick
     */
    public void update() {
        if (acceptsEU && addToIC2NetOnNextTick) {
            addToIC2NetDo();
        }
        
        validateEnergy();
        
        if (Math.abs(getEnergyStored() - energyLastTick) > 1) {
            markTileDirty();
        }
        
        energyLastTick = getEnergyStored();
    }
    
    @Method(modid = "IC2")
    public void addToIC2NetDo() {
        if (acceptsEU) {
            MinecraftForge.EVENT_BUS.post(new EnergyTileLoadEvent((IEnergyTile) tile));
            addToIC2NetOnNextTick = false;
            isAddedToIC2Net = true;
            markTileDirty();
        }
    }
    
    @Override
    public void markTileDirty() {
        hasChanged = true;
    }
    
    @Override
    public void markClean() {
        hasChanged = false;
    }
    
    @Override
    public boolean hasChanged() {
        return hasChanged;
    }
    
    public void writeToNBT(NBTTagCompound compound) {
        NBTTagCompound handlerCompound = new NBTTagCompound();
        
        validateEnergy();
        
        handlerCompound.setDouble("energyStored", energyStored);
        compound.setTag("hptPowerHandler", handlerCompound);
    }
    
    public void loadFromNBT(NBTTagCompound compound) {
        NBTTagCompound handlerCompound = compound.getCompoundTag("hptPowerHandler");
        
        energyStored = handlerCompound.getFloat("energyStored");
        updatePowerCapacity();
    }
    
    public void writeData(ByteBuf data) throws IOException {
        data.writeDouble(energyStored);
    }
    
    public void readData(ByteBuf data) throws IOException {
        double storedLastTick = energyStored;
        double newEnergyStored = data.readDouble();
        
        // Only update the Client-Side energyStored, if the storage was full or empty the last time it synced the change in energy is greater than or equal 64
        if((storedLastTick == 0 || storedLastTick == maxEnergyStored) && Math.abs(storedLastTick - newEnergyStored) < 64) {
            return;
        }
        
        energyStored = newEnergyStored;
        updatePowerCapacity();
    }
    
    public void validateEnergy() {
        maxEnergyStored = Math.max(maxEnergyStored, 0);
        energyStored = Math.max(Math.min(maxEnergyStored, energyStored), 0);
        
        energyRequested = maxEnergyStored - energyStored;
    }

    public double getEnergyRequested() {
        return getEnergyRequested(HPT);
    }

    public double getEnergyRequested(EnergySystem system) {
        return system.convertBack(energyRequested);
    }

    public double getEnergyStored() {
        return getEnergyStored(HPT);
    }

    public double getEnergyStored(EnergySystem system) {
        return system.convertBack(energyStored);
    }

    public double getMaxEnergyStored() {
        return getMaxEnergyStored(HPT);
    }

    public double getMaxEnergyStored(EnergySystem system) {
        return system.convertBack(maxEnergyStored);
    }

    /**
     * Calculates the percentage of power stored
     * @return the percentage of power stored as float between 0 and 1
     */
    public double getEnergyStoredRatio() {
        return getMaxEnergyStored() != 0? getEnergyStored()/ getMaxEnergyStored() : 0;
    }

    /**
     * 
     * @param amount
     *            energy amount to be added
     * @param doAdd
 * @return amount of energy actually added
     */
    public double addEnergy(double amount, boolean doAdd) {
        return addEnergy(HPT, amount, doAdd);
    }

    /**
     * 
     * @param amount
     *            energy amount to be added
     * @return amount of energy actually added
     */
    public double addEnergy(EnergySystem system, double amount, boolean doAdd) {
        if (amount <= 0) {
            return 0;
        }

        amount = system.convert(amount);

        double energyAdded = Math.min(amount, getEnergyRequested());
        
        if(doAdd) {
            energyStored += energyAdded;
            validateEnergy();
        }
        
        return system.convertBack(energyAdded);
    }

    /**
     * 
     * @param amount
     *            Amount of Energy to be drained
     * @param doDrain
     *            if false drain is only simulated
     * @return amount that is actually drained
     */
    public double drainEnergy(double amount, boolean doDrain) {
        return drainEnergy(HPT, amount, doDrain);
    }

    /**
     * 
     * @param amount
     *            Amount of Energy to be drained
     * @param doDrain
     *            if false drain is only simulated
     * @return amount that is actually drained
     */
    public double drainEnergy(EnergySystem system, double amount, boolean doDrain) {
        if (amount <= 0) {
            return 0;
        }

        amount = system.convert(amount);

        double amountRemaining = Math.max(getEnergyStored() - amount, 0);
        double drainedAmount = getEnergyStored() - amountRemaining;
        
        if (doDrain) {
            energyStored = amountRemaining;
            validateEnergy();
        }
        
        return system.convertBack(drainedAmount);
    }
    
    /* -- EU Methods -- */
    public int getIC2Voltage() {
        return Integer.MAX_VALUE;
    }
    
    /**
     * 
     * @param amount
     *            amount to added
     * @return energy not consumed (leftover), Warning: this behaviour is different from the default behaviour, by default the added amount is returned
     */
    public double addEu(double amount) {
        return amount - addEnergy(EU, amount, true);
    }
    
    public void addToIC2Net() {
        if (!isAddedToIC2Net) {
            addToIC2NetOnNextTick = true;
        }
    }
    
    public void removeFromIC2Net() {
        if(acceptsEU) {
            removeFromIC2NetDo();
        }
    }
    
    @Method(modid = "IC2")
    public void removeFromIC2NetDo() {
        if (isAddedToIC2Net && acceptsEU) {
            if (!tile.getWorldObj().isRemote) {
                MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent((IEnergyTile) tile));
                isAddedToIC2Net = false;
            }
        }
    }

    public void reset() {
        energyStored = 0;
    }
}
