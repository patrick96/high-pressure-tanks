/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.inventory.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.core.fluids.BucketType;
import patrick96.hptanks.core.fluids.FluidUtils;

public class SlotFluidContainerInput extends SlotHPT {
    
    public final BucketType type;
    
    public SlotFluidContainerInput(IInventory inventory, int id, int x, int y) {
        this(inventory, id, x, y, BucketType.BOTH);
    }
    
    public SlotFluidContainerInput(IInventory inventory, int id, int x, int y, BucketType type) {
        super(inventory, id, x, y);
        this.type = type;
    }
    
    @Override
    public boolean isItemValid(ItemStack itemStack) {
        if(!FluidUtils.isFluidContainer(itemStack)) {
            return false;
        }
        
        switch (type) {
            case BOTH:
                return true;
            case EMPTY:
                return !FluidUtils.isFilledFluidContainer(itemStack);
            case FILL:
                return !FluidUtils.isEmptyFluidContainer(itemStack);
            default:
                return false;
        }
    }
}
