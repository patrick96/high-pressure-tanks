/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.inventory;

import com.google.common.primitives.Ints;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import patrick96.hptanks.core.inventory.slot.SlotHPT;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryHandler {
    
    private Map<Integer, AdvancedSlot> slots = new HashMap<Integer, AdvancedSlot>();
    
    /**
     * Map handling which slot can be accessed from which side
     * If there is no entry for a slot then it can be accessed from all side (if the list is empty then it can't be accessed at all from outside)
     * Key: Slot Index, Value: List of Side Numbers that slot can be accessed from
     */
    private Map<Integer, List<Integer>> slotsAccessibleBySide = new HashMap<Integer, List<Integer>>();
    
    private IHPTInventory inventory;
    private TileEntityHPTBase tile;
    private String invName = null;
    
    
    public InventoryHandler(IHPTInventory inventory) {
        this.inventory = inventory;
        
        if(inventory instanceof TileEntityHPTBase) {
            this.tile = (TileEntityHPTBase) inventory;
        }
    }
    
    public Map<Integer, AdvancedSlot> getSlots() {
        return slots;
    }
    
    public int getNextAvailableId() {
        return slots.size();
    }
    
    public int[] getAccessibleSlotsFromSide(int side) {
        List<Integer> slotsAccessible = new ArrayList<Integer>();
        
        for(AdvancedSlot slot : getSlots().values()) {
            int index = slot.getSlotIndex();
            if(isSlotAccessibleOnSide(index, side)) {
                slotsAccessible.add(index);
            }
        }
        
        return Ints.toArray(slotsAccessible);
    }
    
    public void addSlot(SlotHPT slot) {
        slots.put(slot.getSlotIndex(), new AdvancedSlot(slot));
    }
    
    public void setAccessibleSides(int slotId, int...sides) {
        if(slotsAccessibleBySide.containsKey(slotId)) {
            slotsAccessibleBySide.remove(slotId);
        }
        
        if(sides.length != 0) {
            slotsAccessibleBySide.put(slotId, Ints.asList(sides));
        }
        else {
            slotsAccessibleBySide.put(slotId, new ArrayList<Integer>());
        }
    }
    
    public void setName(String name) {
        this.invName = name;
    }
    
    public int[] slotsToArray() {
        int[] arr = new int[slots.size()];
        for(AdvancedSlot slot : slots.values()) {
            arr[slot.getSlot().getSlotIndex()] = slot.getSlot().getSlotIndex();
        }
        
        return arr;
    }
    
    public void writeToNBT(NBTTagCompound compound) {
        writeToNBT(compound, "invItems");
    }
    
    public void writeToNBT(NBTTagCompound compound, String compoundName) {
        NBTTagList invItems = new NBTTagList();
        
        for (int i = 0; i < getSizeInventory(); i++) {
            ItemStack stack = getStackInSlot(i);
            
            if (!Utils.isItemStackEmpty(stack)) {
                NBTTagCompound item = new NBTTagCompound();
                item.setByte("slot", (byte) i);
                stack.writeToNBT(item);
                invItems.appendTag(item);
            }
        }
        
        compound.setTag(compoundName, invItems);
    }
    
    public void readFromNBT(NBTTagCompound compound) {
        readFromNBT(compound, "invItems");
    }
    
    public void readFromNBT(NBTTagCompound compound, String compoundName) {
        // 10 is the ID of the NBTTagCompound
        NBTTagList invItems = compound.getTagList(compoundName, Constants.NBT.TAG_COMPOUND);
        
        for (int i = 0; i < invItems.tagCount(); i++) {
            NBTTagCompound item = invItems.getCompoundTagAt(i);
            int slot = item.getByte("slot");
            
            if (slot >= 0 && slot < getSizeInventory()) {
                setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(item));
            }
        }
        
        markDirty();
    }

    public AdvancedSlot getSlot(int i) {
        if(i > -1 && i < slots.size()) {
            return slots.get(i);
        }
        
        return null;
    }
    
    public boolean hasSlot(int i) {
        return getSlot(i) != null;
    }
    
    public boolean isSlotAccessibleOnSide(int slotIndex, int side) {
        return hasSlot(slotIndex)
                && ((slotsAccessibleBySide.containsKey(slotIndex) && slotsAccessibleBySide.get(slotIndex).contains(side))
                        || !slotsAccessibleBySide.containsKey(slotIndex));
    }
    
    public int getSizeInventory() {
        return slots.size();
    }
    
    public ItemStack getStackInSlot(int i) {
        AdvancedSlot slot = slots.get(i);
        return slot != null? slot.getContents() : null;
    }

    public ItemStack decrStackSize(int i, int amount) {
        ItemStack itemstack = getStackInSlot(i);
        
        if (itemstack != null) {
            if (itemstack.stackSize <= amount) {
                setInventorySlotContents(i, null);
            } else {
                itemstack = itemstack.splitStack(amount);
                markDirty();
            }
        }
        
        return itemstack;
    }

    public ItemStack getStackInSlotOnClosing(int i) {
        ItemStack item = getStackInSlot(i);
        setInventorySlotContents(i, null);
        return item;
    }

    public void setInventorySlotContents(int i, ItemStack itemstack) {
        if (itemstack != null && itemstack.stackSize > inventory.getInventoryStackLimit()) {
            itemstack.stackSize = inventory.getInventoryStackLimit();
        }
        
        AdvancedSlot slot = slots.get(i);
        
        if(slot != null) {
            slot.setContents(itemstack);
        }
        
        markDirty();
    }

    public String getInvName() {
        return "container.hpt." + invName;
    }

    public boolean isInvNameLocalized() {
        return false;
    }
    
    public int getInventoryStackLimit() {
        return 64;
    }

    public void markDirty() {
        inventory.markDirty();
    }

    public boolean isUseableByPlayer(EntityPlayer entityplayer) {
        return entityplayer.getDistanceSq(tile.xCoord + 0.5, tile.yCoord + 0.5, tile.zCoord + 0.5) <= 81;
    }

    public boolean isItemValidForSlot(int i, ItemStack itemstack) {
        return slots.get(i).getSlot().isItemValid(itemstack);
    }
    
    public boolean canInsertItem(int slotNum, ItemStack stack, int sideNum) {
        return isSlotAccessibleOnSide(slotNum, sideNum) && getSlot(slotNum).getSlot().canInsertItem(stack);
    }
    
    public boolean canExtractItem(int slotNum, ItemStack stack, int sideNum) {
        return isSlotAccessibleOnSide(slotNum, sideNum) && getSlot(slotNum).getSlot().canExtractItem(stack);
    }
}
