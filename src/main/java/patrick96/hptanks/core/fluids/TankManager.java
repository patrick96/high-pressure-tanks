/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.fluids;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.*;
import patrick96.hptanks.core.network.IChangeable;

import java.io.IOException;

public class TankManager implements IFluidHandler, IChangeable {
    
    protected FluidTank tank;
    
    protected boolean hasChanged = false;
    
    public TankManager(FluidTank tank) {
        this.tank = tank;
    }
    
    public FluidTank getTank() {
        return tank;
    }
    
    public void setTank(FluidTank tank) {
        this.tank = tank;
        markTileDirty();
    }
    
    public int getFluidAmount() {
        return tank == null? 0 : tank.getFluidAmount();
    }
    
    public int getCapacity() {
        return tank == null? 0 : tank.getCapacity();
    }
    
    public void setCapacity(int capacity) {
        tank.setCapacity(capacity);

        if(getCapacity() > 0 && getFluidAmount() > getCapacity()) {
            tank.getFluid().amount = getCapacity();
        }
    }
    
    public boolean isEmpty() {
        return tank == null || FluidUtils.isFluidStackEmpty(tank.getFluid());
    }
    
    @Override
    public void markTileDirty() {
        hasChanged = true;
    }
    
    @Override
    public void markClean() {
        hasChanged = false;
    }
    
    @Override
    public boolean hasChanged() {
        return hasChanged;
    }
    
    @Override
    public int fill(ForgeDirection from, FluidStack resource, boolean doFill) {
        if (FluidUtils.isFluidStackEmpty(resource)) {
            return 0;
        }
        
        int filled = tank.fill(resource, doFill);

        if (doFill && filled > 0) {
            markTileDirty();
        }
        return filled;
    }
    
    @Override
    public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain) {
        FluidStack drained = tank.drain(maxDrain, doDrain);
        if (doDrain && !FluidUtils.isFluidStackEmpty(drained)) {
            markTileDirty();
        }
        return drained;
    }
    
    @Override
    public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain) {
        if (FluidUtils.isFluidStackEmpty(resource) || !resource.isFluidEqual(tank.getFluid())) {
            return null;
        }
        
        FluidStack drained = drain(from, resource.amount, doDrain);
        if (doDrain && !FluidUtils.isFluidStackEmpty(drained)) {
            markTileDirty();
        }
        return drained;
    }
    
    @Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return true;
    }
    
    @Override
    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return true;
    }
    
    @Override
    public FluidTankInfo[] getTankInfo(ForgeDirection from) {
        return new FluidTankInfo[] { tank.getInfo() };
    }
    
    public void writeToNBT(NBTTagCompound compound) {
        tank.writeToNBT(compound);
    }
    
    public void readFromNBT(NBTTagCompound compound) {
        tank.readFromNBT(compound);

        if(getCapacity() > 0 && tank.getFluidAmount() > getCapacity()) {
            tank.getFluid().amount = getCapacity();
        }
    }
    
    public void writeData(ByteBuf data) throws IOException {
        if (!isEmpty()) {
            ByteBufUtils.writeUTF8String(data, tank.getFluid().getFluid().getName());
            data.writeInt(tank.getFluidAmount());
        } else {
            ByteBufUtils.writeUTF8String(data, "");
        }
    }
    
    public void readData(ByteBuf data) throws IOException {
        String fluidName = ByteBufUtils.readUTF8String(data);
        
        if (fluidName != null && fluidName.trim().length() > 0) {
            tank.setFluid(new FluidStack(FluidRegistry.getFluid(fluidName), data.readInt()));
        } else {
            tank.setFluid(null);
        }
    }

    public void reset() {
        setTank(new FluidTank(0));
    }
    
}
