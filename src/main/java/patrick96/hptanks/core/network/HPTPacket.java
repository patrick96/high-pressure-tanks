/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.network;

import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import patrick96.hptanks.lib.Reference;

import java.io.IOException;

public abstract class HPTPacket {
    
    private byte id;
    public ByteBuf input;
    
    public HPTPacket(byte id) {
        this.id = id;
    }
    
    public byte getId() {
        return id;
    }
    
    public FMLProxyPacket getPacket() {
        try {
            ByteBuf buf = Unpooled.buffer();
            buf.writeByte(id);
            writeData(buf);
            return new FMLProxyPacket(buf, Reference.CHANNEL_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    public void writeData(ByteBuf data) throws IOException {}
    
    public void readData(ByteBuf data) throws IOException {
        input = data;
    }
    
}
