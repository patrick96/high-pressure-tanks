/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.network;

import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import io.netty.buffer.ByteBuf;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;

import java.io.IOException;

public interface ISyncedTile extends IChangeable {
    void sendUpdate();
    
    void handleUpdatePacket(PacketTileUpdate packet) throws IOException;
    
    FMLProxyPacket getUpdatePacket();
    
    void writePacketData(ByteBuf data) throws IOException;
    
    void sendRequestPacket();

    void onRequestPacket();
}
