/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.init;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import patrick96.hptanks.core.fluids.BucketHandler;
import patrick96.hptanks.items.*;
import patrick96.hptanks.lib.ItemInfo;
import patrick96.hptanks.recipe.HPCutterRecipeInfo.CutterConfiguration;
import patrick96.hptanks.recipe.HPCutterRecipeInfo.CutterDamage;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.ItemStackIdentifier;

public class ModItems {
    
    public static ItemHPT wrench, circuitBoard, ingot, wire, upgrade, dust;
    
    // Buckets
    public static Item bucketResin;
    
    public static void init() {
        
        wrench = new ItemWrench();
        GameRegistry.registerItem(wrench, ItemInfo.WRENCH_UNLOCALIZED_NAME);
        
        circuitBoard = new ItemCircuitBoard();
        GameRegistry.registerItem(circuitBoard, ItemInfo.CIRCUIT_BOARD_UNLOCALIZED_NAME);
        
        ingot = new ItemIngot();
        GameRegistry.registerItem(ingot, ItemInfo.INGOT_UNLOCALIZED_NAME);
        
        wire = new ItemWire();
        GameRegistry.registerItem(wire, ItemInfo.WIRE_UNLOCALIZED_NAME);
        
        for(ItemPlain itemPlain : ItemPlain.values()) {
            ItemHPT item = new ItemHPT(itemPlain.unlocalizedName);
            item.setTextureName(itemPlain.texture);
            item.setHasEffect(itemPlain.hasEffect);
            itemPlain.item = item;
            GameRegistry.registerItem(item, itemPlain.unlocalizedName);
        }

        for(ItemCutter cutter : ItemCutter.values()) {
            patrick96.hptanks.items.ItemCutter item = new patrick96.hptanks.items.ItemCutter(cutter.unlocalizedName, cutter.config, cutter.maxDamage);
            item.setTextureName(cutter.texture);
            item.setHasEffect(cutter.hasEffect);
            cutter.item = item;
            GameRegistry.registerItem(item, cutter.unlocalizedName);
        }

        for(int i = 0; i < UpgradeType.UPGRADES.length; i++) {
            ItemInfo.UPGRADES_TEXTURE_SUFFIX[i] = UpgradeType.UPGRADES[i].textureName;
            ItemInfo.UPGRADES_UNLOCALIZED_NAME_SUFFIX[i] = UpgradeType.UPGRADES[i].textureName.toLowerCase();
        }

        upgrade = new ItemUpgrade();
        GameRegistry.registerItem(upgrade, ItemInfo.UPGRADES_UNLOCALIZED_NAME);

        dust = new ItemDust();
        GameRegistry.registerItem(dust, ItemInfo.DUST_UNLOCALIZED_NAME);
        
        bucketResin = new ItemHPTBucket(ModBlocks.fluidResin, ItemInfo.BUCKET_RESIN_UNLOCALIZED_NAME)
                .setTextureName(ItemInfo.BUCKET_RESIN_TEXTURE);
        GameRegistry.registerItem(bucketResin, ItemInfo.BUCKET_RESIN_UNLOCALIZED_NAME);
        
        MinecraftForge.EVENT_BUS.register(BucketHandler.instance);
    }

    public enum ItemCutter {
        Cutter("Cutter", new CutterConfiguration(1, CutterDamage.DESTROY, 3F, 2F)),
        HardenedCutter("HardenedCutter", new CutterConfiguration(2, CutterDamage.DAMAGE, 1F, 1F), 500, false),
        NetherStarShard("NetherStarShard", new CutterConfiguration(3, CutterDamage.DAMAGE, 0.9F, 0.8F), 1250, true),
        DiamondCoatedNetherStarShard("DiamondCoatedNetherStarShard", new CutterConfiguration(4, CutterDamage.UNBREAKABLE, 0.5F, 0.5F), 0, true),
        LaserCutter("LaserCutter", new CutterConfiguration(3, CutterDamage.DAMAGE, 3F, 3F), 100, false);

        public final String texture, unlocalizedName;
        public ItemHPT item;
        public final int maxDamage;
        public final boolean hasEffect;
        public final CutterConfiguration config;

        ItemCutter(String name, CutterConfiguration config) {
            this(name, config, 0, false);
        }


        ItemCutter(String name, CutterConfiguration config, int maxDamage, boolean hasEffect) {
            this.texture = name;
            this.unlocalizedName = this.texture.toLowerCase();
            this.hasEffect = hasEffect;
            this.config = config;
            this.maxDamage = maxDamage;
        }

        public ItemStack getStack() {
            return item.getStack();
        }

        public ItemStack getStack(int size) {
            return item.getStack(size);
        }

        public ItemStack getStack(int size, int meta) {
            return item.getStack(size, meta);
        }
    }

    public enum ItemPlain {
        
        ObsidianStick("ObsidianStick"),
        ReinforcedBars("ReinforcedBars"),
        Graphite("Graphite"),
        Graphene("Graphene"),
        GraphiteBulk("GraphiteBulk"),
        CNT("CNT"),
        InternalTank("InternalTank"),
        UpgradePlate("upgrades/Plate", "UpgradePlate"),
        Wafer("Wafer"),
        HardenedRod("HardenedRod"),
        DiamondFilm("DiamondFilm"),
        EnergyCore("EnergyCore", true),
        ;

        
        public final String texture, unlocalizedName;
        public ItemHPT item;
        public final boolean hasEffect;


        ItemPlain(String textureName) {
            this(textureName, textureName, false);
        }

        ItemPlain(String textureName, boolean hasEffect) {
            this(textureName, textureName, hasEffect);
        }

        ItemPlain(String textureName, String unlocalizedName) {
            this(textureName, unlocalizedName, false);
        }

        ItemPlain(String textureName, String unlocalizedName, boolean hasEffect) {
            this.texture = textureName;
            this.unlocalizedName = unlocalizedName.toLowerCase();
            this.hasEffect = hasEffect;
        }
        
        public ItemStack getStack() {
            return item.getStack();
        }
        
        public ItemStack getStack(int size) {
            return item.getStack(size);
        }

        public ItemStack getStack(int size, int meta) {
            return item.getStack(size, meta);
        }
    }
}
