/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.init;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import patrick96.hptanks.blocks.*;
import patrick96.hptanks.blocks.fluids.BlockFluidResin;
import patrick96.hptanks.blocks.hpt.*;
import patrick96.hptanks.lib.BlockInfo;

import java.util.ArrayList;
import java.util.List;

public class ModBlocks {
    
    public static BlockHPT tankCasing, tankGlass, tankValve, /*tank,*/ tankControl, tankCCperipheral, bulletproofGlass, ore, metal, compressed, machine, fluidPump, resin;

    public static Block obsidianBars, blockFluidResin;

    public static Fluid fluidResin;

    public static List<BuildingBlock> buildingBlocks = new ArrayList<BuildingBlock>();
    public static BuildingBlock resinBrick, smoothResin, resinBrickDark, smoothResinDark;
    
    public static void init() {
        tankCasing = new BlockTankCasing();
        GameRegistry.registerBlock(tankCasing, BlockInfo.TANK_CASING_UNLOCALIZED_NAME);
        
        tankGlass = new BlockTankGlass();
        GameRegistry.registerBlock(tankGlass, BlockInfo.TANK_GLASS_UNLOCALIZED_NAME);
        
        tankValve = new BlockTankValve();
        GameRegistry.registerBlock(tankValve, BlockInfo.TANK_VALVE_UNLOCALIZED_NAME);
        
        tankControl = new BlockTankControl();
        GameRegistry.registerBlock(tankControl, ItemBlockTankControl.class, BlockInfo.TANK_CONTROL_UNLOCALIZED_NAME);
        
        tankCCperipheral = new BlockTankCCPeripheral();
        GameRegistry.registerBlock(tankCCperipheral, BlockInfo.TANK_CC_PERIPHERAL_UNLOCALIZED_NAME);

//        tank = new BlockTank();
//        GameRegistry.registerBlock(tank, BlockInfo.TANK_UNLOCALIZED_NAME);

        obsidianBars = new BlockObsidianBars(BlockInfo.OBSIDIAN_BARS_TEXTURE);
        GameRegistry.registerBlock(obsidianBars, BlockInfo.OBSIDIAN_BARS_UNLOCALIZED_NAME);
        
        bulletproofGlass = new BlockGlassBulletProof();
        GameRegistry.registerBlock(bulletproofGlass, BlockInfo.GLASS_BULLETPROOF_UNLOCALIZED_NAME);
        
        ore = new BlockOre();
        GameRegistry.registerBlock(ore, ItemBlockOre.class, BlockInfo.ORE_UNLOCALIZED_NAME);
        
        metal = new BlockMetal();
        GameRegistry.registerBlock(metal, ItemBlockMetal.class, BlockInfo.METAL_BLOCK_UNLOCALIZED_NAME);

        compressed = new BlockCompressed();
        GameRegistry.registerBlock(compressed, ItemBlockCompressed.class, BlockInfo.COMPRESSED_BLOCK_UNLOCALIZED_NAME);
        
        machine = new BlockMachine();
        GameRegistry.registerBlock(machine, ItemBlockMachine.class, BlockInfo.MACHINE_UNLOCALIZED_NAME);
        
        fluidPump = new BlockFluidPump();
        GameRegistry.registerBlock(fluidPump, ItemBlockFluidPump.class, BlockInfo.FLUID_PUMP_UNLOCALIZED_NAME);

        resin = new BlockResin();
        GameRegistry.registerBlock(resin, ItemBlockResin.class, BlockInfo.BLOCK_RESIN_UNLOCALIZED_NAME);

        BlockHPT resinBrickBase = new BlockHPT(Material.rock, "blockResinBrick");
        resinBrickBase.setBlockTextureName("BlockResinBrick").setHardness(2.0F).setResistance(5.0F);
        resinBrick = new BuildingBlock(resinBrickBase);
        addBuildingBlock(resinBrick);

        BlockHPT smoothResinBase = new BlockHPT(Material.rock, "blockSmoothResin");
        smoothResinBase.setBlockTextureName("BlockSmoothResin").setHardness(2.0F).setResistance(5.0F);
        smoothResin = new BuildingBlock(smoothResinBase);
        addBuildingBlock(smoothResin);

        BlockHPT resinBrickDarkBase = new BlockHPT(Material.rock, "blockResinBrickDark");
        resinBrickDarkBase.setBlockTextureName("BlockResinBrickDark").setHardness(2.0F).setResistance(5.0F);
        resinBrickDark = new BuildingBlock(resinBrickDarkBase);
        addBuildingBlock(resinBrickDark);

        BlockHPT smoothResinDarkBase = new BlockHPT(Material.rock, "blockSmoothResinDark");
        smoothResinDarkBase.setBlockTextureName("BlockSmoothResinDark").setHardness(2.0F).setResistance(5.0F);
        smoothResinDark = new BuildingBlock(smoothResinDarkBase);
        addBuildingBlock(smoothResinDark);

        fluidResin = new Fluid(BlockInfo.FLUID_RESIN_UNLOCALIZED_NAME).setViscosity(20000).setDensity(1200);
        FluidRegistry.registerFluid(fluidResin);
        
        Fluid registeredResin = FluidRegistry.getFluid(BlockInfo.FLUID_RESIN_UNLOCALIZED_NAME);
        if (registeredResin.getBlock() == null) {
            fluidResin = registeredResin;
            blockFluidResin = new BlockFluidResin(fluidResin);
            GameRegistry.registerBlock(blockFluidResin, BlockInfo.FLUID_RESIN_UNLOCALIZED_NAME);
            fluidResin.setBlock(blockFluidResin).setUnlocalizedName(blockFluidResin.getUnlocalizedName());
        } else {
            blockFluidResin = registeredResin.getBlock();
        }
    }

    protected static void addBuildingBlock(BuildingBlock block) {
        buildingBlocks.add(block);
        block.register();
    }

    public static boolean isBuildingBlock(Block block) {
        for(BuildingBlock buildingBlock : buildingBlocks) {
            if(buildingBlock.getSlab() == block || buildingBlock.getDoubleSlab() == block || buildingBlock.getStairs() == block || buildingBlock.getBlock() == block) {
                return true;
            }
        }

        return false;
    }
}
