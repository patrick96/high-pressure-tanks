#[High Pressure Tanks](http://www.minecraftforum.net/topic/2334053-/)

Automate your fluids!

##Development
Starting development on HPTank yourself is very easy. Simply fork the repo and clone it onto your machine. After that run `gradlew setupDecompWorkspace` and `gradlew idea` or `gradlew eclipse` depending on your IDE of choice.
Additionally you will need to place the newest version of *Ex Nihilo* into the libs folder and the api source files of *ComputerCraft* and *[RedstoneFlux](https://github.com/CoFH/RedstoneFlux-API)* into the `src/main/java/` folder.

**Note:** Don't just copy everything in the *RedstoneFlux* git repo or the *ComputerCraft* jar file. For *RedstoneFlux* copy everything inside the `src/main/java/` folder, and for *ComputerCraft* everything inside the `api/src/` folder located inside every CC jar.

You can add or edit whatever you want, then make PR and I will see if I like what you did and if so your code may go into this mod.
If you plan on adding anything major, please talk to me about it first, because I don't want anybody spending a lot of time working on a feature and then it gets dismissed just because it doesn't fit the theme or something else.

###Build
To build a jar file of this mod that can be used outside the IDE, you can either run `gradlew buildJar` or `gradlew release`:

* `buildJar` will create a jar file meant for personal testing and not for deployment with incremental build numbers. 

* `release` will create all the files needed for deployment with the version number defined in `build.properties`, including an unobfuscated dev jar with all the source files.


If you have any questions feel free to contact me on here, the [Minecraft Forums](http://www.minecraftforum.net/members/patrick96) or [Twitter](http://twitter.com/patrick96MC)

Everything else you need to know can be found on the [Forum Post](http://www.minecraftforum.net/topic/2334053-/).